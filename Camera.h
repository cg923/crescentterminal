
// Copyright 2019 Happyrock Studios

#ifndef CAMERA_H
#define CAMERA_H

// Moon
class Game;

// STD
#include <memory>

// SFML
#include <SFML/Graphics.hpp>

class Camera {
public:
    Camera(std::shared_ptr<Game> game, std::string targetEntityName = "player");

    void update();

    void setTargetEntity(std::string entityName);

    void zoom(float factor);

    const sf::View view();

private:
    sf::View m_view;

    float m_zoomFactor;

    std::shared_ptr<Game> m_game;

    std::string m_targetEntityName;
};
#endif // CAMERA_H
