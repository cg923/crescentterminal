
// Copyright 2019 Happyrock Studios

// Moon
#include "Crop.h"
#include "Game.h"
#include "Item.h"

// STD
#include <iostream>

Crop::Crop(Crops cropType, sf::Vector2i gridPosition, std::shared_ptr<Game> game, int tileWidth, int tileHeight) {
    m_cropType = cropType;
    m_gridPosition = gridPosition;
    m_game = game;

    m_stage = GS_SEED;
    m_ageInDays = 1;
    m_ripeByDay = -1;
    m_harvested = false;
	m_quality = CQ_GOOD;

    // Initialize Sprite.
    m_textureRect = sf::IntRect(0, 0, tileWidth, tileHeight);
    m_sprite.setTextureRect(m_textureRect);
    m_sprite.setTexture(*m_game->texture("crops"));
    m_sprite.setOrigin(sf::Vector2f(m_textureRect.width/2, m_textureRect.height/2));
}

void Crop::update(double dt) {
}

void Crop::draw() {
    if (m_cropType != CROP_NONE) {
        m_game->renderWindow()->draw(m_sprite);
    }
}

void Crop::plant(Crops cropType) {
    m_cropType = cropType;

    switch(m_cropType) {
    case CROP_ASPARAGUS:
        m_textureRect = sf::IntRect(m_textureRect.width, 0, m_textureRect.width, m_textureRect.height);
        m_ripeByDay = 20;
        break;
    case CROP_BEETS:
        m_textureRect = sf::IntRect(m_textureRect.width * 4, 0, m_textureRect.width, m_textureRect.height);
        m_ripeByDay = 10;
        break;
	case CROP_BOKCHOY:
		m_textureRect = sf::IntRect(m_textureRect.width * 7, 0, m_textureRect.width, m_textureRect.height);
		m_ripeByDay = 6;
		break;
	case CROP_CARROTS:
		m_textureRect = sf::IntRect(0, m_textureRect.height, m_textureRect.width, m_textureRect.height);
		m_ripeByDay = 8;
		break;
	case CROP_GARLIC:
		m_textureRect = sf::IntRect(m_textureRect.width * 3, m_textureRect.height, m_textureRect.width, m_textureRect.height);
		m_ripeByDay = 15;
		break;
    case CROP_NONE:
        m_textureRect = sf::IntRect(0, 0, m_textureRect.width, m_textureRect.height);
        m_ripeByDay = -1;
        break;
    }

    m_sprite.setTextureRect(m_textureRect);
    m_sprite.setPosition(m_game->gridToPixels(m_gridPosition.x, m_gridPosition.y));
}

void Crop::ageUp(int days) {
    if (m_harvested) {
        plant(CROP_NONE);
        m_ageInDays = 0;
        m_harvested = false;
    }
    if (m_cropType != CROP_NONE) m_ageInDays += days;
    else return;

    // Multiplying by 2 puts us in the range of 0 and 2.
    // So, if we add more stages, we'll need to adjust this number.
    float range = m_ageInDays * 2 / m_ripeByDay;
    int whatStage = int(range);
    switch(whatStage) {
    case 0:
        m_stage = GS_SEED;
        m_sprite.setTextureRect(m_textureRect);
        break;
    case 1:
        m_stage = GS_BABY;
        m_sprite.setTextureRect(sf::IntRect(m_textureRect.left + m_textureRect.width, m_textureRect.top, m_textureRect.width, m_textureRect.height));
        break;
    case 2:
        m_stage = GS_GROWN;
        m_sprite.setTextureRect(sf::IntRect(m_textureRect.left + 2*m_textureRect.width, m_textureRect.top, m_textureRect.width, m_textureRect.height));
        break;
    }
}

std::shared_ptr<Yield> Crop::harvest() {
    // TODO - quantity
    m_harvested = true;
    m_textureRect = sf::IntRect(0, 0, m_textureRect.width, m_textureRect.height);
    m_sprite.setTextureRect(m_textureRect);
    return std::make_shared<Yield>(m_cropType, 1, m_quality, "base", m_game);
}

void Crop::wilt() {
	switch (m_quality) {
	case CQ_POOR:
		harvest();
		break;
	case CQ_FAIR:
		m_quality = CQ_POOR;
		break;
	case CQ_GOOD:
		m_quality = CQ_FAIR;
		break;
	case CQ_EXCELLENT:
		m_quality = CQ_GOOD;
		break;
	}
}

const Crops Crop::type() {
    return m_cropType;
}

void Crop::setAge(int age) {
    ageUp(age);
}

int Crop::age() {
    return m_ageInDays;
}

sf::Vector2i Crop::gridPosition() {
    return m_gridPosition;
}

bool Crop::readyForHarvest() {
    return (!m_harvested && m_ageInDays >= m_ripeByDay && m_ripeByDay != -1);
}
