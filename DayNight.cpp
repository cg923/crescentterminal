
// Copyright 2019 Happyrock Studios

// Moon
#include "DayNight.h"
#include "Game.h"
#include "Player.h"
#include "Map.h"

// STD
#include <sstream>
#include <iostream>
#include <math.h>

DayNight::DayNight(std::shared_ptr<Game> game, float timeScale) {
    m_game = game;
    m_timeScale = timeScale;
    m_dayNightClock = sftools::Chronometer();
	m_darknessAlphaOverride = -1;

    startFromBeginningOfTime();

	m_darknessRenderTex.create(m_game->renderWindow()->getSize().x, m_game->renderWindow()->getSize().y);
	m_darkness.setSize(sf::Vector2f(m_game->renderWindow()->getSize()));
	m_darkness.setFillColor(sf::Color(0, 0, 0, 255));
	m_darkness.setOrigin(m_darkness.getSize().x / 2, m_darkness.getSize().y / 2);

	m_flashlightLightSprite.setTexture(*m_game->texture("flashlight_light"));
	m_flashlightLightSprite.setOrigin(m_flashlightLightSprite.getTextureRect().width / 2, m_flashlightLightSprite.getTextureRect().height);
}

void DayNight::startFromBeginningOfTime() {
    m_dayNightClock.reset();
    m_day = 1;
    m_month = 1;
    m_year = 2087;
    m_hours = 0;
    m_minutes = 0;
    m_secs = 0;

    advanceToHour(6);

    updateStrings();

	pause();
}

void DayNight::drawDarkness() {
	m_darknessRenderTex.clear(sf::Color::Black);
	std::shared_ptr<Player> player = std::dynamic_pointer_cast<Player>(m_game->entity("player"));
	if (player) {
		m_darkness.setPosition(player->pixelPosition());
		m_darknessRenderTex.draw(m_darkness, sf::BlendMultiply);
	}
	if (player && player->flashlightOn()) {
		m_flashlightLightSprite.setPosition(player->pixelPosition() + sf::Vector2f(CHARA_SPRITE_WIDTH * player->spriteScale().x / 2, CHARA_SPRITE_HEIGHT * player->spriteScale().y / 2));
		float angle = m_game->mouseAngle();
		m_flashlightLightSprite.setRotation(angle);
		m_darknessRenderTex.draw(m_flashlightLightSprite, sf::BlendMultiply);
	}
	m_darknessRenderTex.display();
	m_game->renderWindow()->draw(sf::Sprite(m_darknessRenderTex.getTexture()));
}

void DayNight::pause() {
    m_dayNightClock.pause();
}

void DayNight::resume() {
    m_dayNightClock.resume();
}

void DayNight::toggle() {
    m_dayNightClock.toggle();
}

void DayNight::update() {
    int gameTime = m_dayNightClock.getElapsedTime().asSeconds() / m_timeScale;

    if (gameTime > 86400) m_game->advanceDay();

    m_hours = int(gameTime / 3600);
    int remainder = int(gameTime - m_hours * 3600);
    m_minutes = remainder / 60;
    remainder = remainder - m_minutes * 60;
    m_secs = remainder;

	int newAlpha = getDarknessAlpha();
	int oldAlpha = m_darkness.getFillColor().a;

	if (oldAlpha < newAlpha && m_darknessTimer.getElapsedTime().asSeconds() > 0.2) {
		m_darkness.setFillColor(sf::Color(0, 0, 0, oldAlpha + 1));
		m_darknessTimer.restart();
	}
	else if (oldAlpha > newAlpha && m_darknessTimer.getElapsedTime().asSeconds() > 0.2) {
		m_darkness.setFillColor(sf::Color(0, 0, 0, oldAlpha - 1));
		m_darknessTimer.restart();
	}
}

void DayNight::updateStrings() {
    static std::stringstream ss;
    ss.str("");
    ss << m_month << "/" << m_day << "/" << m_year;
    m_currentDateAsString = ss.str();

    ss.str("");
    std::string hoursAdjust = m_hours  < 10 ? "0" : "";
    std::string minsAdjust = m_minutes < 10 ? "0" : "";
    ss << hoursAdjust << m_hours << ":" << minsAdjust << m_minutes;
    m_currentTimeAsString = ss.str();
}

std::string DayNight::formatTime(int hours, int minutes) {
	std::stringstream ss;
	std::string hoursAdjust = hours < 10 ? "0" : "";
	std::string minsAdjust = minutes < 10 ? "0" : "";
	ss << hoursAdjust << hours << ":" << minsAdjust << minutes;
	return ss.str();
}

void DayNight::advanceToHour(int hour) {
    m_dayNightClock.reset(false);
    sf::Time time = sf::seconds(hour * 60 * 60 * m_timeScale);
    m_dayNightClock.add(time);
    m_dayNightClock.resume();
}

void DayNight::advanceDay() {
	beginFadeOut();

    m_dayNightClock.reset(true);
    m_day++;

    if (m_day > 28) {
        m_day = 1;
        m_month++;
    }

    if (m_month > 12) {
        m_month = 1;
        m_year++;
    }
}

void DayNight::beginFadeIn() {
	m_fadingIn = true;
}

void DayNight::beginFadeOut() {
	m_fadingOut = true;
}

void DayNight::fadeIn() {
	// Wait for fadeOut to finish.
	if (m_fadingOut) return;

	// Otherwise player sprite doesn't draw in correct location
	m_game->entity("player")->setSpritesPosition(m_game->entity("player")->pixelPosition());

	int oldAlpha = m_darkness.getFillColor().a;
	int newAlpha = std::max(getDarknessAlpha(), m_darkness.getFillColor().a - 2);

	if (oldAlpha > newAlpha && m_darknessTimer.getElapsedTime().asSeconds() > 0.01) {
		m_darkness.setFillColor(sf::Color(0, 0, 0, newAlpha));
		m_darknessTimer.restart();
	}
	else if (oldAlpha <= newAlpha) {
		m_fadingIn = false;
	}
}

void DayNight::fadeOut() {
	int oldAlpha = m_darkness.getFillColor().a;
	int newAlpha = std::min(255, oldAlpha + 2);

	if (oldAlpha < newAlpha && m_darknessTimer.getElapsedTime().asSeconds() > 0.01) {
		m_darkness.setFillColor(sf::Color(0, 0, 0, newAlpha));
		m_darknessTimer.restart();
	}

	if (newAlpha == 255) {
		m_fadingOut = false;
		m_game->movePlayerToNextMap();
	}
}

void DayNight::resetDarkness() {
	m_darknessRenderTex.create(m_game->currentMap()->dimensions().width, m_game->currentMap()->dimensions().height);
}

//////////////////////////////////////////////////
//              Getters & Setters               //
//////////////////////////////////////////////////

const int DayNight::seconds() {
    return m_secs;
}

const int DayNight::minutes() {
    return m_minutes;
}

const int DayNight::hours() {
    return m_hours;
}

const std::string DayNight::fullTime() {
    updateStrings();
    return m_currentTimeAsString;
}

void DayNight::setTime(sf::Time currentTime, bool startNow) {
    m_dayNightClock.reset(startNow);
    m_dayNightClock.add(currentTime);

    updateStrings();
}

const int DayNight::day() {
    return m_day;
}

void DayNight::setDay(int day) {
    m_day = std::max(1, day);
    m_day = std::min(28, m_day);
}

const std::string DayNight::dayOfWeek() {
    switch (m_day) {
        case 1:
        case 8:
        case 15:
        case 22:
            return "Sunday";
        case 2:
        case 9:
        case 16:
        case 23:
            return "Monday";
        case 3:
        case 10:
        case 17:
        case 24:
            return "Tuesday";
        case 4:
        case 11:
        case 18:
        case 25:
            return "Wednesday";
        case 5:
        case 12:
        case 19:
        case 26:
            return "Thursday";
        case 6:
        case 13:
        case 20:
        case 27:
            return "Friday";
        case 7:
        case 14:
        case 21:
        case 28:
            return "Saturday";
        default:
            return "Invalid day value";
    }
}

const int DayNight::month() {
    return m_month;
}

void DayNight::setMonth(int month) {
    m_month = std::min(12, month);
    m_month = std::max(1, m_month);
}

const int DayNight::year() {
    return m_year;
}

void DayNight::setYear(int year) {
    m_year = std::max(2087, year);
    m_year = std::min(m_year, 2089);
}

const std::string DayNight::fullDate() {
    updateStrings();
    return m_currentDateAsString;
}

const std::string DayNight::timeOfDay() {
    switch (hours()) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
        return "night";
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
        return "morning";
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
        return "afternoon";
    case 18:
    case 19:
    case 20:
    case 21:
        return "evening";
    case 22:
    case 23:
        return "night";
    }

    return "SHOULD NOT HAVE MADE IT HERE";
}

void DayNight::setDate(std::string currentDate) {
    m_day = stoi(currentDate.substr(0, currentDate.find(":")));
    m_month = stoi(currentDate.substr(currentDate.find(":") + 1, currentDate.rfind(":")));
    m_year = stoi(currentDate.substr(currentDate.rfind(":")));

    updateStrings();
}

const int DayNight::getDarknessAlpha() {
	if (m_darknessAlphaOverride >= 0) {
		return m_darknessAlphaOverride;
	}

    switch (hours()) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
        return 200;
    case 6:
        return 120;
    case 7:
        return 80;
    case 8:
        return 40;
    case 9:
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
        return 0;
    case 16:
        return 40;
    case 17:
        return 80;
    case 18:
        return 120;
    case 19:
        return 160;
    case 20:
    case 21:
    case 22:
    case 23:
        return 200;
    default:
        return 0;
    }
}

void DayNight::setDarknessAlpha(int newAlpha) {
	m_darknessAlphaOverride = newAlpha;
}

const bool DayNight::fadingIn() {
	return m_fadingIn;
}

const bool DayNight::fadingOut() {
	return m_fadingOut;
}
