
// Copyright 2019 Happyrock Studios

// STD
#include <iostream>
#include <sstream>

// Moon
#include "Game.h"
#include "Camera.h"
#include "Dialogue.h"
#include "Player.h"
#include "EventManager.h"
#include "Map.h"
#include "Npc.h"
#include "Tool.h"
#include "InventoryGui.h"
#include "ImageObject.h"
#include "TimeWeatherGui.h"
#include "MachiningGui.h"
#include "DayNight.h"
#include "ItemLocker.h"
#include "SaveLoad.h"
#include "Door.h"
#include "ComputerGui.h"
#include "Hoop.h"
#include "ResearchManager.h"
#include "Tree.h"

Game::Game() {}

/////////////////////////////////////////////////////////////////////////////////////
//                           Main Game Loop                                        //
/////////////////////////////////////////////////////////////////////////////////////

void Game::setup() {

    /* Load order is somewhat important and should be preserved when possible.
     * At the same time, the game should be built so that segfaults do not happen.
     * This should include being able to create any object in isolation without consequence.
     */

    // Window
	m_window = std::make_shared<sf::RenderWindow>(sf::VideoMode::getFullscreenModes()[0], "Moon", sf::Style::Fullscreen);

    m_displayMenu       = false;
    m_displayDialogue   = false;
    m_displayInventory  = false;
    m_displayComputer   = false;
	m_displayMachining  = false;
	m_displayGui = true;
	m_pause = false;
    m_displayLocker     = "";
	m_itemToAdd = "";
	m_timeToAdd = "";
    m_mouseHeldItem     = NULL;

    m_guis["dialogue"]      = std::make_shared<Dialogue>(game());
    m_guis["inventory"]     = std::make_shared<InventoryGui>(game(), sf::Vector2f(renderWindow()->getSize().x / 2, 50));
    // TODO - this should be handled in ItemLocker, if possible.
    m_guis["locker1"]       = std::make_shared<InventoryGui>(game(), sf::Vector2f(renderWindow()->getSize().x /2, 300));
    m_guis["locker2"]       = std::make_shared<InventoryGui>(game(), sf::Vector2f(renderWindow()->getSize().x /2, 300));
    m_guis["hopper1"]       = std::make_shared<InventoryGui>(game(), sf::Vector2f(renderWindow()->getSize().x /2, 300));
    m_guis["hopper2"]       = std::make_shared<InventoryGui>(game(), sf::Vector2f(renderWindow()->getSize().x /2, 300));
    m_guis["hopper3"]       = std::make_shared<InventoryGui>(game(), sf::Vector2f(renderWindow()->getSize().x /2, 300));
    m_guis["hopper4"]       = std::make_shared<InventoryGui>(game(), sf::Vector2f(renderWindow()->getSize().x /2, 300));
	m_guis["purchasing"]	= std::make_shared<InventoryGui>(game(), sf::Vector2f(renderWindow()->getSize().x / 2, 300));
    m_guis["timeweather"]   = std::make_shared<TimeWeatherGui>(game());
    m_guis["computer"]      = std::make_shared<ComputerGui>(game());
	m_guis["machining"]		= std::make_shared<MachiningGui>(game());

    // Maps
    m_maps["base"]			= std::make_shared<Map>("base", "map_base.xml", game());
    m_maps["upstairs"]		= std::make_shared<Map>("upstairs", "map_base_upstairs.xml", game());
	m_maps["woods"]			= std::make_shared<Map>("woods", "map_woods.xml", game());
	m_maps["cave"]			= std::make_shared<Map>("cave", "map_cave.xml", game());

    m_maps["upstairs"]->hideLayer("roof1");
    m_maps["base"]->hideLayer("roof1");
    // TODO - this should not need to work this way.
    m_currentMap = "upstairs";
    setCurrentMap("upstairs");

    // Entities
    // Load player textures ahead of time so we can switch between them.
    texture("player1");
    texture("player2");
    texture("player3");

    m_entities["player"]    = std::make_shared<Player>("player", "upstairs", DOWN, game());
    m_entities["robby"]     = std::make_shared<Npc>("robby", "robby", "upstairs", DOWN, AMBIVALENT, game());
    m_entities["stella"]    = std::make_shared<Npc>("stella", "stella", "upstairs", DOWN, AMBIVALENT, game());
    m_entities["al"]        = std::make_shared<Npc>("al", "al", "upstairs", DOWN, AMBIVALENT, game());
    m_entities["antoni"]    = std::make_shared<Npc>("antoni", "antoni", "upstairs", DOWN, AMBIVALENT, game());
    m_entities["locker1"]   = std::make_shared<ItemLocker>("locker1", "upstairs", game());
    m_entities["locker2"]   = std::make_shared<ItemLocker>("locker2", "base", game());
    m_entities["hopper1"]   = std::make_shared<ItemLocker>("hopper1", "base", game());
    m_entities["hopper2"]   = std::make_shared<ItemLocker>("hopper2", "base", game());
    m_entities["hopper3"]   = std::make_shared<ItemLocker>("hopper3", "base", game());
    m_entities["hopper4"]   = std::make_shared<ItemLocker>("hopper4", "base", game());
    m_entities["purchasing"]= std::make_shared<ItemLocker>("purchasing", "base", game());
	m_entities["onion"] = std::make_shared<ImageObject>("onion", "onion", "base", game(), 58, 90);
	m_lockers.push_back(std::dynamic_pointer_cast<ItemLocker>(m_entities["locker1"]));
	m_lockers.push_back(std::dynamic_pointer_cast<ItemLocker>(m_entities["locker2"]));
	m_lockers.push_back(std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper1"]));
	m_lockers.push_back(std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper2"]));
	m_lockers.push_back(std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper3"]));
	m_lockers.push_back(std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper4"]));
	m_lockers.push_back(std::dynamic_pointer_cast<ItemLocker>(m_entities["purchasing"]));
	m_entities["hoop"]		= std::make_shared<Hoop>("base", game(), 6, 14);

    m_entities["player"]->setPositionByGrid(2,3);
    //m_entities["player"]->setPositionByGrid(58,96);
    //m_entities["player"]->setPositionByGrid(28,13);
    m_entities["robby"]->setPositionByGrid(14,3);
    m_entities["stella"]->setPositionByGrid(8,3);
    m_entities["al"]->setPositionByGrid(16,3);
    m_entities["antoni"]->setPositionByGrid(10,3);
    m_entities["locker1"]->setPositionByGrid(1,3);
    m_entities["locker2"]->setPositionByGrid(46,14);
    m_entities["hopper1"]->setPositionByGrid(39,15);
    m_entities["hopper2"]->setPositionByGrid(39,16);
    m_entities["hopper3"]->setPositionByGrid(42,15);
    m_entities["hopper4"]->setPositionByGrid(42,16);
    m_entities["purchasing"]->setPositionByGrid(32,13);
	m_entities["onion"]->setPositionByGrid(40, 8);
	m_entities["hoop"]->setPositionByGrid(6, 14);
    std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper1"])->setDisplayText("Contract A");
    std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper2"])->setDisplayText("Contract B");
    std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper3"])->setDisplayText("Contract C");
    std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper4"])->setDisplayText("Contract D");
    std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper1"])->setDisplayBelowText("No associated contract");
    std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper2"])->setDisplayBelowText("No associated contract");
    std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper3"])->setDisplayBelowText("No associated contract");
    std::dynamic_pointer_cast<ItemLocker>(m_entities["hopper4"])->setDisplayBelowText("No associated contract");
	std::dynamic_pointer_cast<ItemLocker>(m_entities["purchasing"])->setDisplayText("Purchasing");
	std::dynamic_pointer_cast<ItemLocker>(m_entities["purchasing"])->setDisplayBelowText("Purchased items will be stored here");

    // Doors
    m_entities["door1"]     = std::make_shared<Door>("upstairs", game(), "player");
    m_entities["door1"]->setPositionByGrid(2,6);
    m_entities["door2"]     = std::make_shared<Door>("upstairs", game(), "player");
    m_entities["door2"]->setPositionByGrid(6,6);
    m_entities["door3"]     = std::make_shared<Door>("upstairs", game(), "player");
    m_entities["door3"]->setPositionByGrid(9,6);
    m_entities["door4"]     = std::make_shared<Door>("upstairs", game(), "player");
    m_entities["door4"]->setPositionByGrid(13,6);
    m_entities["door5"]     = std::make_shared<Door>("upstairs", game(), "player");
    m_entities["door5"]->setPositionByGrid(17,6);
    m_entities["door6"]     = std::make_shared<Door>("base", game(), "player");
    m_entities["door6"]->setPositionByGrid(22,20);
    m_entities["door7"]     = std::make_shared<Door>("base", game(), "player");
    m_entities["door7"]->setPositionByGrid(25,16);
    m_entities["door8"]     = std::make_shared<Door>("base", game(), "player");
    m_entities["door8"]->setPositionByGrid(30,16);

    defineNpcBehaviors();

    // Tools
    m_entities["rototiller"]    = std::make_shared<Rototiller>(game());
    m_entities["seedspreader"]  = std::make_shared<Seedspreader>(game());
    m_entities["wateringcan"]   = std::make_shared<Wateringcan>(game());
    m_entities["chainsaw"]		= std::make_shared<Chainsaw>(game());
    m_entities["basketball"]	= std::make_shared<Basketball>(game(), gridToPixels(7, 15));

    m_entities["rototiller"]->setPositionByGrid(46,10);
    m_entities["seedspreader"]->setPositionByGrid(46,9);
    m_entities["wateringcan"]->setPositionByGrid(47,9);
    m_entities["chainsaw"]->setPositionByGrid(48,9);
    m_entities["basketball"]->setPositionByGrid(7,15);

    // Events
    m_events = std::make_shared<EventManager>(game());
    std::stringstream eventsFilePath;
    eventsFilePath << RESOURCES_PATH << "events.xml";
    m_events->generateEventsFromFile(eventsFilePath.str());

    // Day night
    m_dayNightCycle = std::make_shared<DayNight>(game(), 0.01f);

	// Research Manager
	m_researchManager = std::make_shared<ResearchManager>(game());

    // Camera
    m_camera = std::make_shared<Camera>(game());

    // Load saved game or begin new one.
    m_promptSleep = false;
    m_saveLoad = std::make_shared<SaveLoad>(game());
}

void Game::loadSaveSlot(int saveSlot) {
	bool loadedGame = m_saveLoad->loadSaveFile(saveSlot);

	m_camera->update();

	m_maps["base"]->update(0.0);
	m_maps["upstairs"]->update(0.0);
	m_maps["woods"]->update(0.0);
	m_maps["cave"]->update(0.0);
	updateEntities(0.0);

	// New game
	if (!loadedGame) {
		std::dynamic_pointer_cast<ItemLocker>(m_entities["locker2"])->addToInventory(std::make_shared<Seed>(CROP_BEETS, 10, "base", game()));
		std::dynamic_pointer_cast<ItemLocker>(m_entities["locker2"])->addToInventory(std::make_shared<Seed>(CROP_ASPARAGUS, 10, "base", game()));
		std::dynamic_pointer_cast<ItemLocker>(m_entities["locker2"])->addToInventory(std::make_shared<Sprinkler>("base", game()));
		std::dynamic_pointer_cast<ComputerGui>(m_guis["computer"])->inboxEmail(1);
		
		// Grow some weeds.
		m_maps["base"]->advanceDay();

		// Set up intro movie
		setCurrentMap("base");
		m_maps["upstairs"]->showLayer("roof1");
		m_entities["player"]->setPositionByGrid(40, 9);
		m_entities["player"]->setVisibility(false);
		m_entities["robby"]->setCurrentMap("base");
		m_entities["robby"]->setPositionByGrid(40, 13);
		m_entities["onion"]->setVisibility(false);
		m_entities["player"]->setVisibility(false);
		hideGui();
		m_events->activateTimeEvent("01/01/2087");
	}
}

void Game::run() {

    // Start yer engines
    m_dayNightCycle->resume();

	m_gameClock.restart();
	double oldTime = 0.0;
	double lag = 0.0;
	double dt = 1 / 100.0;

    while (m_window->isOpen())
    {
        double newTime = m_gameClock.getElapsedTime().asSeconds();
        double elapsed = newTime - oldTime;
        oldTime = newTime;
        lag += elapsed;

        // User input
        sf::Event event;
        while (m_window->pollEvent(event)) handleInput(event);

        // Update loop
        while (lag >= dt) {
			sf::Vector2f mouseCoords = renderWindow()->mapPixelToCoords(sf::Mouse::getPosition());
			m_mouseWorldCoords = mouseCoords;
			m_mousePixelPosition = sf::Vector2f(sf::Mouse::getPosition());
			m_mouseGridPosition = pixelsToGrid(mouseCoords.x, mouseCoords.y);

            // Changing maps
            if (m_newMap.compare(m_currentMap) != 0) {
                m_dayNightCycle->beginFadeOut();
				m_dayNightCycle->beginFadeIn();
            }
            else m_camera->update();

            // Gui Updates
            updateGuis(dt);
            if (m_displayLocker.compare("") != 0) m_displayInventory = true;
            m_guiUp = (m_displayDialogue || m_displayMenu || m_displayInventory || m_displayMachining || m_dayNightCycle->fadingIn() || m_dayNightCycle->fadingOut());

			if (m_itemToAdd.compare("") != 0) {
				if (m_timeToAdd.compare(m_dayNightCycle->fullTime()) == 0) {
					std::dynamic_pointer_cast<ItemLocker>(entity("purchasing"))->addToInventory(m_itemToAdd);
					m_itemToAdd = "";
					m_timeToAdd = "";
				}
			}

            // Game updates only if Gui is not up.
            if (!m_guiUp) {
				if (!m_pause) {
					m_dayNightCycle->resume();
					m_dayNightCycle->update();
				}
                updateEntities(dt);
                m_maps[m_currentMap]->update(dt);
				m_events->update();
            }
            else {
                m_dayNightCycle->pause();
                if (m_dayNightCycle->fadingIn()) m_dayNightCycle->fadeIn();
                if (m_dayNightCycle->fadingOut()) m_dayNightCycle->fadeOut();
            }

            if (m_mouseHeldItem) {
                m_mouseHeldItem->setPositionByPixel(sf::Mouse::getPosition().x, sf::Mouse::getPosition().y);
                m_mouseHeldItem->update(dt);
            }

            lag -= dt;
        }

        // Draw.
        m_window->clear();
        m_window->setView(m_camera->view());

        // Background
        m_maps[m_currentMap]->drawLayer("background");
        m_maps[m_currentMap]->drawLayer("obstacles");
        m_maps[m_currentMap]->drawLayer("overlap");

        // Entities
        drawEntities();

        // Foreground
        m_maps[m_currentMap]->drawLayer("foreground");
        m_maps[m_currentMap]->drawLayer("roof1");
        m_maps[m_currentMap]->drawLayer("roof2");
        m_maps[m_currentMap]->drawLayer("roof3");
        m_maps[m_currentMap]->drawLayer("roof4");
        m_maps[m_currentMap]->drawLayer("roof5");

        // Draw THE DARKNESS (GUITARS!)
		m_dayNightCycle->drawDarkness();

        // Gui
		m_window->setView(m_window->getDefaultView());
        drawGuis();
        if (m_mouseHeldItem) m_mouseHeldItem->draw("middle");

        // I will never understand why I have to do this.
        m_window->setView(m_camera->view());

        m_window->display();
    }

    tearDown();
}

void Game::handleInput(sf::Event event) {

    // Window events
    if (event.type == sf::Event::Closed)
        m_window->close();

    // Keyboard events
    if (event.type == sf::Event::KeyPressed) {
        switch (event.key.code) {
        case sf::Keyboard::E:
            if (m_displayInventory || !m_guiUp) {
                m_displayInventory = !m_displayInventory;
                std::dynamic_pointer_cast<Player>(m_entities["player"])->stopMovement();
            }
            break;
        case sf::Keyboard::Num1:
            if (!std::dynamic_pointer_cast<Player>(m_entities["player"])->usingItem()) {
                std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->setItemSlot(0);
            }
            break;
        case sf::Keyboard::Num2:
            if (!std::dynamic_pointer_cast<Player>(m_entities["player"])->usingItem()) {
                std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->setItemSlot(1);
            }
            break;
		case sf::Keyboard::Num3:
			if (!std::dynamic_pointer_cast<Player>(m_entities["player"])->usingItem()) {
				std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->setItemSlot(2);
			}
			break;
		case sf::Keyboard::Num4:
			if (!std::dynamic_pointer_cast<Player>(m_entities["player"])->usingItem()) {
				std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->setItemSlot(3);
			}
			break;
		case sf::Keyboard::Num5:
			if (!std::dynamic_pointer_cast<Player>(m_entities["player"])->usingItem()) {
				std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->setItemSlot(4);
			}
			break;
		case sf::Keyboard::Num6:
			if (!std::dynamic_pointer_cast<Player>(m_entities["player"])->usingItem()) {
				std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->setItemSlot(5);
			}
			break;
		case sf::Keyboard::Num7:
			if (!std::dynamic_pointer_cast<Player>(m_entities["player"])->usingItem()) {
				std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->setItemSlot(6);
			}
			break;
		case sf::Keyboard::Num8:
			if (!std::dynamic_pointer_cast<Player>(m_entities["player"])->usingItem()) {
				std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->setItemSlot(7);
			}
			break;
        case sf::Keyboard::T:
			entity("basketball")->setPositionByGrid(7, 15);
            break;
        default:
            if (!m_guiUp) std::dynamic_pointer_cast<Player>(m_entities["player"])->handleInput(event);
            break;
        }
    }
    if (event.type == sf::Event::KeyReleased) {
        switch (event.key.code) {
        default:
            if (!m_guiUp) std::dynamic_pointer_cast<Player>(m_entities["player"])->handleInput(event);
            break;
        }
    }

    // Mouse events
    if (event.type == sf::Event::MouseButtonPressed) {
        switch(event.mouseButton.button) {
        default:
            if (!m_guiUp && !m_displayComputer) std::dynamic_pointer_cast<Player>(m_entities["player"])->handleInput(event);
            break;
        }
    }
    if (event.type == sf::Event::MouseButtonReleased) {
        switch(event.mouseButton.button) {
        case sf::Mouse::Left:
			if (!m_guiUp && !m_displayComputer) std::dynamic_pointer_cast<Player>(m_entities["player"])->handleInput(event);
			else if (m_displayComputer) std::dynamic_pointer_cast<ComputerGui>(m_guis["computer"])->handleInput(event);
			else if (m_displayDialogue) std::dynamic_pointer_cast<Dialogue>(m_guis["dialogue"])->next();
			else if (m_displayMachining) std::dynamic_pointer_cast<MachiningGui>(m_guis["machining"])->handleInput(event);
            else if (m_displayLocker.compare("") == 0 && m_displayInventory) {
                m_mouseHeldItem = std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->handleClick(event, m_mouseHeldItem);
            }
            else if (m_displayLocker.compare("") != 0) {
                // We send the click to both the inventory and the locker because both should be
                // 'listening' for item movement at this point.
                m_mouseHeldItem = std::dynamic_pointer_cast<ItemLocker>(m_entities[m_displayLocker])->handleClick(event, m_mouseHeldItem);
                m_mouseHeldItem = std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->handleClick(event, m_mouseHeldItem);
            }
            break;
        case sf::Mouse::Right:
            if (!m_guiUp && !m_displayComputer) std::dynamic_pointer_cast<Player>(m_entities["player"])->handleInput(event);
            else if (m_displayLocker.compare("") != 0) {
                std::dynamic_pointer_cast<ItemLocker>(m_entities[m_displayLocker])->close();
            }
            break;
        default:
            break;
        }
    }
    if (event.type == sf::Event::MouseMoved) {
        
    }
    if (event.type == sf::Event::MouseWheelScrolled) {
		if (m_displayComputer) std::dynamic_pointer_cast<ComputerGui>(m_guis["computer"])->handleInput(event);
		else if (m_displayDialogue) std::dynamic_pointer_cast<Dialogue>(m_guis["dialogue"])->handleInput(event);
    }
}

void Game::tearDown() {
	m_entities.clear();
	m_guis.clear();
	m_fonts.clear();
	m_textures.clear();
	m_lockers.clear();
}

/////////////////////////////////////////////////////////////////////////////////////
//                           Helper Functions                                      //
/////////////////////////////////////////////////////////////////////////////////////

void Game::setCurrentMap(std::string whichMap) {
    if(m_maps.find(whichMap) == m_maps.end()) {
        std::cout << "Map does not exist: " << whichMap << std::endl;
    }
    else {
        m_newMap = whichMap;
    }
}

void Game::defineNpcBehaviors() {

    // ROBBY
    std::unordered_map<std::string, std::unordered_map<std::string, std::shared_ptr<Activity>>> robbyActivities;
    // Sunday
    robbyActivities["Sunday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(14,3), "sleep", DOWN);
    robbyActivities["Sunday"]["09:15"] = std::make_shared<Activity>("base", sf::Vector2i(19,15), "kitchen", RIGHT);
    robbyActivities["Sunday"]["10:00"] = std::make_shared<Activity>("base", sf::Vector2i(12,16), "chill", UP);
    robbyActivities["Sunday"]["12:30"] = std::make_shared<Activity>("base", sf::Vector2i(6,16), "basketball", UP);
    robbyActivities["Sunday"]["18:00"] = std::make_shared<Activity>("base", sf::Vector2i(19,15), "kitchen", RIGHT);
    robbyActivities["Sunday"]["21:30"] = std::make_shared<Activity>("upstairs", sf::Vector2i(14,3), "sleep", DOWN);

    // Weekdays
    robbyActivities["Monday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(14,3), "sleep", DOWN);
    robbyActivities["Monday"]["07:15"] = std::make_shared<Activity>("base", sf::Vector2i(19,15), "kitchen", RIGHT);
    robbyActivities["Monday"]["08:15"] = std::make_shared<Activity>("base", sf::Vector2i(28,11), "work", UP);
    robbyActivities["Monday"]["17:50"] = std::make_shared<Activity>("base", sf::Vector2i(17,15), "eat", DOWN);
    robbyActivities["Monday"]["21:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(14,3), "sleep", DOWN);
    robbyActivities["Tuesday"] = robbyActivities["Monday"];
    robbyActivities["Wednesday"] = robbyActivities["Monday"];
    robbyActivities["Thursday"] = robbyActivities["Monday"];
    robbyActivities["Friday"] = robbyActivities["Monday"];

    // Saturday
    robbyActivities["Saturday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(14,3), "sleep", DOWN);
    robbyActivities["Saturday"]["09:00"] = std::make_shared<Activity>("base", sf::Vector2i(19,15), "kitchen", RIGHT);
    robbyActivities["Saturday"]["12:30"] = std::make_shared<Activity>("base", sf::Vector2i(6,16), "basketball", UP);
    robbyActivities["Saturday"]["18:00"] = std::make_shared<Activity>("base", sf::Vector2i(32,20), "chill", DOWN);
    robbyActivities["Saturday"]["23:30"] = std::make_shared<Activity>("upstairs", sf::Vector2i(14,3), "sleep", DOWN);

    std::dynamic_pointer_cast<Npc>(m_entities["robby"])->giveSchedule(robbyActivities);

    // STELLA
	setStellaSchedule();

    // AL
    std::unordered_map<std::string, std::unordered_map<std::string, std::shared_ptr<Activity>>> alActivities;
    // Sunday
    alActivities["Sunday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(16,3), "sleep", DOWN);
    alActivities["Sunday"]["06:15"] = std::make_shared<Activity>("base", sf::Vector2i(19,15), "kitchen", RIGHT);
    alActivities["Sunday"]["09:15"] = std::make_shared<Activity>("base", sf::Vector2i(35,9), "bocce", DOWN);
    alActivities["Sunday"]["12:45"] = std::make_shared<Activity>("base", sf::Vector2i(13,16), "chill", UP);
    alActivities["Sunday"]["23:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(16,3), "sleep", DOWN);

    // Weekdays
    alActivities["Monday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(16,3), "sleep", DOWN);
    alActivities["Monday"]["06:15"] = std::make_shared<Activity>("base", sf::Vector2i(25,13), "work", LEFT);
    alActivities["Monday"]["15:00"] = std::make_shared<Activity>("base", sf::Vector2i(17,17), "eat", UP);
	alActivities["Monday"]["23:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(16, 3), "sleep", DOWN);
    alActivities["Tuesday"] = alActivities["Monday"];
    alActivities["Wednesday"] = alActivities["Monday"];
    alActivities["Wednesday"]["12:00"] = std::make_shared<Activity>("base", sf::Vector2i(35,9), "bocce", DOWN);
    alActivities["Wednesday"]["15:00"] = std::make_shared<Activity>("base", sf::Vector2i(35,9), "bocce", DOWN);
    alActivities["Thursday"] = alActivities["Monday"];
    alActivities["Friday"] = alActivities["Monday"];

    // Saturday
    alActivities["Sunday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(16,3), "sleep", DOWN);
    alActivities["Saturday"]["06:30"] = std::make_shared<Activity>("base", sf::Vector2i(19,15), "kitchen", DOWN);
    alActivities["Saturday"]["08:00"] = std::make_shared<Activity>("base", sf::Vector2i(13,16), "chill", UP);
    alActivities["Saturday"]["16:00"] = std::make_shared<Activity>("base", sf::Vector2i(17,17), "eat", UP);
    alActivities["Sunday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(16,3), "sleep", DOWN);

    std::dynamic_pointer_cast<Npc>(m_entities["al"])->giveSchedule(alActivities);

    // ANTONI
    std::unordered_map<std::string, std::unordered_map<std::string, std::shared_ptr<Activity>>> antoniActivities;
    // Sunday
    antoniActivities["Sunday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(8,3), "sleep", DOWN);
    antoniActivities["Sunday"]["12:00"] = std::make_shared<Activity>("base", sf::Vector2i(44,17), "read", RIGHT);
    antoniActivities["Sunday"]["15:00"] = std::make_shared<Activity>("base", sf::Vector2i(44,17), "wander", UP);
    antoniActivities["Sunday"]["21:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(8,3), "sleep", DOWN);

    // Weekdays
    antoniActivities["Monday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(8,3), "sleep", DOWN);
    antoniActivities["Monday"]["08:00"] = std::make_shared<Activity>("base", sf::Vector2i(31,14), "read", RIGHT);
    antoniActivities["Monday"]["13:00"] = std::make_shared<Activity>("base", sf::Vector2i(47,15), "play", RIGHT);
    antoniActivities["Monday"]["15:00"] = std::make_shared<Activity>("base", sf::Vector2i(18,17), "eat", UP);
    antoniActivities["Monday"]["19:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(8,3), "sleep", DOWN);
    antoniActivities["Tuesday"] = antoniActivities["Monday"];
    antoniActivities["Wednesday"] = antoniActivities["Monday"];
    antoniActivities["Thursday"] = antoniActivities["Monday"];
    antoniActivities["Friday"] = antoniActivities["Monday"];

    // Saturday
    antoniActivities["Saturday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(8,3), "sleep", DOWN);
    antoniActivities["Saturday"]["12:00"] = std::make_shared<Activity>("base", sf::Vector2i(23,25), "play", DOWN);
    antoniActivities["Saturday"]["16:00"] = std::make_shared<Activity>("base", sf::Vector2i(18,17), "eat", UP);
    antoniActivities["Saturday"]["19:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(8,3), "sleep", DOWN);

    std::dynamic_pointer_cast<Npc>(m_entities["antoni"])->giveSchedule(antoniActivities);
}

void Game::updateEntities(double dt) {
    std::unordered_map<std::string, std::shared_ptr<Entity> >::iterator itr;
    for (itr = m_entities.begin(); itr != m_entities.end(); itr++) {
		if (itr->second->currentMap().compare(m_currentMap) == 0
			|| itr->second->type().compare("npc") == 0) {
			itr->second->update(dt);
		}
    }
}

void Game::drawEntities() {
    std::unordered_map<std::string, std::shared_ptr<Entity> >::iterator itr;
    for (itr = m_entities.begin(); itr != m_entities.end(); itr++) {
        if(itr->second->currentMap().compare(m_currentMap) == 0) itr->second->draw("background");
    }
    for (itr = m_entities.begin(); itr != m_entities.end(); itr++) {
        if(itr->second->currentMap().compare(m_currentMap) == 0) itr->second->draw("middle");
    }
    for (itr = m_entities.begin(); itr != m_entities.end(); itr++) {
        if(itr->second->currentMap().compare(m_currentMap) == 0) itr->second->draw("foreground");
    }
}

void Game::updateGuis(double dt) {
    std::unordered_map<std::string, std::shared_ptr<Gui> >::iterator itr;
    for (itr = m_guis.begin(); itr != m_guis.end(); itr++) {
        itr->second->update(dt);
    }
}

void Game::drawGuis() {
    if (m_displayComputer && m_displayGui) m_guis["computer"]->draw();

    if (m_displayDialogue) m_guis["dialogue"]->draw();

	if (m_displayMachining && m_displayGui) m_guis["machining"]->draw();

    if (m_displayInventory) std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->expand();
    else std::dynamic_pointer_cast<InventoryGui>(m_guis["inventory"])->contract();
	if (m_displayGui) m_guis["inventory"]->draw();

    if (m_displayLocker.compare("") != 0 && m_displayGui) std::dynamic_pointer_cast<InventoryGui>(m_guis[m_displayLocker])->draw();

	if (m_displayGui) m_guis["timeweather"]->draw();
}

std::shared_ptr<Entity> Game::checkForEntityByGrid(int x, int y, std::string mapName, std::string butNotThisEntity) {

    std::unordered_map<std::string, std::shared_ptr<Entity> >::iterator itr;
    std::vector<std::shared_ptr<Entity>> foundEntities;
    for (itr = m_entities.begin(); itr != m_entities.end(); itr++) {
        if (itr->second->gridPosition() == sf::Vector2i(x, y) && itr->second->currentMap().compare(mapName) == 0) {
            foundEntities.push_back(std::dynamic_pointer_cast<Entity>(itr->second));
        }
    }

    switch(foundEntities.size()) {
    case 0:
        return NULL;
        break;
    case 1:
        return foundEntities[0];
        break;
    case 2:
        if (foundEntities[0]->name().compare(std::dynamic_pointer_cast<Player>(entity("player"))->currentToolName()) == 0) {
            return foundEntities[1];
        }
        else {
            if (foundEntities[0]->name().compare(butNotThisEntity) == 0) return foundEntities[1];
            else if (foundEntities[1]->name().compare(butNotThisEntity) == 0) return foundEntities[0];
            else return foundEntities[0];
        }
        break;
    default:
        std::cout << "More than 2 entities found, and you haven't told me how to deal with that!" << std::endl;
        return NULL;
        break;
    }

    return NULL;
}

bool Game::activateEventByGrid(int x, int y, std::string eventType) {
    if (eventType.compare("map") == 0) {
        if (m_events->activateMapEvent(x, y, m_currentMap)) return true;
        return false;
    }
    else if (eventType.compare("click") == 0) {
        if ( m_events->activateClickEvent(x, y, m_currentMap)) return true;
        return false;
    }
}

void Game::activateNpcEvent(int x, int y, std::string mapName) {
    m_events->activateNpcEvent(x, y, mapName);
}

void Game::showDialogue(std::string who, std::string what) {
    m_displayDialogue = true;
    std::dynamic_pointer_cast<Dialogue>(m_guis["dialogue"])->showDialogue(who, what);
}

void Game::showDialogueWithChoices(std::string who,
                               std::string what,
                               std::vector<Choice> choices) {
    m_displayDialogue = true;
    std::dynamic_pointer_cast<Dialogue>(m_guis["dialogue"])->showDialogueWithChoices(who, what, choices);
}

void Game::showPrewrittenDialogue(std::string dialogueName) {
	m_displayDialogue = true;
	std::dynamic_pointer_cast<Dialogue>(m_guis["dialogue"])->showPrewrittenDialogue(dialogueName);
}

void Game::showNpcDialogueOptions(std::string who) {
	m_displayDialogue = true;
	std::dynamic_pointer_cast<Dialogue>(m_guis["dialogue"])->showNpcDialogueOptions(who);
}

void Game::clearDialogue() {
    m_displayDialogue = false;
}

void Game::promptSleep() {
    if(m_promptSleep) return;

    std::dynamic_pointer_cast<Player>(m_entities["player"])->stopMovement();
    std::vector<Choice> choices;
    choices.push_back({"Yes", "save_game"});
    choices.push_back({"No", "cancel_dialogue"});
    showDialogueWithChoices("System", "Save and go to sleep?", choices);
    m_promptSleep = true;
}

void Game::cancelSleepPrompt() {
    m_promptSleep = false;
}

void Game::saveAndSleep() {
    advanceDay(true);
    m_saveLoad->saveFile(m_saveLoad->currentSaveSlot());
}

void Game::pauseWorld() {
	m_pause = true;
}

void Game::resumeWorld() {
	m_pause = false;
}

void Game::hideGui() {
	m_displayGui = false;
}

void Game::showGui() {
	m_displayGui = true;
}

void Game::advanceDay(bool becauseOfSleep) {
	completeStellaSchedule(m_dayNightCycle->dayOfWeek());
    m_dayNightCycle->advanceDay();
    m_maps["base"]->advanceDay();
	m_maps["woods"]->advanceDay();
	advanceEntities();
    std::dynamic_pointer_cast<ComputerGui>(gui("computer"))->advanceDay(m_dayNightCycle->fullDate());
	m_researchManager->advanceDay();
	setStellaSchedule();

    if (becauseOfSleep) {
        m_dayNightCycle->advanceToHour(6);
		m_events->activateTimeEvent(m_dayNightCycle->fullDate());
    }
}

void Game::advanceEntities() {
    std::unordered_map<std::string, std::shared_ptr<Entity> >::iterator itr;
    for (itr = m_entities.begin(); itr != m_entities.end(); itr++) {
		itr->second->advanceDay();
    }

    m_entities["robby"]->setCurrentMap("upstairs");
    m_entities["robby"]->setPositionByGrid(14, 3);
    m_entities["stella"]->setCurrentMap("upstairs");
    m_entities["stella"]->setPositionByGrid(10, 3);
    m_entities["al"]->setCurrentMap("upstairs");
    m_entities["al"]->setPositionByGrid(16, 3);
    m_entities["antoni"]->setCurrentMap("upstairs");
    m_entities["antoni"]->setPositionByGrid(8, 3);
}

void Game::holdItem(std::shared_ptr<Item> item) {
	m_mouseHeldItem = item;
}

void Game::dropHeldItem() {
    // TODO
    m_mouseHeldItem = NULL;
}

void Game::startComputer() {
    m_displayComputer = true;
}

void Game::closeComputer() {
    m_displayComputer = false;
}

void Game::openMachining() {
	m_displayMachining = true;
}

void Game::closeMachining() {
	m_displayMachining = false;
}

void Game::movePlayerToNextMap() {
	if (entity("player")) {
		entity("player")->setCurrentMap(m_newMap);
		std::shared_ptr<Tool> tool = std::dynamic_pointer_cast<Player>(entity("player"))->currentTool();
		if (tool) {
			tool->setCurrentMap(m_newMap);
		}
	}
	m_currentMap = m_newMap;
	m_dayNightCycle->resetDarkness();
}

void Game::completeStellaSchedule(std::string dayOfWeek) {
	std::unordered_map<std::string, std::shared_ptr<Activity>> activities = std::dynamic_pointer_cast<Npc>(entity("stella"))->getSchedule(dayOfWeek);

	std::unordered_map<std::string, std::shared_ptr<Activity>>::iterator itr;
	for (itr = activities.begin(); itr != activities.end(); itr++) {
		if (itr->second->m_activityName.compare("water") == 0) {
			map("base")->waterPlot(itr->second->m_gridPosition);
		}
		else if (itr->second->m_activityName.compare("weed") == 0) {
			map("base")->removeWeedsFromPlot(itr->second->m_gridPosition);
		}
		else if (itr->second->m_activityName.compare("fertilize") == 0) {
			// TODO
			//map("base")->waterPlot(itr->second->m_gridPosition);
		}
		else if (itr->second->m_activityName.compare("spray") == 0) {
			// TODO
			//map("base")->waterPlot(itr->second->m_gridPosition);
		}
	}
}

void Game::setStellaSchedule() {
	std::unordered_map<std::string, std::unordered_map<std::string, std::shared_ptr<Activity>>> stellaActivities;
	// Sunday
	stellaActivities["Sunday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(10, 3), "sleep", DOWN);
	stellaActivities["Sunday"]["08:30"] = std::make_shared<Activity>("base", sf::Vector2i(31, 11), "stand", UP);
	stellaActivities["Sunday"]["12:30"] = std::make_shared<Activity>("base", sf::Vector2i(34, 21), "wander", RIGHT);
	stellaActivities["Sunday"]["22:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(10, 3), "sleep", DOWN);

	// Weekdays
	stellaActivities["Monday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(10, 3), "sleep", DOWN);
	stellaActivities["Monday"]["07:00"] = std::make_shared<Activity>("base", sf::Vector2i(31, 11), "stand", UP);

	std::unordered_map<std::string, bool> farmActivities = std::dynamic_pointer_cast<ComputerGui>(gui("computer"))->stellaActivities();
	std::unordered_map<int, bool> farmPlots = std::dynamic_pointer_cast<ComputerGui>(gui("computer"))->stellaPlots();
	std::unordered_map<int, bool>::iterator itr;
	int activityNumber = 1;
	std::stringstream time;
	for (itr = farmPlots.begin(); itr != farmPlots.end(); itr++) {
		if (itr->second) {
			if (farmActivities["water"]) {
				int hour = 9 + 1 * (activityNumber - 1);
				if (hour <= 17) {
					time.str("");
					hour < 10 ? time << "0" << hour : time << hour;
					time << ":00";
					stellaActivities["Monday"][time.str()] = std::make_shared<Activity>("base", map("base")->farmPlotCenter(itr->first), "water", DOWN);
					activityNumber += STELLA_WORK_WATER_TIME;
				}
			}
			if (farmActivities["weed"]) {
				int hour = 9 + 1 * (activityNumber - 1);
				if (hour <= 17) {
					time.str("");
					hour < 10 ? time << "0" << hour : time << hour;
					time << ":00";
					stellaActivities["Monday"][time.str()] = std::make_shared<Activity>("base", map("base")->farmPlotCenter(itr->first), "weed", DOWN);
					activityNumber += STELLA_WORK_WEED_TIME;
				}
			}
			if (farmActivities["fertilize"]) {
				int hour = 9 + 1 * (activityNumber - 1);
				if (hour <= 17) {
					time.str("");
					hour < 10 ? time << "0" << hour : time << hour;
					time << ":00";
					stellaActivities["Monday"][time.str()] = std::make_shared<Activity>("base", map("base")->farmPlotCenter(itr->first), "fertilize", DOWN);
					activityNumber += STELLA_WORK_FERTILIZE_TIME;
				}
			}
			if (farmActivities["spray"]) {
				int hour = 9 + 1 * (activityNumber - 1);
				if (hour <= 17) {
					time.str("");
					hour < 10 ? time << "0" << hour : time << hour;
					time << ":00";
					stellaActivities["Monday"][time.str()] = std::make_shared<Activity>("base", map("base")->farmPlotCenter(itr->first), "spray", DOWN);
					activityNumber += STELLA_WORK_SPRAY_TIME;
				}
			}
		}
	}

	// Send Stella back to base when she's done with work
	int hour = 9 + 1 * (activityNumber - 1);
	if (hour < 20) {
		time.str("");
		hour < 10 ? time << "0" << hour : time << hour;
		time << ":00";
		stellaActivities["Monday"][time.str()] = std::make_shared<Activity>("base", sf::Vector2i(31, 11), "stand", UP);
	}

	stellaActivities["Monday"]["20:00"] = std::make_shared<Activity>("base", sf::Vector2i(18, 15), "eat", DOWN);
	stellaActivities["Monday"]["22:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(10, 3), "sleep", DOWN);
	stellaActivities["Tuesday"] = stellaActivities["Monday"];
	stellaActivities["Wednesday"] = stellaActivities["Monday"];
	stellaActivities["Thursday"] = stellaActivities["Monday"];
	stellaActivities["Friday"] = stellaActivities["Monday"];

	// Saturday
	stellaActivities["Saturday"]["00:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(10, 3), "sleep", DOWN);
	stellaActivities["Saturday"]["08:00"] = std::make_shared<Activity>("base", sf::Vector2i(30, 32), "wander", DOWN);
	stellaActivities["Saturday"]["12:00"] = std::make_shared<Activity>("base", sf::Vector2i(18, 15), "eat", DOWN);
	stellaActivities["Saturday"]["16:00"] = std::make_shared<Activity>("base", sf::Vector2i(34, 19), "chill", DOWN);
	stellaActivities["Saturday"]["23:00"] = std::make_shared<Activity>("upstairs", sf::Vector2i(10, 3), "sleep", DOWN);

	std::dynamic_pointer_cast<Npc>(m_entities["stella"])->giveSchedule(stellaActivities);
}

void Game::assignAl(std::shared_ptr<Machinable> machinable) {
	if (std::dynamic_pointer_cast<Player>(entity("player"))->currentFunds() < machinable->m_cost) {
		std::vector<Choice> choices;
		choices.push_back(Choice({ "Yes", "al_machining" }));
		choices.push_back(Choice({ "No thanks", "al_cancel" }));
		showDialogueWithChoices("al", "Can't afford that, kid. Somethin' else?", choices);
	}
	else if (!std::dynamic_pointer_cast<InventoryGui>(gui("inventory"))->contains(machinable->m_requirements)) {
		std::vector<Choice> choices;
		choices.push_back(Choice({ "Yes", "al_machining" }));
		choices.push_back(Choice({ "No thanks", "al_cancel" }));
		showDialogueWithChoices("al", "You don't have the materials. Somethin' else?", choices);
	}
	else if (std::dynamic_pointer_cast<ItemLocker>(entity("purchasing"))->full()) {
		showDialogue("al", "There ain't no room in the purchasing locker. Fix that and come back.");
	}
	else {
		std::stringstream ss;
		ss << "You got it. Should take me about " << machinable->m_hourCost << " hours. I'll put it in the purchasing locker.";
		showDialogue("al", ss.str());
		m_itemToAdd = machinable->name();
		m_timeToAdd = m_dayNightCycle->formatTime(m_dayNightCycle->hours() + machinable->m_hourCost, m_dayNightCycle->minutes());
		std::dynamic_pointer_cast<Player>(entity("player"))->addFunds(-1 * machinable->m_cost);
		std::vector<std::string>::iterator itr;
		for (itr = machinable->m_requirements.begin(); itr != machinable->m_requirements.end(); itr++) {
			std::dynamic_pointer_cast<InventoryGui>(gui("inventory"))->removeItemOfType(*itr);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////
//                           Getters and Setters                                   //
/////////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<sf::RenderWindow> Game::renderWindow() {
	return m_window;
}

std::shared_ptr<sf::Texture> Game::texture(std::string textureName) {
	if (m_textures.find(textureName) == m_textures.end()) {
		m_textures[textureName] = std::make_shared<sf::Texture>();
		std::stringstream texturePath;
		texturePath << IMAGES_PATH << textureName << ".png";
		if (m_textures[textureName]->loadFromFile(texturePath.str())) {
			return m_textures[textureName];
		}
		else return NULL;
	}
	else return m_textures[textureName];
}

std::shared_ptr<sf::Sound> Game::sound(std::string soundName) {
	if (m_sounds.find(soundName) == m_sounds.end()) {
		m_sounds[soundName] = std::make_shared<sf::Sound>();
		std::stringstream soundPath;
		soundPath << SOUNDS_PATH << soundName << ".wav";
		m_soundBuffers[soundName] = std::make_shared<sf::SoundBuffer>();
		if (m_soundBuffers[soundName]->loadFromFile(soundPath.str())) {
			m_sounds[soundName]->setBuffer(*m_soundBuffers[soundName]);
			return m_sounds[soundName];
		}
		else return NULL;
	}
	else return m_sounds[soundName];
}

std::shared_ptr<sf::Music> Game::music(std::string musicName) {
	if (m_musics.find(musicName) == m_musics.end()) {
		m_musics[musicName] = std::make_shared<sf::Music>();
		std::stringstream musicPath;
		musicPath << SOUNDS_PATH << musicName << ".ogg";
		if (m_musics[musicName]->openFromFile(musicPath.str())) {
			return m_musics[musicName];
		}
		else return NULL;
	}
	else return m_musics[musicName];
}

std::shared_ptr<sf::Font> Game::font(std::string fontName) {
	if (m_fonts.find(fontName) == m_fonts.end()) {
		m_fonts[fontName] = std::make_shared<sf::Font>();
		std::stringstream fontPath;
		fontPath << FONTS_PATH << fontName << ".ttf";
		if (m_fonts[fontName]->loadFromFile(fontPath.str())) {
			return m_fonts[fontName];
		}
		else return NULL;
	}
	else return m_fonts[fontName];
}

std::shared_ptr<Map> Game::map(std::string mapName) {
	if (m_maps.find(mapName) == m_maps.end()) {
		std::cout << "Map " << mapName << " does not exist!";
		return NULL;
	}
	else {
		return m_maps[mapName];
	}
}

std::shared_ptr<Entity> Game::entity(std::string entityName) {
	if (m_entities.find(entityName) != m_entities.end()) return m_entities[entityName];
	else {
		std::cout << "Entity " << entityName << " does not exist!";
		return NULL;
	}
}

void Game::addEntity(std::string entityName, std::shared_ptr<Entity> entity) {
	if (m_entities.find(entityName) != m_entities.end()) {
		std::cout << "Entity " << entityName << " cannot be added because it already exists!";
		return;
	}
	else {
		m_entities[entityName] = entity;
	}
}

std::string Game::addEntityWithoutName(std::shared_ptr<Entity> entity) {
	if (m_entityCount.find(entity->type()) == m_entityCount.end()) m_entityCount[entity->type()] = 0;
	m_entityCount[entity->type()]++;
	std::stringstream ss;
	ss << entity->type() << m_entityCount[entity->type()];
	m_entities[ss.str()] = entity;
	entity->rename(ss.str());
	return ss.str();
}

std::shared_ptr<Entity> Game::removeEntity(std::string name) {
	if (m_entities.find(name) == m_entities.end()) return NULL;
	std::shared_ptr<Entity> entity = m_entities[name];
	m_entities.erase(name);
	return entity;
}

std::shared_ptr<Gui> Game::gui(std::string guiName) {
	if (m_guis.find(guiName) != m_guis.end()) return m_guis[guiName];
	else {
		std::cout << "Gui " << guiName << " does not exist!";
		return NULL;
	}
}

std::shared_ptr<DayNight> Game::dayNight() {
	return m_dayNightCycle;
}

std::vector<std::shared_ptr<Crop>> Game::allPlantedCrops(std::string mapName) {
	return m_maps[mapName]->allPlantedCrops();
}

std::vector<std::pair<int, int>> Game::allWeeds(std::string mapName) {
	return m_maps[mapName]->allWeeds();
}

std::vector<std::pair<int, int>> Game::cutTrees(std::string mapName) {
	std::vector<std::pair<int, int>> trees;
	std::unordered_map<std::string, std::shared_ptr<Entity>>::iterator itr;
	for (itr = m_entities.begin(); itr != m_entities.end(); itr++) {
		if (itr->second->type().compare("tree") == 0
			&& itr->second->currentMap().compare(mapName) == 0
			&& std::dynamic_pointer_cast<Tree>(itr->second)->isCut()) {
			trees.push_back(std::pair<int,int>(itr->second->gridPosition().x, itr->second->gridPosition().y));
		}
	}

	return trees;
}

std::vector<std::shared_ptr<Entity>> Game::allPlaceables(std::string mapName) {
	std::vector<std::shared_ptr<Entity>> entities;
	std::unordered_map<std::string, std::shared_ptr<Entity>>::iterator itr;
	for (itr = m_entities.begin(); itr != m_entities.end(); itr++) {
		if (itr->second->type().compare("sprinkler") == 0
			&& itr->second->currentMap().compare(mapName) == 0) {
			entities.push_back(itr->second);
		}
	}

	return entities;
}

std::shared_ptr<Item> Game::heldItem() {
	return m_mouseHeldItem;
}

std::shared_ptr<ResearchManager> Game::researchManager() {
	return m_researchManager;
}

std::shared_ptr<Map> Game::currentMap() {
    return m_maps[m_currentMap];
}

sf::Vector2i Game::pixelsToGrid(float x, float y) {
    return m_maps[m_currentMap]->pixelsToGrid(x, y);
}

sf::Vector2f Game::gridToPixels(int x, int y) {
    return m_maps[m_currentMap]->gridToPixels(x, y);
}

const sf::Vector2i Game::mouseGridPosition() {
    return m_mouseGridPosition;
}

const sf::Vector2f Game::mousePixelPosition() {
    return m_mousePixelPosition;
}

const sf::Vector2f Game::mouseWorldPosition() {
	return m_mouseWorldCoords;
}

const float Game::mouseAngle() {
	return 90 + atan2f(mouseWorldPosition().y - entity("player")->pixelPosition().y, mouseWorldPosition().x - entity("player")->pixelPosition().x) * 180 / 3.14159;
}

const std::string Game::timeOfDayAsString() {
    return m_dayNightCycle->timeOfDay();
}

const std::string Game::dayOfWeek() {
    return m_dayNightCycle->dayOfWeek();
}

const std::string Game::date() {
    return m_dayNightCycle->fullDate();
}

const std::string Game::time() {
    return m_dayNightCycle->fullTime();
}

std::vector<std::shared_ptr<ItemLocker>> Game::allLockers() {
	return m_lockers;
}

void Game::displayLocker(std::string lockerName) {
    m_displayLocker = lockerName;
    if (m_displayLocker.compare("") == 0) m_displayInventory = false;
}

int Game::nextOpenSaveSlot() {
	return m_saveLoad->nextOpenSaveSlot();
}

const bool Game::displayingDialogue() {
	return m_displayDialogue;
}

const bool Game::guiVisible() {
	return m_displayGui;
}
