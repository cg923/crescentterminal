
// Copyright 2019 Corey Selover

#ifndef SAVE_LOAD_H
#define SAVE_LOAD_H

// Moon
class Game;

// STD
#include <memory>

class SaveLoad {
public:
    SaveLoad(std::shared_ptr<Game> game) : m_game(game), m_currentSaveSlot(-1) {}

    bool loadSaveFile(int slot);

    bool saveFile(int slot);

	int nextOpenSaveSlot();

	const int currentSaveSlot();

private:
	bool fileExists(int slotNumber);

    std::shared_ptr<Game> m_game;

	int m_currentSaveSlot;

};

#endif // SAVE_LOAD_H
