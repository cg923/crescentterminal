
// Copyright 2019 Happyrock Studios

#ifndef INVENTORY_GUI_H
#define INVENTORY_GUI_H

// Moon
#include "Gui.h"
class Item;
class Seed;

class ItemSlot {
public:
    ItemSlot(int slotNumber);

    ItemSlot(std::shared_ptr<Item> item, int slotNumber);

    void draw();

    std::shared_ptr<Item> put(std::shared_ptr<Item> item);

    std::shared_ptr<Item> item();

    void clearItem();

    const sf::Vector2f position();

    void setPosition(sf::Vector2f position);

    int slotNumber();

private:

    std::shared_ptr<Item> m_item;

    sf::Vector2f m_position;

    int m_slotNumber;

	sf::RectangleShape m_shape;
};

class InventoryGui : public Gui {
public:
    InventoryGui(std::shared_ptr<Game> game, 
		sf::Vector2f position, 
		std::string textureName = "inventory", 
		int maxItems = 16, 
		int rows = 2,
		sf::IntRect contractedRect = sf::IntRect(0,42,153,25),
		sf::IntRect expandedRect = sf::IntRect(0,0,153,42));

    void defineSprite();

    void update(double dt);

    void draw();

    //////////////////////////////////////////////////////
    //                  Helper Functions                //
    //////////////////////////////////////////////////////

	void expand();

	void contract();

    std::shared_ptr<Item> handleClick(sf::Event event, std::shared_ptr<Item> heldItem);

    std::shared_ptr<Item> addToInventory(std::shared_ptr<Item> item, int slot = -1);

	std::shared_ptr<Item> addToInventory(std::string item, int slot = -1);

	void addToInventory(std::vector<std::shared_ptr<Item>> items);

    std::shared_ptr<Item> removeFromInventory(int slot = -1);

	std::shared_ptr<Item> removeItemOfType(std::string itemType);

    unsigned int nextEmptySlot();

    int holdingItemType(std::string specificType);

    void useCurrentItem(sf::Vector2i focusLocation);

    void resort();

    int count(std::string itemType);

	bool contains(std::vector<std::string> items);

	int totalPriceOfItems();

	void clear();

	bool full();

    //////////////////////////////////////////////////////
    //               Getters and Setters                //
    //////////////////////////////////////////////////////

    void setItemSlot(int itemSlot);

    int getItemSlot(float mouseX, float mouseY);

    std::vector<std::shared_ptr<ItemSlot>> allItemSlots();

	std::vector<std::shared_ptr<Item>> allItems();

    void setDisplayText(std::string text);

    void setDisplayBelowText(std::string text);

	sf::Vector2f position();

protected:

    const int SLOT_WIDTH = 16;
    const int SLOT_HEIGHT = 16;

	std::string m_textureName;

    sf::Text m_displayText;
    sf::Text m_displayBelowText;

	sf::IntRect m_contractedRect;
	sf::IntRect m_expandedRect;

    std::vector<std::shared_ptr<ItemSlot>> m_items;

    unsigned int m_maxItems;
    unsigned int m_currentItem;
	unsigned int m_rows;

	bool m_expanded;
};

#endif // INVENTORY_GUI_H
