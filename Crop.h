
// Copyright 2019 Happyrock Studios

#ifndef CROP_H
#define CROP_H

// Moon
class Game;
class Yield;

// STD
#include <memory>

// SFML
#include <SFML/Graphics.hpp>

enum Crops {
    CROP_ASPARAGUS,
    CROP_BEETS,
    CROP_BOKCHOY,
    CROP_CARROTS,
    CROP_GARLIC,
    CROP_LEEKS,
    CROP_ONIONS,
    CROP_POTATOES,
    CROP_NONE
};

enum GrowthStage {
    GS_SEED,
    GS_BABY,
    GS_GROWN
};

enum CropQuality {
    CQ_POOR,
    CQ_FAIR,
    CQ_GOOD,
    CQ_EXCELLENT
};

class Crop {
public:
    Crop(Crops cropType, sf::Vector2i gridPosition, std::shared_ptr<Game> game, int tileWidth, int tileHeight);

    void update(double dt);

    void draw();

    ////////////////////////////////////////////////////////////////////////
    //                       Helper Functions                             //
    ////////////////////////////////////////////////////////////////////////

    void plant(Crops cropType);

    void ageUp(int days = 1);

    std::shared_ptr<Yield> harvest();

	void wilt();


    ////////////////////////////////////////////////////////////////////////
    //                       Getters/Setters                              //
    ////////////////////////////////////////////////////////////////////////

    const Crops type();

    void setAge(int age);

    int age();

    sf::Vector2i gridPosition();

    bool readyForHarvest();

protected:
    Crops m_cropType;
    GrowthStage m_stage;
	CropQuality m_quality;
    std::shared_ptr<Game> m_game;
    sf::Vector2i m_gridPosition;
    sf::IntRect m_textureRect;
    sf::Sprite m_sprite;
    unsigned int m_ageInDays;
    unsigned int m_ripeByDay;
    bool m_harvested;
};

#endif // CROP_H
