
// Copyright 2019 Happyrock Studios

#ifndef MAP_H
#define MAP_H

// Moon
class Game;
#include "Crop.h"

// STD
#include <memory>
#include <vector>
#include <unordered_map>

// Other libs
#include "libraries/pugixml.hpp"

// SFML
#include <SFML/Graphics.hpp>

const int TILE_SET_FILE_WIDTH = 320;
const int TILE_SET_FILE_HEIGHT = 640;

struct Tile {
    sf::Vector2f m_pixelPosition;
    sf::Vector2i m_gridPosition;
    sf::IntRect m_textureRect;
    sf::Sprite m_wateredSprite;
	sf::Sprite m_weedSprite;
    bool m_exists;
    bool m_farmable;
    bool m_tilled;
    bool m_watered;
	bool m_weeds;
	bool m_isWater;
    std::shared_ptr<Crop> m_crop;
};

typedef std::vector<std::vector<std::shared_ptr<Tile> > > Layer;

class Map {
public:
    Map(std::string name, std::string filePath, std::shared_ptr<Game> game);

    void update(double dt);

    void drawLayer(std::string layerName);

    ////////////////////////////////////////////////////////////////////////
    //                      Helper Functions                              //
    ////////////////////////////////////////////////////////////////////////

    void advanceDay();

    bool checkForObstacleByPixel(float x, float y);

    bool checkForObstacleByGrid(int x, int y);

    sf::Vector2i pixelsToGrid(float x, float y);

    sf::Vector2f gridToPixels(int x, int y);

    std::vector<sf::Vector2i> validNeighborsOf(sf::Vector2i tile);

    void tillByGrid(int x, int y);

    bool plantByGrid(int x, int y, Crops cropType);

    void waterByGrid(int x, int y);

	void waterPlot(sf::Vector2i position);

	void waterPlot(int plotNumber);

	void addWeedByGrid(int x, int y);

	bool removeWeedByGrid(int x, int y);

	void removeWeedsFromPlot(sf::Vector2i position);

	void removeWeedsFromPlot(int plotNumber);

    bool readyForHarvest(int x, int y);

    void updateTilledSprite(int x, int y);

    void updateWateredSprite(int x, int y);

    void setAgeOfCropAtTile(int x, int y, int age);

    void makeTileAnObstacle(int x, int y);

    void makeTileNotAnObstacle(int x, int y);

    void hideLayer(std::string layerName);

    void showLayer(std::string layerName);

    ////////////////////////////////////////////////////////////////////////
    //                      Getters and Setters                           //
    ////////////////////////////////////////////////////////////////////////

    const std::string name();

    sf::IntRect dimensions();

    sf::Vector2i mapSize();

    sf::Vector2i tileDimensions();

    std::shared_ptr<Tile> tileByGrid(std::string layerName, int x, int y);

    std::shared_ptr<Tile> tileByPixels(std::string layerName, int x, int y);

    std::vector<std::shared_ptr<Crop>> allPlantedCrops();

	std::vector<std::pair<int, int>> allWeeds();

	std::vector<std::shared_ptr<Tile>> adjacentFarmableTiles(sf::Vector2i position);

	sf::Vector2i farmPlotCenter(int plot);

	int numberOfPlots();


private:
    void makeFarmable(pugi::xml_node layer);

	void designatePlot(pugi::xml_node layer, std::string layerName);

    void placeObjects(pugi::xml_node layer);

	std::string m_name;
    std::shared_ptr<Game> m_game;

    std::unordered_map<std::string, Layer> m_layers;
    std::unordered_map<std::string, bool> m_layerVisibility;
    std::unordered_map<std::string, sf::VertexArray> m_vertexArrays;

	std::unordered_map<int, std::vector<sf::Vector2i>> m_farmPlots;
	std::unordered_map<int, sf::Vector2i> m_farmPlotCenters;

    sf::Texture m_tileSet;
    sf::Texture m_wateredTexture;
	sf::Texture m_weedTexture;

    int m_mapWidth;
    int m_mapHeight;
    int m_tileWidth;
    int m_tileHeight;
    int m_tileSetWidth;
    int m_tileSetHeight;
};

#endif // MAP_H
