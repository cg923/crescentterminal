
// Copyright 2019 Corey Selover

#ifndef HOOP_H
#define HOOP_H

// Moon
#include "Entity.h"

class Hoop : public Entity {
public:
	Hoop(std::string mapName, std::shared_ptr<Game> game, int gridX, int gridY);

	void update(double dt);

private:
	std::shared_ptr<sf::Sprite> m_baseSprite;

	sf::Vector2i m_initGridPos;

	bool m_init;
};


#endif