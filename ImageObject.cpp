
// Copyright 2019 Corey Selover

#include "ImageObject.h"
#include "Game.h"

#include <iostream>

ImageObject::ImageObject(std::string name, std::string texturePath, std::string currentMap, std::shared_ptr<Game> game, int spriteWidth, int spriteHeight)
	: Entity(name, "image", texturePath, currentMap, game, spriteWidth, spriteHeight) {
	loadSprite(spriteWidth, spriteHeight);
}

void ImageObject::update(double dt) {
	Entity::update(dt);
}

void ImageObject::setPositionByGrid(int x, int y) {
	m_gridPosition = sf::Vector2i(x, y);
	m_position = m_game->gridToPixels(x, y);

	// Adjust for sprite origin
	m_position.x -= 12;
	m_position.y -= 16;
}