
// Copyright 2019 Happyrock Studios

#ifndef NPC_H
#define NPC_H

// Moon
#include "Character.h"

const int HOSTILE = 9;
const int HATE = 19;
const int DISLIKE = 29;
const int AMBIVALENT = 49;
const int LIKE = 69;
const int LOVE = 89;
const int MAX_FRIENDSHIP_LEVEL = 100;

const int STELLA_WORK_STOP_TIME = 17;
const int STELLA_WORK_WATER_TIME = 1;
const int STELLA_WORK_WEED_TIME = 2;
const int STELLA_WORK_FERTILIZE_TIME = 1;
const int STELLA_WORK_SPRAY_TIME = 1;

struct Activity {
    Activity(std::string mapName, sf::Vector2i gridPosition, std::string activityName, Direction direction)
        : m_mapName(mapName), m_gridPosition(gridPosition), m_activityName(activityName), m_direction(direction) {}
    std::string m_mapName;
    sf::Vector2i m_gridPosition;
    std::string m_activityName;
    Direction m_direction;
};

class Npc : public Character {
public:
    Npc(std::string name,
        std::string texturePath,
        std::string currentMap,
        Direction direction,
        int friendshipLevel,
        std::shared_ptr<Game> game);

    void update(double dt);

    std::string whatDialogue();

    const int friendshipLevel();

    void setFriendshipLevel(int level);

    void giveSchedule(std::unordered_map<std::string, std::unordered_map<std::string, std::shared_ptr<Activity>>> schedule);

	std::unordered_map<std::string, std::shared_ptr<Activity>> getSchedule(std::string dayOfWeek);

    const bool spokeMorning();

    const bool spokeAfternoon();

    const bool spokeEvening();

    void advanceDay();

    void facePlayer();

	std::string getStatus();

private:

	void doCurrentActivity();

    int m_friendshipLevel;

    bool m_spokeMorning;

    bool m_spokeAfternoon;

    bool m_spokeEvening;

    std::unordered_map<std::string, std::unordered_map<std::string, std::shared_ptr<Activity>>> m_schedule;
    std::shared_ptr<Activity> m_currentActivity;
    std::shared_ptr<Activity> m_nextActivity;

    sftools::Chronometer m_facePlayerClock;
    bool m_facePlayer;
};
#endif // NPC_H
