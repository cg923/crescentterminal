
// Copyright 2019 Happyrock Studios

/*
    An Entity is an object that moves around in the game world.
    It has a sprite, a position, a name, and other physical
    properties.  By default it has no behavior and an Entity
    is merely a base class for things like characters, monsters,
    items, etc.
*/

#ifndef ENTITY_H
#define ENTITY_H

// Moon
#include "libraries/Chronometer.hpp"

// STD
#include <memory>
#include <unordered_map>

// SFML
#include <SFML/Graphics.hpp>

const float ANIMATION_SPEED = 0.15f;

class Game;

enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN,
    ULEFT,
    URIGHT,
    DLEFT,
    DRIGHT
};

struct Animation {
    std::string m_name;
    sf::IntRect m_frameInfo;
    int         m_numberOfFrames;
    bool        m_repeats;
    int         m_currentFrame;
	float		m_animationSpeed;
};

class Entity : public std::enable_shared_from_this<Entity> {
public:
    Entity(std::string name,
           std::string type,
           std::string texturePath,
           std::string currentMap,
           std::shared_ptr<Game> game,
           int spriteWidth,
           int spriteHeight);

    virtual void loadSprite(int spriteWidth, int spriteHeight);

    virtual void defineAnimations() {}

    void scaleSprites(float xScale, float yScale);

    virtual void defineHitbox();

    virtual void update(double dt);

    virtual void draw(std::string layerName);

    virtual void setAnimation(std::string animName);

    virtual void animate();

	virtual void advanceDay() {}

	virtual void activate() {}

    //////////////////////////////////////////////////
    //             Helper Functions                 //
    //////////////////////////////////////////////////

    bool clickedOn(float x, float y, bool respond = false);

	void shake(float amount);

	void fadeOut();

	void fadeIn();

    //////////////////////////////////////////////////
    //            Getters and Setters               //
    //////////////////////////////////////////////////

    std::shared_ptr<Entity> thisEntity() { return shared_from_this(); }

	void rename(std::string newName);

    const std::string name();

    const std::string type();

    const std::string texturePath();

    virtual void setCurrentMap(std::string newMap);

    const std::string currentMap();

    virtual void setPositionByGrid(int x, int y);

    virtual void setPositionByGrid(sf::Vector2i vec);

    virtual void setPositionByPixel(float x, float y);

    virtual void setPositionByPixel(sf::Vector2f vec);

    void setSpritesPosition(sf::Vector2f vec);

	sf::Vector2f spriteScale();

    const sf::Vector2f pixelPosition();

    const sf::Vector2i gridPosition();

    std::shared_ptr<sf::Sprite> sprite();

	const Direction direction();

	void setDirection(Direction direction);

	void setDirection(std::string directionString);

	void setVisibility(bool visible);

	const bool visible();

protected:
    std::string m_name;
    std::string m_type;
    std::string m_texturePath;

    sf::Vector2f m_position;
    sf::Vector2i m_gridPosition;

    sf::IntRect m_hitbox;
    sf::RectangleShape m_hitboxSprite;
    sf::Vector2i m_hitboxOffset;

    Direction m_direction;

    std::unordered_map<std::string, std::vector<std::shared_ptr<sf::Sprite>>> m_sprites;
    std::shared_ptr<sf::Sprite> m_sprite;
	sf::Vector2f m_spriteScale;
	bool m_visible;
	bool m_fadingIn;
	bool m_fadingOut;

    std::string m_currentMap;
    std::shared_ptr<Game> m_game;

    std::string m_currentAnimation;
    std::unordered_map<std::string, Animation> m_animations;
    sftools::Chronometer m_animationClock;

    bool m_obstacle;

	float m_shakeAmount;
};
#endif // ENTITY_H
