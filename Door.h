
// Copyright 2019 Happyrock Studios

#ifndef DOOR_H
#define DOOR_H

// Moon
#include "Entity.h"

class Door : public Entity {
public:
    Door(std::string currentMap, std::shared_ptr<Game> game, std::string owner);

    void update(double dt);

    void unlock();

    void lock();

    void open();

    void close();

    bool someoneInDoorWay();

    bool someoneTryingToOpen();

    const std::string owner();

    void setOwner(std::string owner);

private:
    sftools::Chronometer m_openCloseTimer;
    bool m_open;

    bool m_locked;

    std::string m_owner;
};
#endif // DOOR_H
