
// Copyright 2019 Happyrock Studios

// Moon
#include "Tool.h"
#include "Game.h"
#include "Map.h"
#include "InventoryGui.h"
#include "Tree.h"
#include "Player.h"

// STD
#include <iostream>

Tool::Tool(std::string name,
           std::string texturePath,
           std::string currentMap,
           std::shared_ptr<Game> game,
           ItemQuality quality,
           int spriteWidth, int
           spriteHeight)
    : Item(name, "tool", "UNDEFINED", texturePath, 1, currentMap, game, spriteWidth, spriteHeight) {

    m_quality = quality;
    m_inUse = false;
    m_held = false;
    m_sprites["middle"].push_back(m_sprite);
    m_sprite->scale(0.5, 0.5);
	defineAnimations();
    repair();
}

void Tool::defineAnimations() {
	sf::IntRect texRect = m_sprite->getTextureRect();
	m_animations["stopped_down"] = Animation{ "stopped_down", sf::IntRect(texRect.width * 3, 0, texRect.width, texRect.height), 1, true, 1, 1 };
	m_animations["stopped_up"] = Animation{ "stopped_up", sf::IntRect(texRect.width * 2, 0, texRect.width, texRect.height), 1, true, 1, 1 };
	m_animations["stopped_left"] = Animation{ "stopped_left", sf::IntRect(texRect.width * 1, 0, texRect.width, texRect.height), 1, true, 1, 1 };
	m_animations["stopped_right"] = Animation{ "stopped_right", sf::IntRect(0, 0, texRect.width, texRect.height), 1, true, 1, 1 };
	m_animations["moving_down"] = m_animations["stopped_down"];
	m_animations["moving_up"] = m_animations["stopped_up"];
	m_animations["moving_left"] = m_animations["stopped_left"];
	m_animations["moving_right"] = m_animations["stopped_right"];
	m_animations["working_down"] = m_animations["stopped_down"];
	m_animations["working_up"] = m_animations["stopped_up"];
	m_animations["working_left"] = m_animations["stopped_left"];
	m_animations["working_right"] = m_animations["stopped_right"];
}

void Tool::updateSprite() {
    Item::updateSprite();

    m_quantityText.setString("");

    if(!m_held) {
        m_sprite->setPosition(m_position);
		switch (m_direction) {
		case ULEFT:
		case DLEFT:
		case LEFT:
			setAnimation("stopped_left");
			break;
		case URIGHT:
		case DRIGHT:
		case RIGHT:
			setAnimation("stopped_right");
			break;
		case UP:
			setAnimation("stopped_up");
			break;
		case DOWN:
			setAnimation("stopped_down");
			break;
		default:
			break;
		}
    }
	else {
		bool walking = std::dynamic_pointer_cast<Player>(m_game->entity("player"))->walking();
		switch (m_direction) {
		case ULEFT:
		case DLEFT:
		case LEFT:
			m_sprite->setPosition(m_position - sf::Vector2f(28, -8));
			if (walking && !m_inUse) setAnimation("moving_left");
			else if (m_inUse) setAnimation("working_left");
			else setAnimation("stopped_left");
			break;
		case URIGHT:
		case DRIGHT:
		case RIGHT:
			m_sprite->setPosition(m_position + sf::Vector2f(22, 8));
			if (walking && !m_inUse) setAnimation("moving_right");
			else if (m_inUse) setAnimation("working_right");
			else setAnimation("stopped_right");
			break;
		case UP:
			m_sprite->setPosition(m_position - sf::Vector2f(4, 8));
			if (walking && !m_inUse) setAnimation("moving_up");
			else if (m_inUse) setAnimation("working_up");
			else setAnimation("stopped_up");
			break;
		case DOWN:
			m_sprite->setPosition(m_position + sf::Vector2f(-4, 22));
			if (walking && !m_inUse) setAnimation("moving_down");
			else if (m_inUse) setAnimation("working_down");
			else setAnimation("stopped_down");
			break;
		default:
			break;
		}
	}
}

void Tool::update(double dt) {
    Item::update(dt);

    if (m_inUse) m_gridPosition = m_game->pixelsToGrid(m_position.x + m_sprite->getTextureRect().width / 2,
                                          m_position.y + m_sprite->getTextureRect().height);
}

void Tool::draw(std::string layerName) {
    if (!m_held) {
        Item::draw(layerName);
    }
    else return;
}

void Tool::drawFromPlayer(std::string layerName) {
    if (m_held) {
        Item::draw(layerName);
    }
    else return;
}

void Tool::use(sf::Vector2i gridPosition) {
    m_inUse = true;
	m_usePosition = gridPosition;
}

void Tool::stop() {
    m_inUse = false;
}

void Tool::repair() {
    switch(m_quality) {
    case IQ_BROKEN:
        m_maxDurability = 10;
        break;
    case IQ_POOR:
        m_maxDurability = 50;
        break;
    case IQ_FAIR:
        m_maxDurability = 100;
        break;
    case IQ_GOOD:
        m_maxDurability = 200;
        break;
    case IQ_HIGH:
        m_maxDurability = 500;
        break;
    default:
        break;
    }

    m_currentDurability = m_maxDurability;
}

void Tool::improve() {
    switch(m_quality) {
    case IQ_BROKEN:
        m_quality = IQ_POOR;
        break;
    case IQ_POOR:
       m_quality = IQ_FAIR;
        break;
    case IQ_FAIR:
        m_quality = IQ_GOOD;
        break;
    case IQ_GOOD:
        m_quality = IQ_HIGH;
        break;
    default:
        break;
    }

    repair();
}

void Tool::hold() {
    m_held = true;
}

void Tool::letGo() {
    m_held = false;
}

void Tool::setPositionByGrid(int x, int y) {
	Entity::setPositionByGrid(x, y);

	m_position.x -= m_sprite->getTextureRect().width / 2;
	m_position.y -= m_sprite->getTextureRect().height / 2;

	m_inInventory = false;
}

void Tool::setPositionByGrid(sf::Vector2i vec) {
	Entity::setPositionByGrid(vec);

	m_position.x -= m_sprite->getTextureRect().width / 2;
	m_position.y -= m_sprite->getTextureRect().height / 2;

	m_inInventory = false;
}

void Tool::setDirection(Direction direction) {
    m_direction = direction;
}

const bool Tool::inUse() {
    return m_inUse;
}

//////////////////////////////////////////////////////
//                  Rototiller                      //
//////////////////////////////////////////////////////

Rototiller::Rototiller(std::shared_ptr<Game> game)
    : Tool("rototiller", "rototiller", "base", game, IQ_FAIR, 28, 33) {
    m_maxGas = m_maxDurability;
    m_currentGas = m_maxGas;

    m_direction = LEFT;
    m_maxSpeed = 0.3;

    m_specificType = "rototiller";

	defineAnimations();
}

void Rototiller::defineAnimations() {
	Tool::defineAnimations();
	sf::IntRect texRect = m_sprite->getTextureRect();
	m_animations["moving_right"] = Animation{ "moving_right", sf::IntRect(0, texRect.height, texRect.width, texRect.height), 2, true, 1, 1 };
	m_animations["moving_left"] = Animation{ "moving_left", sf::IntRect(texRect.width * 2, texRect.height, texRect.width, texRect.height), 2, true, 1, 1 };
	m_animations["working_left"] = Animation{ "working_left", sf::IntRect(texRect.width * 2, texRect.height, texRect.width, texRect.height), 2, true, 1, 2 };
	m_animations["working_right"] = Animation{ "working_right", sf::IntRect(0, texRect.height, texRect.width, texRect.height), 2, true, 1, 2 };
}

void Rototiller::update(double dt) {
	Tool::update(dt);
	if (m_inUse) m_game->currentMap()->tillByGrid(m_gridPosition.x, m_gridPosition.y);
}

void Rototiller::repair() {
    Tool::repair();
    m_currentGas = m_maxGas;
}

//////////////////////////////////////////////////////
//                  Seedspreader                    //
//////////////////////////////////////////////////////

Seedspreader::Seedspreader(std::shared_ptr<Game> game)
    : Tool("seedspreader", "seedspreader", "base", game, IQ_FAIR, 28, 33) {
    m_loadedSeedSprite.setTexture(*m_game->texture("seedspreader"));
    m_loadedSeedSprite.setTextureRect(sf::IntRect(112, 0, 10, 9));
    m_loadedSeedSprite.setOrigin(5, 5);

    m_direction = LEFT;
    m_maxSpeed = 0.5;
    m_specificType = "seedspreader";
}

void Seedspreader::update(double dt) {
    Tool::update(dt);
    if (m_inUse && m_loadedSeed && m_loadedSeed->quantity() > 0) {
        if (m_game->currentMap()->plantByGrid(m_gridPosition.x, m_gridPosition.y, m_loadedSeed->seedType())) {
            m_loadedSeed->addToQuantity(-1);
        }
    }

    if (m_direction == UP) m_loadedSeedSprite.setPosition(m_sprite->getPosition() + sf::Vector2f(16, 11));
    if (m_direction == DOWN) m_loadedSeedSprite.setPosition(m_sprite->getPosition() + sf::Vector2f(16, 19));
}

void Seedspreader::drawFromPlayer(std::string layerName) {
    Tool::drawFromPlayer(layerName);

    if (layerName.compare("middle") != 0 ) return;

    if (m_loadedSeed && m_loadedSeed->quantity() > 0 && (m_direction == UP || m_direction == DOWN)) {
        m_game->renderWindow()->draw(m_loadedSeedSprite);
    }
}

std::shared_ptr<Seed> Seedspreader::loadSeed(std::shared_ptr<Seed> seedsToLoad) {
    if (!m_inUse) {
        // If switching seed type, send held seeds back to player
        if (m_loadedSeed) {
            if (m_loadedSeed->seedType() == seedsToLoad->seedType()) {
                int remainder = m_loadedSeed->addToQuantity(seedsToLoad->quantity());
                if (remainder > 0) {
                    return std::make_shared<Seed>(seedsToLoad->seedType(), remainder, m_currentMap, m_game);
                }
                else return NULL;
            }
            else {
                std::shared_ptr<Seed> oldSeed = m_loadedSeed;
                m_loadedSeed = seedsToLoad;
                if (oldSeed->quantity() > 0) return oldSeed;
                else return NULL;
            }
        }
        else {
            m_loadedSeed = seedsToLoad;
        }
    }

    return NULL;
}

//////////////////////////////////////////////////////
//                  Watering Can                    //
//////////////////////////////////////////////////////

Wateringcan::Wateringcan(std::shared_ptr<Game> game)
    : Tool("wateringcan", "wateringcan", "base", game, IQ_FAIR, 28, 33) {
    m_direction = DOWN;
    m_specificType = "wateringcan";
}

void Wateringcan::use(sf::Vector2i gridPosition) {
    m_game->currentMap()->waterByGrid(gridPosition.x, gridPosition.y);
}

void Wateringcan::stop() {
    return;
}

//////////////////////////////////////////////////////
//                  Chainsaw	                    //
//////////////////////////////////////////////////////

Chainsaw::Chainsaw(std::shared_ptr<Game> game)
	: Tool("chainsaw", "chainsaw", "base", game, IQ_FAIR, 28, 33) {
	m_direction = DOWN;
	m_specificType = "chainsaw";
}

void Chainsaw::update(double dt) {
	Tool::update(dt);
	if (m_inUse) {
		std::shared_ptr<Entity> entity = m_game->checkForEntityByGrid(m_usePosition.x, m_usePosition.y, m_currentMap);
		if (entity) {
			if (entity->type().compare("tree") == 0) {
				std::dynamic_pointer_cast<Tree>(entity)->cut();
			}
		}
	}
}

//////////////////////////////////////////////////////
//                  Basketball	                    //
//////////////////////////////////////////////////////

Basketball::Basketball(std::shared_ptr<Game> game, sf::Vector2f position)
	: Tool("basketball", "basketball", "base", game, IQ_FAIR, 16, 16) {
	m_direction = DOWN;
	m_specificType = "basketball";
	scaleSprites(0.75, 0.75);
	m_charge = 0;
	m_target = position;
	m_shootPos = position;
	m_shootAngle = 0;
}

void Basketball::use(sf::Vector2i gridPosition) {
	Tool::use(gridPosition);
}

void Basketball::stop() {
	letGo();
	shoot(m_game->mouseAngle() * 3.14159 / 180, m_charge);
	Tool::stop();
}

void Basketball::shoot(float angle, int magnitude) {
	m_shootPos = m_position;
	m_shootAngle = angle;
	m_target = m_position + sf::Vector2f(magnitude * sin(angle), -1 * magnitude * cos(angle));
}

void Basketball::update(double dt) {
	Tool::update(dt);

	if (m_inUse) {
		m_charge = std::min(m_charge + 2, 200);
	}
	else m_charge = 0;

	if (m_position != m_target) {

		// Because we're working with floats, it is unlikely that the values will exactly match,
		// so we give some buffer room.
		if (abs(m_target.x - m_position.x) < abs(m_target.x - m_shootPos.x) * 0.1) { // && abs(m_target.y - m_position.y) < abs(m_target.y - m_shootPos.y) * 0.1) {
			m_target = m_position;
			m_shootPos = m_position;
		}

		m_position.x += (m_target.x - m_position.x) / 70;
		m_position.y += (m_target.y - m_position.y) / 70;

		float adjust = abs(sin(m_shootAngle)) * -0.05 * pow(m_position.x - m_shootPos.x, 2) + 2 * (abs(m_position.x - m_shootPos.x)) + 3;
		m_sprite->setPosition(m_sprite->getPosition().x, m_sprite->getPosition().y - adjust);
	}
}

void Basketball::updateSprite() {
	Item::updateSprite();

	m_quantityText.setString("");

	if (!m_held) {
		m_sprite->setPosition(m_position);
		return;
	}

	sf::IntRect currentRect = m_sprite->getTextureRect();
	switch (m_direction) {
	case ULEFT:
	case DLEFT:
	case LEFT:
		m_sprite->setPosition(m_position - sf::Vector2f(5, -12));
		break;
	case URIGHT:
	case DRIGHT:
	case RIGHT:
		m_sprite->setPosition(m_position + sf::Vector2f(18, 12));
		break;
	case UP:
		m_sprite->setPosition(m_position - sf::Vector2f(-6, 0));
		break;
	case DOWN:
		m_sprite->setPosition(m_position + sf::Vector2f(6, 18));
		break;
	default:
		break;
	}
}

void Basketball::setPositionByGrid(int x, int y) {
	Tool::setPositionByGrid(x, y);

	m_target = m_position;
	m_shootPos = m_position;
}

