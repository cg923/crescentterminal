
// Copyright 2019 Happyrock Studios

// Moon
#include "Camera.h"
#include "Game.h"
#include "Entity.h"
#include "Map.h"

Camera::Camera(std::shared_ptr<Game> game, std::string entityName) {
    m_game = game;
    m_view = m_game->renderWindow()->getDefaultView();
    m_zoomFactor = 1;
    zoom(0.35);
    setTargetEntity(entityName);
}

void Camera::update() {
    sf::Vector2f targetPos = m_game->entity(m_targetEntityName)->pixelPosition();
    sf::Vector2f windowSize = m_zoomFactor * sf::Vector2f(m_game->renderWindow()->getSize());
    sf::IntRect mapBoundaries = m_game->currentMap()->dimensions();

    float x;
    float y;

    // X
    if (targetPos.x <= windowSize.x / 2) x = windowSize.x / 2;
    else if (targetPos.x >= mapBoundaries.width - windowSize.x / 2) x = mapBoundaries.width - windowSize.x / 2;
    else x = targetPos.x;

    // Y
    if (targetPos.y <= windowSize.y / 2) y = windowSize.y / 2;
    else if (targetPos.y >= mapBoundaries.height - windowSize.y / 2) y = mapBoundaries.height - windowSize.y / 2;
    else y = targetPos.y;

    // Small maps
    if (mapBoundaries.width <= windowSize.x) x = mapBoundaries.width / 2;
    if (mapBoundaries.height <= windowSize.y) y = mapBoundaries.height / 2;

    m_view.setCenter(sf::Vector2f(x, y));
}

void Camera::zoom(float factor) {
    m_zoomFactor *= factor;
    m_view.zoom(factor);
}

void Camera::setTargetEntity(std::string entityName) {
    m_targetEntityName = entityName;
    update();
}

const sf::View Camera::view() {
    return m_view;
}
