
// Copyright 2019 Happyrock Studios

// Moon
#include "InventoryGui.h"
#include "Game.h"
#include "Tool.h"

// STD

#include <iostream>
#include <queue>

ItemSlot::ItemSlot(int slotNumber) {
    m_item = NULL;
    m_position = sf::Vector2f();
    m_slotNumber = slotNumber;
}

ItemSlot::ItemSlot(std::shared_ptr<Item> item, int slotNumber) {
    m_item = item;
    m_slotNumber = slotNumber;
}

void ItemSlot::draw() {
    if (m_item) m_item->draw("middle");
}

std::shared_ptr<Item> ItemSlot::put(std::shared_ptr<Item> item) {
    if (m_item && m_item->specificType().compare(item->specificType()) == 0) {
        if (m_item->quantity() + item->quantity() > MAX_ITEM_COUNT) {
            item->addToQuantity(-1 * (MAX_ITEM_COUNT - m_item->quantity()));
            m_item->setQuantity(MAX_ITEM_COUNT);
            return item;
        }
        else {
            m_item->addToQuantity(item->quantity());
            return NULL;
        }
    }
    else if (m_item) {
        std::shared_ptr<Item> oldItem = m_item;
        m_item = item;
        return oldItem;
    }
    else {
        m_item = item;
        return NULL;
    }
}

std::shared_ptr<Item> ItemSlot::item() {
    return m_item;
}

void ItemSlot::clearItem() {
    m_item = NULL;
}

const sf::Vector2f ItemSlot::position() {
    return m_position;
}

void ItemSlot::setPosition(sf::Vector2f position) {
    m_position = position;
}

int ItemSlot::slotNumber() {
    return m_slotNumber;
}

//////////////////////////////////////////////////////////////////////////
//                          Inventory Gui                               //
//////////////////////////////////////////////////////////////////////////

InventoryGui::InventoryGui(std::shared_ptr<Game> game, 
	sf::Vector2f position, 
	std::string textureName, 
	int maxItems, 
	int rows,
	sf::IntRect contractedRect,
	sf::IntRect expandedRect)
    : Gui(sf::Vector2f(), game) {

    m_maxItems      = maxItems;
    m_currentItem   = 0;
	m_rows = rows;
	m_textureName = textureName;
	m_expanded = false;
	m_contractedRect = contractedRect;
	m_expandedRect = expandedRect;
    m_position = position;
    defineSprite();
    m_displayText = Gui::buildText(*m_game->font("dialogue"), 30, true);
    m_displayBelowText = Gui::buildText(*m_game->font("dialogue"), 30, true);

	int perRow = m_maxItems / m_rows;

    for (int i = 0; i < m_maxItems; i++) {
        m_items.push_back(std::make_shared<ItemSlot>(i));
		m_items[i]->setPosition(position - m_sprite.getScale().x * m_sprite.getOrigin() +
			m_sprite.getScale().x * sf::Vector2f(4, 4) + // Center in slot
			m_sprite.getScale().x * sf::Vector2f(i % perRow * SLOT_WIDTH, 0) + // Column
			m_sprite.getScale().x * sf::Vector2f(2 * (i % perRow), 0) + // row spacing
			m_sprite.getScale().y * sf::Vector2f(0, int(i / perRow)) + // column spacing
			m_sprite.getScale().y * sf::Vector2f(0, int(i / perRow) * SLOT_HEIGHT)); // Row
    }
}

void InventoryGui::defineSprite() {
    m_sprite.setTexture(*m_game->texture(m_textureName));
    m_sprite.setPosition(m_position);
    m_sprite.scale(3, 3);
    m_sprite.setOrigin(m_sprite.getTexture()->getSize().x / 2, 0);
}

void InventoryGui::update(double dt) {
    for (unsigned int i = 0; i < m_maxItems; i++) {
        if (m_items[i]->item() && m_items[i]->item()->quantity() > 0) {
            m_items[i]->item()->update(dt);
            m_items[i]->item()->setPositionByPixel(m_items[i]->position());
            m_items[i]->item()->sprite()->setPosition(m_items[i]->item()->pixelPosition());
        }
        else if (m_items[i]->item() && m_items[i]->item()->quantity() <= 0) {
            m_items[i]->item()->update(dt);
        }
    }

    m_displayText.setPosition(m_position - sf::Vector2f(m_displayText.getLocalBounds().width / 2, 80));
    m_displayBelowText.setPosition(m_position - sf::Vector2f(m_displayBelowText.getLocalBounds().width / 2, - 110));
}

void InventoryGui::draw() {
    m_game->renderWindow()->draw(m_sprite);

	if (m_expanded) {
		std::vector<std::shared_ptr<ItemSlot>>::iterator itr;
		for (itr = m_items.begin(); itr != m_items.end(); itr++) {
			(*itr)->draw();
		}
		m_game->renderWindow()->draw(m_displayText);
		m_game->renderWindow()->draw(m_displayBelowText);
	}
	else {
		for (int i = 0; i < int(m_maxItems / m_rows); i++) {
			m_items[i]->draw();
		}
	}
}

//////////////////////////////////////////////////////
//                  Helper Functions                //
//////////////////////////////////////////////////////

void InventoryGui::expand() {
	m_sprite.setTextureRect(m_expandedRect);
	m_expanded = true;
}

void InventoryGui::contract() {
	m_sprite.setTextureRect(m_contractedRect);
	m_expanded = false;
}

std::shared_ptr<Item> InventoryGui::handleClick(sf::Event event, std::shared_ptr<Item> heldItem) {
    sf::Vector2f mouseCoords = sf::Vector2f(event.mouseButton.x, event.mouseButton.y);
    int clickedSlot = getItemSlot(mouseCoords.x, mouseCoords.y);
    if ( clickedSlot > -1 && heldItem) {
        return addToInventory(heldItem, getItemSlot(mouseCoords.x, mouseCoords.y));
    }
    else if (clickedSlot > -1) {
        return removeFromInventory(clickedSlot);
    }
    else return heldItem;
}

std::shared_ptr<Item> InventoryGui::addToInventory(std::shared_ptr<Item> item, int slot) {
    if (slot >= 0 && slot < m_maxItems) {
		item->setAsInInventory();
        std::shared_ptr<Item> oldItem = m_items[slot]->put(item);
        return oldItem;
    }
    else {
        int itemExistsInSlot = holdingItemType(item->specificType());
        if (itemExistsInSlot > -1) {
			item->setAsInInventory();
            m_items[itemExistsInSlot]->put(item);
			return NULL;
        }
        else if (nextEmptySlot() < m_maxItems) {
			item->setAsInInventory();
            m_items[nextEmptySlot()]->put(item);
            return NULL;
        }
        else {
            return item;
        }
    }
}

std::shared_ptr<Item> InventoryGui::addToInventory(std::string item, int slot) {
	if (item.compare("sprinkler") == 0) {
		return addToInventory(std::make_shared<Sprinkler>("base", m_game), slot);
	}
	else {
		std::cout << "I don't know how to create item of type: " << item << std::endl;
	}
}

void InventoryGui::addToInventory(std::vector<std::shared_ptr<Item>> items) {
	std::vector<std::shared_ptr<Item>>::iterator itr;
	for (itr = items.begin(); itr != items.end(); itr++) {
		if (nextEmptySlot() < m_maxItems) {
			(*itr)->setAsInInventory();
			m_items[nextEmptySlot()]->put(*itr);
		}	
	}
}

std::shared_ptr<Item> InventoryGui::removeFromInventory(int slot) {
    if (slot >= 0 && slot < m_maxItems) {
        std::shared_ptr<Item> oldItem = m_items[slot]->item();
        m_items[slot]->clearItem();
        return oldItem;
    }
    else {
        std::shared_ptr<Item> oldItem = m_items[m_currentItem]->item();
        m_items[m_currentItem]->clearItem();
        return oldItem;
    }
}

std::shared_ptr<Item> InventoryGui::removeItemOfType(std::string type) {
	for (unsigned int i = 0; i < m_maxItems; i++) {
		if (m_items[i]->item() && m_items[i]->item()->type().compare(type) == 0)
			return removeFromInventory(i);
	}
}

unsigned int InventoryGui::nextEmptySlot() {
    for (unsigned int i = 0; i < m_maxItems; i++) {
        if (!m_items[i]->item()) return i;
    }

    return m_maxItems;
}

int InventoryGui::holdingItemType(std::string specificType) {
    for (unsigned int i = 0; i < m_maxItems; i++) {
        if (m_items[i]->item() && m_items[i]->item()->specificType().compare(specificType) == 0) return i;
    }

    return -1;
}

void InventoryGui::useCurrentItem(sf::Vector2i focusLocation) {
    std::shared_ptr<Item> currentItem = m_items[m_currentItem]->item();
    if (currentItem) {
        currentItem->use(focusLocation);
    }
}

void InventoryGui::resort() {
    std::queue<std::shared_ptr<Item>> heldItems;
    std::vector<std::shared_ptr<ItemSlot>>::iterator itr;
    for (itr = m_items.begin(); itr != m_items.end(); itr++) {
        if ((*itr)->item()) {
            heldItems.push((*itr)->item());
        }
        (*itr)->clearItem();
    }

    for (int unsigned i = 0; i < m_maxItems; i++) {
        if(heldItems.size() > 0) {
            m_items[i]->put(heldItems.front());
            heldItems.pop();
        }
    }
}

int InventoryGui::count(std::string itemType) {
    int numItems = 0;
    for (unsigned int i = 0; i < m_maxItems; i++) {
        if (m_items[i]->item() && m_items[i]->item()->specificType().compare(itemType) == 0) numItems += m_items[i]->item()->quantity();
    }

    return numItems;
}

bool InventoryGui::contains(std::vector<std::string> items) {
	bool contains = false;
	if (items.size() == 0) return true;
	std::vector<std::string>::iterator itr;
	for (itr = items.begin(); itr != items.end(); itr++) {
		if (count(*itr) > 0) contains = true;
		else contains = false;
	}

	return contains;
}

int InventoryGui::totalPriceOfItems() {
	int totalPrice = 0;
	for (unsigned int i = 0; i < m_maxItems; i++) {
		if (m_items[i]->item()) {
			totalPrice += m_items[i]->item()->price() * m_items[i]->item()->quantity();
		}
	}

	return totalPrice;
}

void InventoryGui::clear() {
	for (unsigned int i = 0; i < m_maxItems; i++) {
		m_items[i]->clearItem();
	}
}

bool InventoryGui::full() {
	return nextEmptySlot() == m_maxItems;
}

//////////////////////////////////////////////////////
//                  Getters & Setters               //
//////////////////////////////////////////////////////

void InventoryGui::setItemSlot(int itemSlot) {
    m_currentItem = itemSlot;
}

int InventoryGui::getItemSlot(float mouseX, float mouseY) {
	// TODO - This should behave differently based on whether the gui is collapsed or expanded
    for (unsigned int i = 0; i < m_maxItems; i++) {
        if (mouseX >= m_items[i]->position().x
            && mouseX <= m_items[i]->position().x + SLOT_WIDTH*m_sprite.getScale().x
            && mouseY >= m_items[i]->position().y
            && mouseY <= m_items[i]->position().y + SLOT_HEIGHT*m_sprite.getScale().y) {
                return i;
        }
    }

    return -1;
}

std::vector<std::shared_ptr<ItemSlot>> InventoryGui::allItemSlots() {
    return m_items;
}

std::vector<std::shared_ptr<Item>> InventoryGui::allItems() {
	std::vector<std::shared_ptr<Item>> items;
	std::vector< std::shared_ptr<ItemSlot>>::iterator itr;
	for (itr = m_items.begin(); itr != m_items.end(); itr++) {
		if ((*itr)->item()) items.push_back((*itr)->item());
	}

	return items;
}

void InventoryGui::setDisplayText(std::string text) {
    m_displayText.setString(text);
}

void InventoryGui::setDisplayBelowText(std::string text) {
    m_displayBelowText.setString(text);
}

sf::Vector2f InventoryGui::position() {
	return m_position;
}


