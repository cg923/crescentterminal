
// Copyright 2019 Happyrock Studios

// Moon
#include "Gui.h"

// STD
#include <vector>
#include <unordered_map>

const sf::Vector2f NAME_OFFSET = sf::Vector2f(30, 20);
const sf::Vector2f DIALOGUE_OFFSET = sf::Vector2f(30, 90);

struct Choice {
    std::string m_option;
    std::string m_result;
};

struct SingleDialogue {
    std::string m_who;
    std::string m_what;
    std::vector<Choice> m_choices;
};

class Dialogue : public Gui {
public:
    Dialogue(std::shared_ptr<Game> game);

    void defineSprite();

    void update(double dt);

    void draw();

    /////////////////////////////////////////////////////////////
    //                  Helper Functions                       //
    /////////////////////////////////////////////////////////////

	void writeDialogues();

    void showDialogue(std::string who, std::string what);

    void showDialogueWithChoices(std::string who, std::string what, std::vector<Choice> choices);

	void showPrewrittenDialogue(std::string dialogueName);

	void showNpcDialogueOptions(std::string who);

    void next();

    void handleChoice(std::string choice);

	void handleInput(sf::Event event);

    std::string resultOfClickedOption();

	std::string convertToMultiline(std::string str);

    /////////////////////////////////////////////////////////////
    //               Getters and Setters                       //
    /////////////////////////////////////////////////////////////

    const std::string currentString();

private:
	const int MAX_CHARS_PER_LINE = 50;

    sf::Text m_text;
    sf::Text m_speakerText;

    std::vector<sf::Text> m_choiceTexts;
	std::vector<sf::Text> m_aboveChoiceTexts;
	std::vector<sf::Text> m_belowChoiceTexts;
    sf::RectangleShape m_choiceSelection;
	sf::Sprite m_upArrowSprite;
	sf::Sprite m_downArrowSprite;

    std::vector<SingleDialogue> m_dialoguesToDisplay;

    std::string m_currentString;
    std::string m_currentSpeaker;

    bool m_displayChoices;
    int m_selectedChoice;

	std::unordered_map<std::string, SingleDialogue> m_prewrittenDialogues;
};

