
// Copyright 2019 Happyrock Studios

// Moon
#include "Hoop.h"
#include "Game.h"
#include "Map.h"

// STD
#include <iostream>

Hoop::Hoop(std::string mapName, std::shared_ptr<Game> game, int gridX, int gridY)
	: Entity("hoop", "hoop", "hoop", mapName, game, 32, 96) {

	m_baseSprite = std::make_shared<sf::Sprite>();
	m_baseSprite->setTexture(*m_game->texture(m_texturePath));
	m_baseSprite->setPosition(m_position);
	m_baseSprite->setTextureRect(sf::IntRect(32, 0, 32, 96));

	m_sprites["background"].push_back(m_baseSprite);
	m_sprites["foreground"].push_back(m_sprite);

	m_initGridPos = sf::Vector2i(gridX, gridY);

	m_init = false;
}

void Hoop::update(double dt) {
	Entity::update(dt);

	// Calling either of these functions in the constructor breaks, so we have to do this.
	if (!m_init) {
		setPositionByGrid(m_initGridPos);
		m_game->map(m_currentMap)->makeTileAnObstacle(m_initGridPos.x, m_initGridPos.y);
		m_init = true;
	}

	m_sprite->setPosition(m_position - sf::Vector2f(16, 77));
	m_baseSprite->setPosition(m_position - sf::Vector2f(16, 77));
}
