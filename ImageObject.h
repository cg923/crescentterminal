
// Copyright 2019 Corey Selover

// This class is for placing things in the world that do absolutely nothing. 
// They are images, essentially, but cannot be manipulated like a tileset.

#ifndef IMAGE_OBJECT_H
#define IMAGE_OBJECT_H

#include "Entity.h"

class ImageObject : public Entity {
public:
	ImageObject(std::string name,
		std::string texturePath,
		std::string currentMap,
		std::shared_ptr<Game> game,
		int spriteWidth,
		int spriteHeight);

	void update(double dt);

	void setPositionByGrid(int x, int y);
};

#endif
