
// Copyright 2019 Happyrock Studios

#ifndef EVENT_H
#define EVENT_H

#include <string>
#include <vector>
#include <unordered_map>

struct Command {
    std::string who;
    std::string what;
    std::string where;
};

class Event {
public:
    Event(std::string name,
          std::string type,
          std::vector<std::string> commands,
          bool repeat,
		  bool immediate = false);

    void parse(std::vector<std::string> commands);

    std::vector<Command> execute();

    void reset();

    const std::string name();

	const bool immediate();

	void setImmediate(bool immediate);

protected:
    std::string m_name;

    std::string m_type;

    std::vector<Command> m_commands;

    bool m_activated;

    bool m_repeat;

	bool m_immediate;
};
#endif // EVENT_H
