
// Copyright 2019 Happyrock Studios

#ifndef MACHINING_GUI_H
#define MACHINING_GUI_H

// Moon
#include "Gui.h"

// STD
#include <unordered_map>

class Machinable : public Clickable {
public:
	Machinable(std::string name, bool unlocked, int cost, int hourCost, sf::IntRect texRect, std::vector<std::string> requirements);
	bool m_unlocked;
	int m_cost;
	int m_hourCost;
	sf::IntRect m_textureRect;
	sf::Sprite m_sprite;
	std::vector<std::string> m_requirements;
};

class MachiningGui : public Gui {
public:
	MachiningGui(std::shared_ptr<Game> game);

	void defineSprite();

	void update(double dt);

	void draw();

	void handleInput(sf::Event event);

	void handleClick(sf::Event event);

	void machine(std::string itemName);

	void unlock(std::string itemName);

	//////////////////////////////////////////////////////
	//                  Helper Functions                //
	//////////////////////////////////////////////////////


	//////////////////////////////////////////////////////
	//               Getters and Setters                //
	//////////////////////////////////////////////////////

private:
	std::unordered_map<std::string, std::shared_ptr<Machinable>> m_machinables;

	std::string m_itemToMachine;

	bool m_displayButtons;

	sf::Text m_costText;

	std::shared_ptr<Clickable> m_confirmClickable;
	sf::Sprite m_confirmSprite;

	std::shared_ptr<Clickable> m_cancelClickable;
	sf::Sprite m_cancelSprite;
};

#endif // MACHINING_GUI_H

