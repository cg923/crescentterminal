
// Copyright 2019 Happyrock Studios

// Moon
#include "Dialogue.h"
#include "Game.h"
#include "Npc.h"
#include "ResearchManager.h"

// STD
#include <iostream>
#include <sstream>

Dialogue::Dialogue(std::shared_ptr<Game> game)
    : Gui(sf::Vector2f(), game) {
    defineSprite();

    m_currentString = "";
    m_currentSpeaker = "";
    m_displayChoices = false;
    m_selectedChoice = 0;

	writeDialogues();
}

void Dialogue::defineSprite() {
    m_sprite.setTexture(*m_game->texture("dialogue"));
    m_position = sf::Vector2f(m_game->renderWindow()->getSize().x / 2, m_game->renderWindow()->getSize().y - m_sprite.getTexture()->getSize().y - 100);
    m_sprite.setPosition(m_position);
    m_sprite.scale(2, 2);
    m_sprite.setOrigin(m_sprite.getTexture()->getSize().x / 2, m_sprite.getTexture()->getSize().y / 2);

    m_text = buildText(*m_game->font("dialogue"));
    m_text.setPosition(m_position + DIALOGUE_OFFSET - m_sprite.getScale().x * m_sprite.getOrigin());

    m_speakerText = buildText(*m_game->font("dialogue"));
    m_speakerText.setPosition(m_position + NAME_OFFSET - m_sprite.getScale().x * m_sprite.getOrigin());

    m_choiceSelection.setOutlineThickness(2);
    m_choiceSelection.setOutlineColor(sf::Color::Blue);
    m_choiceSelection.setFillColor(sf::Color::Transparent);
    m_choiceSelection.setSize(sf::Vector2f(200, 35));

	m_upArrowSprite.setTexture(*m_game->texture("arrow"));
	m_upArrowSprite.setPosition(m_position + DIALOGUE_OFFSET - m_sprite.getScale().x * m_sprite.getOrigin() + sf::Vector2f(0, 44));

	m_downArrowSprite.setTexture(*m_game->texture("arrow"));
	m_downArrowSprite.rotate(180);
	m_downArrowSprite.setPosition(m_position + DIALOGUE_OFFSET - m_sprite.getScale().x * m_sprite.getOrigin() + sf::Vector2f(16, 138));
}

void Dialogue::update(double dt) {
    Gui::update(dt);

    if (m_dialoguesToDisplay.size() > 0) {
        m_currentString = convertToMultiline(m_dialoguesToDisplay[0].m_what);
        m_currentSpeaker = m_dialoguesToDisplay[0].m_who;
        m_currentSpeaker[0] = toupper(m_currentSpeaker[0]);

        // Display choices if there are any.
        if (m_dialoguesToDisplay[0].m_choices.size() > 0) {
            m_displayChoices = true;
            if (m_choiceTexts.size() == 0) {
                for (int i = 0; i < m_dialoguesToDisplay[0].m_choices.size(); i++) {
					if (i < 3) {
						m_choiceTexts.push_back(buildText(*m_game->font("dialogue")));
						m_choiceTexts[i].setString(m_dialoguesToDisplay[0].m_choices[i].m_option);
						m_choiceTexts[i].setPosition(sf::Vector2f(m_text.getPosition().x + 20, m_text.getPosition().y + 35 * (i + 1)));
					}
					else {
						m_belowChoiceTexts.push_back(buildText(*m_game->font("dialogue")));
						m_belowChoiceTexts[i-3].setString(m_dialoguesToDisplay[0].m_choices[i].m_option);
					}

                }
            }

            // Update choice selection rectangle
            sf::Vector2f mouseCoords = m_game->mousePixelPosition();
            if (mouseCoords.y < m_choiceTexts[0].getPosition().y + m_choiceTexts[0].getGlobalBounds().height) {
                m_selectedChoice = 0;
                m_choiceSelection.setPosition(m_choiceTexts[0].getPosition());
				m_choiceSelection.setSize(sf::Vector2f(m_choiceTexts[0].getLocalBounds().width + 6, m_choiceTexts[0].getLocalBounds().height + 6));
            }
            else if (mouseCoords.y >= m_choiceTexts[m_choiceTexts.size() - 1].getPosition().y) {
                m_selectedChoice = m_choiceTexts.size() - 1;
                m_choiceSelection.setPosition(m_choiceTexts[m_choiceTexts.size() - 1].getPosition());
				m_choiceSelection.setSize(sf::Vector2f(m_choiceTexts[m_choiceTexts.size() - 1].getLocalBounds().width + 6, m_choiceTexts[m_choiceTexts.size() - 1].getLocalBounds().height + 6));
            }
            else {
                for (int i = 0; i < m_choiceTexts.size() - 1; i++) {
                    if (mouseCoords.y >= m_choiceTexts[i].getPosition().y
                        && mouseCoords.y < m_choiceTexts[i].getPosition().y + m_choiceTexts[i].getGlobalBounds().height) {
                            m_selectedChoice = i;
                            m_choiceSelection.setPosition(m_choiceTexts[i].getPosition());
							m_choiceSelection.setSize(sf::Vector2f(m_choiceTexts[i].getLocalBounds().width + 6, m_choiceTexts[i].getLocalBounds().height + 6));
                        }
                }
            }
        }
        else {
            m_choiceTexts.clear();
			m_aboveChoiceTexts.clear();
			m_belowChoiceTexts.clear();
        }
    } else {
        m_currentString = "";
        m_currentSpeaker = "";
    }
    m_text.setString(m_currentString);
    m_speakerText.setString(m_currentSpeaker);
}

void Dialogue::draw() {
    Gui::draw();
    m_game->renderWindow()->draw(m_text);
    m_game->renderWindow()->draw(m_speakerText);
    for (int i = 0; i < m_choiceTexts.size(); i++) {
        m_game->renderWindow()->draw(m_choiceTexts[i]);
    }
    if (m_displayChoices) {
        m_game->renderWindow()->draw(m_choiceSelection);
		if (m_aboveChoiceTexts.size() > 0) m_game->renderWindow()->draw(m_upArrowSprite);
		if (m_belowChoiceTexts.size() > 0) m_game->renderWindow()->draw(m_downArrowSprite);
    }
}

void Dialogue::writeDialogues() {
	// Robby
	std::vector<Choice> choices;
	choices.push_back({ "How are you?", "robby_status" });
	choices.push_back({ "Let's talk research.", "robby_research" });
	choices.push_back({ "Nevermind.", "cancel_dialogue" });
	m_prewrittenDialogues["robby_greeting"] = { "robby", "What's up?", choices };

	choices.clear();
	choices.push_back({ "How are you?", "stella_status" });
	choices.push_back({ "Nevermind.", "cancel_dialogue" });
	m_prewrittenDialogues["stella_greeting"] = { "stella", "Yes, Doctor?", choices };

	choices.clear();
	choices.push_back({ "How are you?", "al_status" });
	choices.push_back({ "Can you make something?", "al_machining" });
	choices.push_back({ "Nevermind.", "cancel_dialogue" });
	m_prewrittenDialogues["al_greeting"] = { "al", "Yup?", choices };

	choices.clear();
	choices.push_back({ "How are you?", "antoni_status" });
	choices.push_back({ "Nevermind.", "cancel_dialogue" });
	m_prewrittenDialogues["antoni_greeting"] = { "antoni", "What?", choices };

	// Prewritten event dialogues
	choices.clear();
	choices.push_back({ "I'm all set.", "intro_1_allset" });
	choices.push_back({ "Got anything to eat?", "intro_1_eat" });
	choices.push_back({ "Where is everyone?", "intro_1_where" });
	m_prewrittenDialogues["intro1"] = { "robby", "Need anything?", choices };
}

void Dialogue::showDialogue(std::string who, std::string what) {
    m_dialoguesToDisplay.push_back({who, what});
}

void Dialogue::showDialogueWithChoices(std::string who, std::string what, std::vector<Choice> choices) {
    m_dialoguesToDisplay.push_back({who, what, choices});
}

void Dialogue::showPrewrittenDialogue(std::string dialogueName) {
	if (m_prewrittenDialogues.find(dialogueName) != m_prewrittenDialogues.end()) {
		if (m_prewrittenDialogues[dialogueName].m_choices.size() > 0) {
			showDialogueWithChoices(m_prewrittenDialogues[dialogueName].m_who, m_prewrittenDialogues[dialogueName].m_what, m_prewrittenDialogues[dialogueName].m_choices);
		}
		else showDialogue(m_prewrittenDialogues[dialogueName].m_who, m_prewrittenDialogues[dialogueName].m_what);
	}
}

void Dialogue::showNpcDialogueOptions(std::string who) {
	std::stringstream ss;
	ss << who << "_greeting";
	m_dialoguesToDisplay.push_back({ who, m_prewrittenDialogues[ss.str()].m_what, m_prewrittenDialogues[ss.str()].m_choices });
}

void Dialogue::next() {
    if (m_displayChoices) {
		m_choiceTexts.clear();
		m_aboveChoiceTexts.clear();
		m_belowChoiceTexts.clear();
        std::string result = resultOfClickedOption();
        if (result.compare("cancel_dialogue") == 0) {
            m_dialoguesToDisplay.clear();
            m_displayChoices = false;
        }
        else {
            handleChoice(result);
			m_displayChoices = false;
        }
    }
    if (m_dialoguesToDisplay.size() > 0) {
        m_dialoguesToDisplay.erase(m_dialoguesToDisplay.begin());
    }
    if (m_dialoguesToDisplay.size() == 0) {
        m_display = false;
		m_displayChoices = false;
        m_game->clearDialogue();
    }
}

void Dialogue::handleChoice(std::string choice) {
    if (choice.compare("save_game") == 0) {
        m_game->saveAndSleep();
    }
	else if (m_prewrittenDialogues.find(choice) != m_prewrittenDialogues.end()) {
		if (m_prewrittenDialogues[choice].m_choices.size() > 0) {
			showDialogueWithChoices(m_prewrittenDialogues[choice].m_who, m_prewrittenDialogues[choice].m_what, m_prewrittenDialogues[choice].m_choices);
		}
		else showDialogue(m_prewrittenDialogues[choice].m_who, m_prewrittenDialogues[choice].m_what);
	}
	else if (choice.find("_status") != std::string::npos) {
		std::string who = choice.substr(0, choice.find("_"));
		std::string what = std::dynamic_pointer_cast<Npc>(m_game->entity(who))->getStatus();
		showDialogue(who, what);
	}
	else if (choice.compare("robby_research") == 0) {
		std::string currentResearch = m_game->researchManager()->currentResearchName();
		if (currentResearch.compare("none") == 0) {
			showDialogue("robby", "I'm not really working on anything at the moment.");
			std::vector<Choice> choices;
			choices.push_back({ "Fertilizers", "robby_fertilizers" });
			choices.push_back({ "Pesticides", "robby_pesticides" });
			choices.push_back({ "Irrigation", "robby_irrigation" });
			showDialogueWithChoices("robby", "Want me to look into something?", choices);
		}
		else {
			std::stringstream ss;
			ss << "Oh, right. I'm currently looking into " << currentResearch;
			showDialogue("robby", ss.str());
			showDialogue("robby", m_game->researchManager()->timeLeft());
			std::vector<Choice> choices;
			choices.push_back({ "Yes", "robby_finished" });
			choices.push_back({ "No, let's work on something else", "robby_reassign" });
			showDialogueWithChoices("robby", "Shall I keep crackin' on that?", choices);
		}
	}
	else if (choice.compare("robby_reassign") == 0) {
		m_game->researchManager()->stopResearch();
		std::vector<Choice> choices;
		choices.push_back({ "Fertilizers", "robby_fertilizers" });
		choices.push_back({ "Pesticides", "robby_pesticides" });
		choices.push_back({ "Irrigation", "robby_irrigation" });
		showDialogueWithChoices("robby", "Sure thing. What do you want me to look at?", choices);
	}
	else if (choice.compare("robby_fertilizers") == 0) {
		if (m_game->researchManager()->nextResearchId("fertilizer").compare("basic_fertilizer") == 0
			|| m_game->researchManager()->nextResearchId("fertilizer").compare("advanced_fertilizer") == 0) {
			showDialogue("robby", m_game->researchManager()->nextResearchDescription("fertilizer"));
			std::vector <Choice> choices;
			choices.push_back({ "Yes", "robby_finished_fertilizers" });
			choices.push_back({ "No", "robby_cancel" });
			showDialogueWithChoices("robby", "Want me to look into that?", choices);
		}
		else if (m_game->researchManager()->nextResearchId("fertilizer").compare("fertilizers_finished") == 0) {
			showDialogue("robby", "We've actually already researched all the fertilizers.");
		}
		else {
			showDialogue("robby", m_game->researchManager()->nextResearchDescription("fertilizer"));
			std::vector<Choice> choices;
			if (!m_game->researchManager()->researchFinished("asparagus_fertilizer")) choices.push_back({ "Asparagus", "robby_finished_fertilizers_asparagus" });
			if (!m_game->researchManager()->researchFinished("beet_fertilizer"))choices.push_back({ "Beets", "robby_finished_fertilizers_beet" });
			if (!m_game->researchManager()->researchFinished("bokchoy_fertilizer"))choices.push_back({ "Bok Choy", "robby_finished_fertilizers_bokchoy" });
			if (!m_game->researchManager()->researchFinished("carrot_fertilizer"))choices.push_back({ "Carrots", "robby_finished_fertilizers_carrot" });
			if (!m_game->researchManager()->researchFinished("garlic_fertilizer"))choices.push_back({ "Garlic", "robby_finished_fertilizers_garlic" });
			if (!m_game->researchManager()->researchFinished("leek_fertilizer"))choices.push_back({ "Leeks", "robby_finished_fertilizers_leek" });
			if (!m_game->researchManager()->researchFinished("onion_fertilizer"))choices.push_back({ "Onions", "robby_finished_fertilizers_onion" });
			if (!m_game->researchManager()->researchFinished("potato_fertilizer"))choices.push_back({ "Potatoes", "robby_finished_fertilizers_potato" });
			choices.push_back({ "Nevermind", "robby_cancel" });
			showDialogueWithChoices("robby", "Which one should I work on?", choices);
		}
	}
	else if (choice.compare("robby_pesticides") == 0) {
		if (m_game->researchManager()->nextResearchId("pesticide").compare("basic_pesticide") == 0) {
			showDialogue("robby", m_game->researchManager()->nextResearchDescription("pesticide"));
			std::vector <Choice> choices;
			choices.push_back({ "Yes", "robby_finished_pesticides" });
			choices.push_back({ "No", "robby_cancel" });
			showDialogueWithChoices("robby", "Sound like a plan?", choices);
		}
		else if (m_game->researchManager()->nextResearchId("pesticide").compare("pesticides_finished") == 0) {
			showDialogue("robby", "Sorry. That's all I got on the pesticide front.");
		}
		else {
			showDialogue("robby", m_game->researchManager()->nextResearchDescription("pesticide"));
			std::vector <Choice> choices;
			if (!m_game->researchManager()->researchFinished("beetle_pesticide")) choices.push_back({ "Beetles", "robby_finished_pesticides_beetle" });
			if (!m_game->researchManager()->researchFinished("worm_pesticide")) choices.push_back({ "Worms", "robby_finished_pesticides_worm" });
			if (!m_game->researchManager()->researchFinished("moth_pesticide")) choices.push_back({ "Moths", "robby_finished_pesticides_moth" });
			if (!m_game->researchManager()->researchFinished("slug_pesticide")) choices.push_back({ "Slugs", "robby_finished_pesticides_slug" });
			choices.push_back({ "Nevermind", "robby_cancel" });
			showDialogueWithChoices("robby", "What sounds good?", choices);
		}

	}
	else if (choice.compare("robby_irrigation") == 0) {
		showDialogue("robby", m_game->researchManager()->nextResearchDescription("irrigation"));
		std::vector <Choice> choices;
		choices.push_back({ "Yes", "robby_finished_irrigation" });
		choices.push_back({ "No", "robby_cancel" });
		showDialogueWithChoices("robby", "Want me to look into that?", choices);
	}
	else if (choice.compare("robby_finished_fertilizers") == 0) {
		showDialogue("robby", "On it.");
		m_game->researchManager()->research(m_game->researchManager()->nextResearchId("fertilizer"));
	}
	else if (choice.find("robby_finished_fertilizers_") != std::string::npos) {
		std::string cropName = choice.substr(choice.rfind("_") + 1);
		showDialogue("robby", "Sure thing.");
		std::stringstream ss;
		ss << cropName << "_fertilizer";
		m_game->researchManager()->research(ss.str());
	}
	else if (choice.compare("robby_finished_pesticides") == 0) {
		showDialogue("robby", "On it.");
		m_game->researchManager()->research(m_game->researchManager()->nextResearchId("pesticide"));
	}
	else if (choice.find("robby_finished_pesticides_") != std::string::npos) {
		std::string pestName = choice.substr(choice.rfind("_") + 1);
		if (pestName.compare("beetle") == 0) {
			showDialogue("robby", "Roll over, Beethoven. More beetles joining shortly.");
			m_game->researchManager()->research("beetle_pesticide");
		}
		else if (pestName.compare("worm") == 0) {
			showDialogue("robby", "Those worms are toast!");
			showDialogue("robby", "Ew. That's... actually pretty disgusting.");
			m_game->researchManager()->research("worm_pesticide");
		}
		else if (pestName.compare("moth") == 0) {
			showDialogue("robby", "Moth it to me baby!");
			showDialogue("robby", "Sorry, that was weak.");
			m_game->researchManager()->research("moth_pesticide");
		}
		else if (pestName.compare("slug") == 0) {
			showDialogue("robby", "Hopefully I can come up with something better than 'salt.'");
			m_game->researchManager()->research("slug_pesticide");
		}
	}
	else if (choice.compare("robby_finished_irrigation") == 0) {
		showDialogue("robby", "On it.");
		m_game->researchManager()->research(m_game->researchManager()->nextResearchId("irrigation"));
	}
	else if (choice.compare("robby_cancel") == 0) {
		showDialogue("robby", "No worries. Let me know if you change your mind.");
	}
	else if (choice.compare("al_cancel") == 0) {
		showDialogue("al", "...Okay then.");
	}
	else if (choice.compare("al_machining") == 0) {
		m_game->openMachining();
	}
	else if (choice.compare("intro_1_allset") == 0) {
		showDialogue("robby", "Great. I'll give you the grand tour in the morning.");
	}
	else if (choice.compare("intro_1_eat") == 0) {
		showDialogue("robby", "Oh. Uh� You can poke around in the kitchen if you like, it�s down the stairs and to your right.");
	}
	else if (choice.compare("intro_1_where") == 0) {
		showDialogue("robby", "Oh. Well, everyone�s asleep.  It�s a 24 hour cycle, amazingly enough.  But, uh� right now it�s about 3am so I�m gonna catch some sleep too.");
	}
}

void Dialogue::handleInput(sf::Event event) {
	if (event.mouseWheelScroll.delta > 0) {
		if (m_aboveChoiceTexts.size() > 0) {
			m_belowChoiceTexts.insert(m_belowChoiceTexts.begin(), m_choiceTexts.back());
			m_choiceTexts.pop_back();
			m_choiceTexts.insert(m_choiceTexts.begin(), m_aboveChoiceTexts.back());
			m_aboveChoiceTexts.pop_back();
		}
	}
	else {
		if (m_belowChoiceTexts.size() > 0) {
			m_aboveChoiceTexts.push_back(m_choiceTexts.front());
			m_choiceTexts.erase(m_choiceTexts.begin());
			m_choiceTexts.push_back(m_belowChoiceTexts.front());
			m_belowChoiceTexts.erase(m_belowChoiceTexts.begin());
		}
	}
	for (int i = 0; i < m_choiceTexts.size(); i++) {
		m_choiceTexts[i].setPosition(sf::Vector2f(m_text.getPosition().x + 20, m_text.getPosition().y + 35 * (i + 1)));
	}
}

std::string Dialogue::resultOfClickedOption() {
    return m_dialoguesToDisplay[0].m_choices[m_selectedChoice].m_result;
}

const std::string Dialogue::currentString() {
    return m_currentString;
}

std::string Dialogue::convertToMultiline(std::string str) {
	int numberOfLines = int(str.length() / MAX_CHARS_PER_LINE);
	for (int i = 1; i <= numberOfLines; i++) {
		str.insert(str.rfind(" ", MAX_CHARS_PER_LINE * i), "\n");
	}
	return Gui::escapeString(str);
}
