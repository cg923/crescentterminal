
// Copyright 2019 Happyrock Studios

#ifndef TIMEWEATHER_GUI_H
#define TIMEWEATHER_GUI_H

// Moon
#include "Gui.h"

class TimeWeatherGui : public Gui {
public:
    TimeWeatherGui(std::shared_ptr<Game> game);

    void defineSprite();

    void update(double dt);

    void draw();

    //////////////////////////////////////////////////////
    //                  Helper Functions                //
    //////////////////////////////////////////////////////


    //////////////////////////////////////////////////////
    //               Getters and Setters                //
    //////////////////////////////////////////////////////

private:
    sf::Text m_weatherText;
    sf::Text m_dateText;
    sf::Text m_timeText;
    sf::Text m_fundsText;
};

#endif // TIMEWEATHER_GUI_H

