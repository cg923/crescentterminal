
// Copyright 2019 Happyrock Studios

#ifndef GUI_H
#define GUI_H

// Moon
class Game;

// STD
#include <memory>

// SFML
#include <SFML/Graphics.hpp>

class Clickable {
public:
    Clickable(std::string name, sf::IntRect hitBox);

    const bool wasClicked(sf::Vector2f clickPos);

    // TODO - do these get used?
    const bool visible();

    void setVisible(bool visible);

    const std::string name();

    void setHitbox(sf::IntRect newRect);

private:
    std::string m_name;

    sf::IntRect m_hitBox;

    bool m_visible;
};

class Gui {
public:
    Gui(sf::Vector2f position, std::shared_ptr<Game> game);

    virtual void defineSprite() = 0;

    virtual void update(double dt);

    virtual void draw();

    static sf::Text buildText(sf::Font& font, int characterSize = 30, bool outline = true);

    static std::string escapeString(std::string str, bool removeNewLines = false);

    /////////////////////////////////////////////////////////
    //                  Getters and Setters                //
    /////////////////////////////////////////////////////////

    void display();

    void hide();

    const bool shouldDisplay();

protected:
    std::shared_ptr<Game> m_game;

    bool m_display;

    sf::Sprite m_sprite;

    sf::Vector2f m_position;
};
#endif // GUI_H
