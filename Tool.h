
// Copyright 2019 Happyrock Studios

#ifndef TOOL_H
#define TOOL_H

// Moon
#include "Item.h"

class Tool : public Item {
public:
    Tool(std::string name,
         std::string texturePath,
         std::string currentMap,
         std::shared_ptr<Game> game,
         ItemQuality quality,
         int spriteWidth,
         int spriteHeight);

	virtual void defineAnimations();

    virtual void updateSprite();

    virtual void update(double dt);

    virtual void draw(std::string layerName);

    virtual void drawFromPlayer(std::string layerName);

    virtual void use(sf::Vector2i gridPosition);

    virtual void repair();

    virtual void improve();

    virtual void stop();

    void hold();

    void letGo();

    ////////////////////////////////////////////////////////////////
    //                  Getters & Setters                         //
    ////////////////////////////////////////////////////////////////

	void setPositionByGrid(int x, int y);

	void setPositionByGrid(sf::Vector2i vec);

    void setDirection(Direction direction);

    const bool inUse();

protected:
    int m_maxDurability;
    int m_currentDurability;

    bool m_inUse;
    bool m_held;

	sf::Vector2i m_usePosition;
};

//////////////////////////////////////////////////////
//              Specific Tools                      //
//////////////////////////////////////////////////////

class Rototiller : public Tool {
public:
    Rototiller(std::shared_ptr<Game> game);

	void defineAnimations();

	void update(double dt);

    void repair();

private:
    int m_maxGas;
    int m_currentGas;
};

class Seedspreader : public Tool {
public:
    Seedspreader(std::shared_ptr<Game> game);

    void update(double dt);

    void drawFromPlayer(std::string layerName);

    std::shared_ptr<Seed> loadSeed(std::shared_ptr<Seed> seeds);

private:
    std::shared_ptr<Seed> m_loadedSeed;

    sf::Sprite m_loadedSeedSprite;
};

class Wateringcan : public Tool {
public:
    Wateringcan(std::shared_ptr<Game> game);

    void use(sf::Vector2i gridPosition);

    void stop();
};

class Chainsaw : public Tool {
public:
	Chainsaw(std::shared_ptr<Game> game);

	void update(double dt);
};

class Basketball : public Tool {
public:
	Basketball(std::shared_ptr<Game> game, sf::Vector2f position);

	void use(sf::Vector2i gridPosition);

	void stop();

	void shoot(float angle, int magnitude);

	void update(double dt);

	void updateSprite();

	void setPositionByGrid(int x, int y);

private:
	int m_charge;

	sf::Vector2f m_target;
	sf::Vector2f m_shootPos;
	float m_shootAngle;
};

#endif // TOOL_H
