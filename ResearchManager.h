
// Copyright 2019 Corey Selover

#ifndef RESEARCH_MANAGER_H
#define RESEARCH_MANAGER_H

// Moon
#include "Game.h"

struct ResearchItem {
	std::string m_name;
	std::string m_description;
	int m_duration;
	int m_progress;
	bool m_finished;
	std::vector<std::string> m_requirements;
};

class ResearchManager {
public:
	ResearchManager(std::shared_ptr<Game> game);

	void advanceDay();

	void unlockResearchItems(std::vector<std::pair<std::string, int>> unlocked);

	std::unordered_map<std::string, ResearchItem> researchItems();

	void research(std::string researchId);

	void stopResearch();

	const bool researchFinished(std::string researchId);

	const std::string currentResearchName();

	const std::string currentResearchId();

	const std::string nextResearchDescription(std::string researchBranch);

	const std::string nextResearchId(std::string researchBranch);

	const std::string timeLeft();

private:
	std::shared_ptr<Game> m_game;

	std::unordered_map<std::string, ResearchItem> m_researchItems;

	std::string m_currentResearchId;
};

#endif;