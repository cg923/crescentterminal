
// Copyright 2019 Happyrock Studios

#ifndef GAME_H
#define GAME_H

// Moon
class Camera;
class Entity;
class EventManager;
class ResearchManager;
class Gui;
class Map;
class Npc;
class Tile;
class Item;
class DayNight;
class SaveLoad;
class Crop;
class ItemLocker;
class Machinable;
struct Choice;

// STD
#include <unordered_map>
#include <memory>

// SFML
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

// Public constants
const std::string RESOURCES_PATH    = "resources/";
const std::string MAPS_PATH         = "resources/maps/";
const std::string IMAGES_PATH       = "resources/images/";
const std::string FONTS_PATH        = "resources/fonts/";
const std::string SOUNDS_PATH		= "resources/sounds/";
const std::string SAVES_PATH        = "saves/";

class Game : public std::enable_shared_from_this<Game> {
public:
    Game();

    void setup();

	void loadSaveSlot(int saveSlot);

    void run();

    void handleInput(sf::Event event);

    void tearDown();

    /////////////////////////////////////////////////////////////////////
    //                      Helper Functions                           //
    /////////////////////////////////////////////////////////////////////

    std::shared_ptr<Entity> checkForEntityByGrid(int x, int y, std::string mapName, std::string butNotThisEntity = "");

    bool activateEventByGrid(int x, int y, std::string eventType);

    void activateNpcEvent(int x, int y, std::string mapName);

    void showDialogue(std::string who, std::string what);

    void showDialogueWithChoices(std::string who, std::string what, std::vector<Choice> choices);

	void showNpcDialogueOptions(std::string who);

	void showPrewrittenDialogue(std::string dialogueName);

    void clearDialogue();

    void promptSleep();

    void cancelSleepPrompt();

    void saveAndSleep();

	void pauseWorld();

	void resumeWorld();

	void hideGui();

	void showGui();

    // TODO - Is there a situation when this would not be because of sleep??
    void advanceDay(bool becauseOfSleep = false);

    // TODO - should this go in a gui?

	void holdItem(std::shared_ptr<Item> item);

    void dropHeldItem();

    void startComputer();

    void closeComputer();

	void openMachining();

	void closeMachining();

	void movePlayerToNextMap();

	void completeStellaSchedule(std::string dayOfWeek);

	void setStellaSchedule();

	void assignAl(std::shared_ptr<Machinable> machinable);


    /////////////////////////////////////////////////////////////////////
    //                      Getters and Setters                        //
    /////////////////////////////////////////////////////////////////////

    // Stored objects

    std::shared_ptr<Game> game() { return shared_from_this(); }

    std::shared_ptr<sf::RenderWindow> renderWindow();

    std::shared_ptr<sf::Texture> texture(std::string textureName);

	std::shared_ptr<sf::Sound> sound(std::string soundName);

	std::shared_ptr<sf::Music> music(std::string musicName);

    std::shared_ptr<sf::Font> font(std::string fontName);

    std::shared_ptr<Map> map(std::string mapName);

    std::shared_ptr<Entity> entity(std::string entityName);
	
    void addEntity(std::string entityName, std::shared_ptr<Entity> entity);

	std::string addEntityWithoutName(std::shared_ptr<Entity> entity);

	std::shared_ptr<Entity> removeEntity(std::string entityName);

    std::shared_ptr<Gui> gui(std::string guiName);

    std::shared_ptr<DayNight> dayNight();

    std::vector<std::shared_ptr<Crop>> allPlantedCrops(std::string mapName);

	std::vector<std::pair<int, int>> allWeeds(std::string mapName);

	std::vector<std::pair<int, int>> cutTrees(std::string mapName);

	std::vector<std::shared_ptr<Entity>> allPlaceables(std::string mapName);

	std::vector<std::shared_ptr<ItemLocker>> allLockers();

	std::shared_ptr<Item> heldItem();

	std::shared_ptr<ResearchManager> researchManager();

    // Map

    std::shared_ptr<Map> currentMap();

    void setCurrentMap(std::string whichMap);

    sf::Vector2i pixelsToGrid(float x, float y);

    sf::Vector2f gridToPixels(int x, int y);

    // Gui

    const sf::Vector2i mouseGridPosition();

    const sf::Vector2f mousePixelPosition();

	const sf::Vector2f mouseWorldPosition();

	const float mouseAngle();

    const std::string timeOfDayAsString();

    const std::string dayOfWeek();

    const std::string date();

    const std::string time();

    void displayLocker(std::string lockerName);

	int nextOpenSaveSlot();

	const bool displayingDialogue();

	const bool guiVisible();

private:
    void defineNpcBehaviors();

    void updateEntities(double dt);

    void updateGuis(double dt);

    void drawEntities();

    void drawGuis();

    void advanceEntities();

    // Managers and objects
    std::unordered_map<std::string, std::shared_ptr<sf::Texture> >    m_textures;
	std::unordered_map<std::string, std::shared_ptr<sf::Sound> >	  m_sounds;
	std::unordered_map<std::string, std::shared_ptr<sf::SoundBuffer>> m_soundBuffers;
	std::unordered_map<std::string, std::shared_ptr<sf::Music> >	  m_musics;
    std::unordered_map<std::string, std::shared_ptr<sf::Font> >       m_fonts;
    std::unordered_map<std::string, std::shared_ptr<Map> >            m_maps;
    std::unordered_map<std::string, std::shared_ptr<Entity> >         m_entities;
	std::unordered_map<std::string, int>							  m_entityCount;
    std::unordered_map<std::string, std::shared_ptr<Gui> >            m_guis;
    std::shared_ptr<EventManager>                           m_events;
    std::shared_ptr<sf::RenderWindow>                       m_window;
    std::shared_ptr<Camera>                                 m_camera;
    std::shared_ptr<DayNight>                               m_dayNightCycle;
    std::shared_ptr<Item>                                   m_mouseHeldItem;
    std::shared_ptr<SaveLoad>                               m_saveLoad;
	std::shared_ptr<ResearchManager>						m_researchManager;
	std::vector<std::shared_ptr<ItemLocker>>				m_lockers;

    // Values
    std::string     m_currentMap;
    std::string     m_newMap;
    sf::Vector2i    m_tileDimensions;
    sf::Vector2i    m_mouseGridPosition;
    sf::Vector2f    m_mousePixelPosition;
	sf::Vector2f	m_mouseWorldCoords;
    sf::IntRect     m_mapBoundaries;
    sf::Clock       m_gameClock;
    std::string     m_displayLocker;
	std::string		m_itemToAdd;
	std::string		m_timeToAdd;

    // Switches
	bool m_pause;
	bool m_displayGui;
    bool m_displayInventory;
    bool m_displayMenu;
    bool m_displayDialogue;
    bool m_displayComputer;
	bool m_displayMachining;
    bool m_guiUp;
    bool m_promptSleep;
};
#endif // GAME_H
