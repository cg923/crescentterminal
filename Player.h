
// Copyright 2019 Happyrock Studios

#ifndef PLAYER_H
#define PLAYER_H

// Moon
#include "Character.h"
class Tool;

const unsigned int MAX_FUNDS = 9999999;

struct FocusTile {
    sf::Vector2i coordinates;
    sf::Sprite sprite;

};

class Player : public Character {
public:
    Player(std::string name, std::string currentMap, Direction direction, std::shared_ptr<Game> game);

    void update(double dt);

    void draw(std::string layerName);

    void handleInput(sf::Event event);

    //////////////////////////////////////////////////////////////////
    //                          Helper Functions                    //
    //////////////////////////////////////////////////////////////////

	void pauseMovement();

	void resumeMovement();

    void useHeldItem();

    void stopUsingHeldItem();

    void stopMovement();

    void pickUpTool(std::shared_ptr<Tool> tool);

    std::shared_ptr<Tool> putDownTool();

    void setOutfit(std::unordered_map<std::string, int> outfit);

	void pause(float timeOut);

    //////////////////////////////////////////////////////////////////
    //                         Getters & Setters                    //
    //////////////////////////////////////////////////////////////////

    const int currentFunds();

    void setFunds(int funds);

    void addFunds(int fundsToAdd);

    const bool usingItem();

    const std::string currentToolName();

    std::shared_ptr<Tool> currentTool();

	const bool flashlightOn();

    std::unordered_map<std::string, int> outfit();

private:
	bool m_canMove;

    FocusTile m_focusTile;

    std::shared_ptr<Tool> m_currentTool;

    int m_currentFunds;

    std::shared_ptr<sf::Sprite> m_headSprite;
    std::shared_ptr<sf::Sprite> m_torsoSprite;
    std::shared_ptr<sf::Sprite> m_legsSprite;
    std::shared_ptr<sf::Sprite> m_shoesSprite;

    std::unordered_map<std::string, int> m_outfit;

	bool m_flashlightOn;

	sftools::Chronometer m_actionTimeout;
	float m_timeOut;

};
#endif // PLAYER_H
