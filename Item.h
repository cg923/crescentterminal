
// Copyright 2019 Happyrock Studios

#ifndef ITEM_H
#define ITEM_H

// Moon
#include "Entity.h"
// TODO - If we are only including Crop.h for the Crop type enum, we should just move it here.
#include "Crop.h"

const int MAX_ITEM_COUNT = 999;

enum ItemQuality {
    IQ_BROKEN,
    IQ_POOR,
    IQ_FAIR,
    IQ_GOOD,
    IQ_HIGH
};

class Item : public Entity {
public:
    Item(std::string name,
		 std::string type,
		 std::string specificType,
         std::string texturePath,
         int quantity,
         std::string currentMap,
         std::shared_ptr<Game> game,
         int spriteWidth = 16,
         int spriteHeight = 16);

    virtual void defineAnimations() {}

    virtual void update(double dt);

    virtual void draw(std::string layerName);

    virtual void use(sf::Vector2i gridPosition);

    virtual void stop() {}

    virtual void destroy();

    virtual void updateSprite();

    //////////////////////////////////////////////////////////////////
    //                      Getters & Setters                       //
    //////////////////////////////////////////////////////////////////

    const float maxSpeed();

    const int quantity();

    void setQuantity(int newAmount);

    int addToQuantity(int amount);

	void setAsInInventory();

    virtual void setPositionByGrid(int x, int y);

    virtual void setPositionByGrid(sf::Vector2i vec);

    std::unordered_map<std::string, std::string> properties();

    const std::string specificType();

	void setPrice(int newPrice);

	const int price();

protected:
    std::string m_specificType;

    ItemQuality m_quality;

    float m_maxSpeed;

    int m_quantity;
	int m_price;

	bool m_inInventory;

    sf::Text m_quantityText;

    std::unordered_map<std::string, std::string> m_itemProperties;
};

//////////////////////////////////////////////////////////////////////
//                      Specific Items                              //
//////////////////////////////////////////////////////////////////////


class Seed: public Item {
public:
    Seed(Crops seedType, int quantity, std::string currentMap, std::shared_ptr<Game> game);

    void defineAnimations() {}

    void use(sf::Vector2i gridPosition);

    const Crops seedType();

private:
    Crops m_seedType;

	int m_price;
};

class Yield : public Item {
public:
    Yield(Crops cropType, int quantity, CropQuality quality, std::string currentMap, std::shared_ptr<Game> game);

    void initialize(Crops cropType, int quantity, CropQuality quality);

    void use(sf::Vector2i gridPosition);

    const Crops cropType();

private:
    Crops m_cropType;

    CropQuality m_quality;
};

class Sprinkler : public Item {
public:
	Sprinkler(std::string currentMap, std::shared_ptr<Game> game);

	void defineAnimations();

	void use(sf::Vector2i gridPosition);

	void advanceDay();

	void water();

	void setAsAuto();

private:
	bool m_auto;
};

#endif // ITEM_H
