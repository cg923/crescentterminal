
// Copyright 2019 Happyrock Studios

#include "Item.h"
#include "Game.h"
#include "InventoryGui.h"
#include "DayNight.h"
#include "Player.h"
#include "Tool.h"
#include "Map.h"

// STD
#include <iostream>

Item::Item(std::string name,
			std::string type,
			std::string specificType,
			std::string texturePath,
			int quantity,
			std::string currentMap,
			std::shared_ptr<Game> game,
			int spriteWidth,
			int spriteHeight)
    : Entity(name, type, texturePath, currentMap, game, spriteWidth, spriteHeight) {
    m_direction = DOWN;
    m_maxSpeed = 0;
	m_price = 0;
	m_inInventory = true;
    m_specificType = specificType;
    m_quantity = std::min(quantity, MAX_ITEM_COUNT);
    m_sprites["middle"].push_back(m_sprite);
    scaleSprites(2, 2);

    // Quantity text
    m_quantityText.setFont(*m_game->font("dialogue"));
    m_quantityText.setCharacterSize(20);
    m_quantityText.setFillColor(sf::Color::White);
    m_quantityText.setOutlineColor(sf::Color::Black);
    m_quantityText.setOutlineThickness(1);
}

void Item::update(double dt) {
    Entity::update(dt);

    updateSprite();

    if (m_quantity < 1) destroy();
}

void Item::use(sf::Vector2i gridPosition) {
    m_quantity--;
}

void Item::destroy() {
    std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->removeFromInventory(-1);
}

void Item::updateSprite() {
    m_quantityText.setString(std::to_string(m_quantity));
    m_quantityText.setPosition(m_position + sf::Vector2f(6,24));
}

void Item::draw(std::string layerName) {
    Entity::draw(layerName);

    if (layerName.compare("middle") == 0 && m_inInventory) m_game->renderWindow()->draw(m_quantityText);
}

const float Item::maxSpeed() {
    return m_maxSpeed;
}

const int Item::quantity() {
    return m_quantity;
}

int Item::addToQuantity(int amount) {
    m_quantity += amount;
    if (m_quantity > MAX_ITEM_COUNT) {
        int remainder = m_quantity - MAX_ITEM_COUNT;
        m_quantity = MAX_ITEM_COUNT;
        return remainder;
    }
    else return -1;
}

void Item::setQuantity(int newAmount) {
    m_quantity = newAmount;
}

void Item::setAsInInventory() {
	m_inInventory = true;
	scaleSprites(2, 2);
}

void Item::setPositionByGrid(int x, int y) {
    Entity::setPositionByGrid(x, y);

    m_position.x -= m_sprite->getTextureRect().width / 2;
    m_position.y -= m_sprite->getTextureRect().height / 2;

	if (m_inInventory) scaleSprites(0.75, 0.75);
	m_inInventory = false;
}

void Item::setPositionByGrid(sf::Vector2i vec) {
    Entity::setPositionByGrid(vec);

    m_position.x -= m_sprite->getTextureRect().width / 2;
    m_position.y -= m_sprite->getTextureRect().height / 2;

	if (m_inInventory) scaleSprites(0.75, 0.75);
	m_inInventory = false;
}

std::unordered_map<std::string, std::string> Item::properties() {
    return m_itemProperties;
}

const std::string Item::specificType() {
    return m_specificType;
}

void Item::setPrice(int newPrice) {
	m_price = newPrice;
}

const int Item::price() {
	return m_price;
}

//////////////////////////////////////////////////////////////////////
//                      Specific Items                              //
//////////////////////////////////////////////////////////////////////

Seed::Seed(Crops seedType, int quantity, std::string currentMap, std::shared_ptr<Game> game)
    : Item("UNNAMED_SEED", "seed", "UNDEFINED", "items", quantity, currentMap, game, 16, 16) {
    m_seedType = seedType;

    switch(m_seedType) {
    case CROP_ASPARAGUS:
        m_sprite->setTextureRect(sf::IntRect(0, 0, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
        m_itemProperties["seed_type"] = std::to_string(CROP_ASPARAGUS);
        m_specificType = "seed_asparagus";
		setPrice(120);
        break;
    case CROP_BEETS:
        m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().width, 0, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
        m_itemProperties["seed_type"] = std::to_string(CROP_BEETS);
        m_specificType = "seed_beets";
		setPrice(70);
        break;
	case CROP_BOKCHOY:
		m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().width*2, 0, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
		m_itemProperties["seed_type"] = std::to_string(CROP_BOKCHOY);
		m_specificType = "seed_bokchoy";
		setPrice(50);
		break;
	case CROP_CARROTS:
		m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().width * 3, 0, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
		m_itemProperties["seed_type"] = std::to_string(CROP_CARROTS);
		m_specificType = "seed_carrots";
		setPrice(70);
		break;
	case CROP_GARLIC:
		m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().width * 4, 0, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
		m_itemProperties["seed_type"] = std::to_string(CROP_GARLIC);
		m_specificType = "seed_garlic";
		setPrice(170);
		break;
    default:
        break;
    }
}

void Seed::use(sf::Vector2i gridPosition) {
    if (!m_game->entity("seedspreader") || m_game->entity("seedspreader")->gridPosition() != gridPosition) return;

    std::shared_ptr<Seed> otherSeed = std::dynamic_pointer_cast<Seedspreader>(m_game->entity("seedspreader"))->loadSeed(std::dynamic_pointer_cast<Seed>(thisEntity()));
    if (otherSeed && otherSeed->seedType() == seedType()) {
        int remainder = addToQuantity(otherSeed->quantity());
        if (remainder > 0) std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->addToInventory(std::make_shared<Seed>(seedType(), remainder, currentMap(), m_game));
    }
    else if (otherSeed) {
        std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->removeFromInventory();
        std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->addToInventory(otherSeed);
    }
    else {
        std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->removeFromInventory();
    }
}

const Crops Seed::seedType() {
    return m_seedType;
}

////////////////////////////////////////////////////////////////////////
//                          Yield                                     //
////////////////////////////////////////////////////////////////////////

Yield::Yield(Crops cropType, int quantity, CropQuality quality, std::string currentMap, std::shared_ptr<Game> game)
    : Item("UNNAMED_YIELD", "yield", "UNDEFINED", "items", quantity, currentMap, game, 16, 16) {
    initialize(cropType, quantity, quality);
}

void Yield::initialize(Crops cropType, int quantity, CropQuality quality) {
    m_cropType = cropType;
    m_quality = quality;
    m_quantity = quantity;

    switch(m_cropType) {
    case CROP_ASPARAGUS:
        m_sprite->setTextureRect(sf::IntRect(0, 16, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
        m_itemProperties["crop_type"] = std::to_string(CROP_ASPARAGUS);
        m_specificType = "yield_asparagus";
        break;
    case CROP_BEETS:
        m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().width, 16, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
        m_itemProperties["crop_type"] = std::to_string(CROP_BEETS);
        m_specificType = "yield_beets";
        break;
	case CROP_BOKCHOY:
		m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().width * 2, 16, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
		m_itemProperties["crop_type"] = std::to_string(CROP_BOKCHOY);
		m_specificType = "yield_bokchoy";
		break;
	case CROP_CARROTS:
		m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().width * 3, 16, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
		m_itemProperties["crop_type"] = std::to_string(CROP_CARROTS);
		m_specificType = "yield_carrots";
		break;
	case CROP_GARLIC:
		m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().width * 4, 16, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
		m_itemProperties["crop_type"] = std::to_string(CROP_GARLIC);
		m_specificType = "yield_garlic";
		break;
    default:
        break;
    }

    m_itemProperties["quality"] = m_quality;
}

void Yield::use(sf::Vector2i gridPosition) {
    // Ummm... Eat it I guess?
}

const Crops Yield::cropType() {
    return m_cropType;
}

/////////////////////////////////////////////////////////////
//						Sprinkler						   //
/////////////////////////////////////////////////////////////

Sprinkler::Sprinkler(std::string currentMap, std::shared_ptr<Game> game)
	: Item("UNNAMED_SPRINKLER", "sprinkler", "sprinkler", "items", 1, currentMap, game) {
	defineAnimations();
	m_auto = false;
}

void Sprinkler::defineAnimations() {
	m_sprite->setTextureRect(sf::IntRect(0, 64, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
}

void Sprinkler::use(sf::Vector2i gridPosition) {
	if (!m_game->currentMap()->tileByGrid("background", gridPosition.x, gridPosition.y)->m_farmable) return;
	m_game->addEntity(name(), std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->removeFromInventory());
	setPositionByGrid(gridPosition);
}

void Sprinkler::advanceDay() {
	if (!m_auto) return;
	water();
}


void Sprinkler::water() {
	for (int x = m_gridPosition.x - 2; x <= m_gridPosition.x + 2; x++) {
		for (int y = m_gridPosition.y - 2; y <= m_gridPosition.y + 2; y++) {
			m_game->map(m_currentMap)->waterByGrid(x, y);
		}
	}
}

void Sprinkler::setAsAuto() {
	m_auto = true;
}
