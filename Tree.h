
// Copyright 2019 Happyrock Studios

#ifndef TREE_H
#define TREE_H

// Moon
#include "Entity.h"

class Tree : public Entity {
public:
    Tree(std::string mapName, std::shared_ptr<Game> game, int gridX, int gridY, int spriteWidth = 64, int spriteHeight = 128);

    void update(double dt);

	void draw(std::string layerName);

	void cut();

	void cutDown();

	bool isCut();

private:
    std::shared_ptr<sf::Sprite> m_stumpSprite;
    std::shared_ptr<sf::Sprite> m_trunkSprite;

    sf::Vector2i m_initGridPos;

	bool m_init;
	bool m_cut;

	int m_health;
};

#endif // TREE_H
