
// Copyright 2019 Happyrock Studios

// Moon
#include "Character.h"
#include "Game.h"
#include "Map.h"

// STD
#include <iostream>
#include <queue>

Character::Character(std::string name,
                     std::string type,
                     std::string texturePath,
                     std::string currentMap,
                     Direction direction,
                     std::shared_ptr<Game> game)
    : Entity(name, type, texturePath, currentMap, game, CHARA_SPRITE_WIDTH, CHARA_SPRITE_HEIGHT)
{
    m_direction         = direction;
    m_leftCollision     = false;
    m_rightCollision    = false;
    m_upCollision       = false;
    m_downCollision     = false;
    m_walking           = false;
    m_working           = false;
    m_tempSpeed         = -1;
	m_walkTarget = sf::Vector2i();
	m_warpTo = "";

    defineHitbox();
    defineAnimations();
    setAnimation("stopped_down");
}

void Character::defineHitbox() {
    m_hitboxSprite.setFillColor(sf::Color(100, 250, 50, 180));
    m_hitbox = sf::IntRect(m_position.x,
                           m_position.y,
                           m_sprite->getTextureRect().width - 2,
                           m_sprite->getTextureRect().height - 13);
    m_hitboxOffset = sf::Vector2i(1, 20);
}

void Character::defineAnimations() {
    sf::IntRect texRect = m_sprite->getTextureRect();
    m_animations["stopped_down"]    = Animation {"stopped_down", sf::IntRect(0, 0, texRect.width, texRect.height), 1, true, 1, 1 };
    m_animations["stopped_up"]      = Animation {"stopped_up", sf::IntRect(texRect.width*3, 0, texRect.width, texRect.height), 1, true, 1, 1 };
    m_animations["stopped_left"]    = Animation {"stopped_left", sf::IntRect(texRect.width*2, 0, texRect.width, texRect.height), 1, true, 1, 1 };
    m_animations["stopped_right"]   = Animation {"stopped_right", sf::IntRect(texRect.width, 0, texRect.width, texRect.height), 1, true, 1, 1 };
    m_animations["walking_down"]    = Animation {"walking_down", sf::IntRect(0, texRect.height, texRect.width, texRect.height), 6, true, 1, 1 };
    m_animations["walking_right"]   = Animation {"walking_right", sf::IntRect(0, texRect.height*2, texRect.width, texRect.height), 6, true, 1, 1 };
    m_animations["walking_left"]    = Animation {"walking_left", sf::IntRect(0, texRect.height*3, texRect.width, texRect.height), 6, true, 1, 1 };
    m_animations["walking_up"]      = Animation {"walking_up", sf::IntRect(0, texRect.height*4, texRect.width, texRect.height), 6, true, 1, 1 };
    m_animations["stopped_working_down"]     = Animation {"stopped_working_down", sf::IntRect(0, texRect.height*5, texRect.width, texRect.height), 1, true, 1, 1 };
    m_animations["stopped_working_right"]    = Animation {"stopped_working_right", sf::IntRect(texRect.width, texRect.height*5, texRect.width, texRect.height), 1, true, 1, 1 };
    m_animations["stopped_working_left"]     = Animation {"stopped_working_left", sf::IntRect(texRect.width*2, texRect.height*5, texRect.width, texRect.height), 1, true, 1, 1 };
    m_animations["stopped_working_up"]       = Animation {"stopped_working_up", sf::IntRect(texRect.width*3, texRect.height*5, texRect.width, texRect.height), 1, true, 1, 1 };
    m_animations["working_down"]    = Animation {"working_down", sf::IntRect(0, texRect.height*6, texRect.width, texRect.height), 6, true, 1, 1 };
    m_animations["working_right"]   = Animation {"working_right", sf::IntRect(0, texRect.height*7, texRect.width, texRect.height), 6, true, 1, 1 };
    m_animations["working_left"]    = Animation {"working_left", sf::IntRect(0, texRect.height*8, texRect.width, texRect.height), 6, true, 1, 1 };
    m_animations["working_up"]      = Animation {"working_up", sf::IntRect(0, texRect.height*9, texRect.width, texRect.height), 6, true, 1, 1 };
}

void Character::update(double dt) {
    Entity::update(dt);

    m_gridPosition = m_game->pixelsToGrid(m_position.x + m_sprite->getTextureRect().width / 2,
                                          m_position.y + m_sprite->getTextureRect().height / 2 + m_hitboxOffset.y);

    checkForCollisions();

    // Movement - TODO - this probably belongs in Player
    if ((m_left || m_right) && (m_up || m_down)) {
        // Adjust speed if moving diagonally.
        m_speed = m_maxSpeed / sqrt(2);
        if (m_tempSpeed > 0) m_speed = m_tempSpeed / sqrt(2);
    }
    else {
        m_speed = m_maxSpeed;
        if (m_tempSpeed > 0) m_speed = m_tempSpeed;
    }

	// Places to go, people to see.
	if (!m_walking && m_walkTarget != sf::Vector2i()) {
		determinePath();
		m_walking = true;
	}

	// WALK THIS WAY!!!
	if (m_walking && m_walkTarget != sf::Vector2i()) {
		if (m_path.empty()) {
			if (m_warpTo.compare("") != 0) {
				setCurrentMap(m_warpTo);
				if (m_warpTo.compare("upstairs") == 0) setPositionByGrid(4, 8);
				else if (m_warpTo.compare("base") == 0) setPositionByGrid(22, 18);
				m_warpTo = "";
			}
			else {
				m_walking = false;
				m_walkTarget = sf::Vector2i();
			}
		}
		else {
			sf::Vector2f nextDest = m_game->gridToPixels(m_path.top().x, m_path.top().y) - sf::Vector2f(m_sprite->getTextureRect().width * m_sprite->getScale().x / 2, 32);
			if (m_position != nextDest) {
				walk(nextDest);
			}
			else m_path.pop();
		}
	}

    updateAnimation();
}

void Character::updateAnimation() {
    switch(m_direction) {
    case ULEFT:
    case DLEFT:
    case LEFT:
        if (m_walking && !m_working) setAnimation("walking_left");
        else if (!m_walking && m_working) setAnimation("stopped_working_left");
        else if (m_walking && m_working) setAnimation("working_left");
        else setAnimation("stopped_left");
        break;
    case URIGHT:
    case DRIGHT:
    case RIGHT:
        if (m_walking && !m_working) setAnimation("walking_right");
        else if (!m_walking && m_working) setAnimation("stopped_working_right");
        else if (m_walking && m_working) setAnimation("working_right");
        else setAnimation("stopped_right");
        break;
    case UP:
        if (m_walking && !m_working) setAnimation("walking_up");
        else if (!m_walking && m_working) setAnimation("stopped_working_up");
        else if (m_walking && m_working) setAnimation("working_up");
        else setAnimation("stopped_up");
        break;
    case DOWN:
        if (m_walking && !m_working) setAnimation("walking_down");
        else if (!m_walking && m_working) setAnimation("stopped_working_down");
        else if (m_walking && m_working) setAnimation("working_down");
        else setAnimation("stopped_down");
        break;
    default:
        break;
    }
}

void Character::walk() {
    determineDirection();

    sf::IntRect boundaries = m_game->currentMap()->dimensions();
    sf::IntRect texRect = m_sprite->getTextureRect();

    switch(m_direction) {
    case LEFT:
        if (m_position.x > boundaries.left && !m_leftCollision) m_position.x -= m_speed;
        break;
    case RIGHT:
        if (m_position.x + texRect.width < boundaries.width && !m_rightCollision) m_position.x += m_speed;
        break;
    case UP:
        if (m_position.y > boundaries.top && !m_upCollision) m_position.y -= m_speed;
        break;
    case DOWN:
        if (m_position.y + texRect.height < boundaries.height && !m_downCollision) m_position.y += m_speed;
        break;
    case ULEFT:
        if (m_position.x > boundaries.left && !m_leftCollision) m_position.x -= m_speed;
        if (m_position.y > boundaries.top && !m_upCollision) m_position.y -= m_speed;
        break;
    case URIGHT:
        if (m_position.x + texRect.width < boundaries.width && !m_rightCollision) m_position.x += m_speed;
        if (m_position.y > boundaries.top && !m_upCollision) m_position.y -= m_speed;
        break;
    case DLEFT:
        if (m_position.x > boundaries.left && !m_leftCollision) m_position.x -= m_speed;
        if (m_position.y + texRect.height < boundaries.height && !m_downCollision) m_position.y += m_speed;
        break;
    case DRIGHT:
        if (m_position.x + texRect.width < boundaries.width && !m_rightCollision) m_position.x += m_speed;
        if (m_position.y + texRect.height < boundaries.height && !m_downCollision) m_position.y += m_speed;
        break;
    default:
        break;
    }
}

void Character::walk(sf::Vector2f nextDest) {
	if (m_position.x > nextDest.x) {
		if (m_direction != LEFT) {
			m_direction = LEFT;
		}
		m_position.x -= m_speed;
	}
	else if (m_position.x < nextDest.x) {
		if (m_direction != RIGHT) {
			m_direction = RIGHT;
		}
		m_position.x += m_speed;
	}
	else if (m_position.y < nextDest.y) {
		if (m_direction != DOWN) {
			m_direction = DOWN;
		}
		m_position.y += m_speed;
	}
	else if (m_position.y > nextDest.y) {
		if (m_direction != UP) {
			m_direction = UP;
		}
		m_position.y -= m_speed;
	}
}

void Character::setPositionByGrid(int x, int y) {
    Entity::setPositionByGrid(x, y);

    m_position.x -= (m_sprite->getTextureRect().width * m_sprite->getScale().x / 2);
    m_position.y -= (m_sprite->getTextureRect().height * m_sprite->getScale().y - 16 / 2);
}

void Character::setPositionByGrid(sf::Vector2i vec) {
    Entity::setPositionByGrid(vec);

    m_position.x -= (m_sprite->getTextureRect().width * m_sprite->getScale().x / 2);
    m_position.y -= (m_sprite->getTextureRect().height * m_sprite->getScale().y - 16 / 2);
}

void Character::setPositionByPixel(float x, float y) {
    Entity::setPositionByPixel(x, y);

    m_position.x -= m_sprite->getTextureRect().width / 2;
}

void Character::setPositionByPixel(sf::Vector2f vec) {
    Entity::setPositionByPixel(vec);

    m_position.x -= m_sprite->getTextureRect().width / 2;
}

void Character::determineDirection() {
    if (m_left) {
        if (!m_up && !m_down) m_direction = LEFT;
        else if(m_up) m_direction = ULEFT;
        else m_direction = DLEFT;
    }
    else if (m_right) {
        if (!m_up && !m_down) m_direction = RIGHT;
        else if(m_up) m_direction = URIGHT;
        else m_direction = DRIGHT;
    }
    else if (m_up) m_direction = UP;
    else if (m_down) m_direction = DOWN;
}

void Character::checkForCollisions() {
    if (m_game->map(currentMap())->checkForObstacleByPixel(m_hitbox.left - 1, m_hitbox.top)
        || m_game->map(currentMap())->checkForObstacleByPixel(m_hitbox.left - 1, m_hitbox.top + m_hitbox.height)) m_leftCollision = true;
    else m_leftCollision = false;
    if (m_game->map(currentMap())->checkForObstacleByPixel(m_hitbox.left + m_hitbox.width + 1, m_hitbox.top)
        || m_game->map(currentMap())->checkForObstacleByPixel(m_hitbox.left + m_hitbox.width + 1, m_hitbox.top + m_hitbox.height)) m_rightCollision = true;
    else m_rightCollision = false;
    if (m_game->map(currentMap())->checkForObstacleByPixel(m_hitbox.left, m_hitbox.top - 1)
        || m_game->map(currentMap())->checkForObstacleByPixel(m_hitbox.left + m_hitbox.width, m_hitbox.top - 1)) m_upCollision = true;
    else m_upCollision = false;
    if (m_game->map(currentMap())->checkForObstacleByPixel(m_hitbox.left, m_hitbox.top + m_hitbox.height + 1)
        || m_game->map(currentMap())->checkForObstacleByPixel(m_hitbox.left + m_hitbox.width, m_hitbox.top + m_hitbox.height + 1)) m_downCollision = true;
    else m_downCollision = false;
}

void Character::setCurrentMap(std::string newMap) {
    Entity::setCurrentMap(newMap);

    m_walking = false;
    m_working = false;
    m_up = false;
    m_down = false;
    m_left = false;
    m_right = false;
}

const bool Character::walking() {
	return m_walking;
}

void Character::setWalkTarget(int x, int y) {
	m_walkTarget = sf::Vector2i(x, y);
}

void Character::determinePath() {

	sf::Vector2i dimensions = m_game->map(m_currentMap)->mapSize();
	// TODO - this is almost certainly a bad idea to just pick 200 as a size
	bool visited[200][200];
	for (auto i = 0; i < dimensions.x; i++) {
		for (auto j = 0; j < dimensions.y; j++) {
			visited[i][j] = false;
		}
	}

	// BFS queue.
	std::queue<std::shared_ptr<CheckTile>> tilesToCheck;
	std::shared_ptr<CheckTile> current = std::make_shared<CheckTile>(m_gridPosition);
	tilesToCheck.push(current);

	// Mark the current tile as visited.
	visited[m_gridPosition.x][m_gridPosition.y] = true;

	// Let's go.
	while (!tilesToCheck.empty()) {
		std::shared_ptr<CheckTile> currentTile = tilesToCheck.front();

		// Check to see if this tile is the tile we're looking for.
		if (currentTile->m_position == m_walkTarget) {
			m_path = std::stack<sf::Vector2i>();
			m_path.push(currentTile->m_position);
			std::shared_ptr<CheckTile> parent = currentTile->m_parent;
			if (!parent) {
				m_walkTarget = sf::Vector2i();
			}
			else {
				while (!parent->m_head) {
					m_path.push(parent->m_position);
					parent = parent->m_parent;
				}
			}
			return;
		}

		// Check each neighbor to see if it's been visited before.
		std::vector<sf::Vector2i> neighbors = m_game->map(m_currentMap)->validNeighborsOf(currentTile->m_position);
		std::vector<sf::Vector2i>::iterator itr;
		for (itr = neighbors.begin(); itr != neighbors.end(); itr++) {
			// If not, add it to the queue and assign this tile as its parent.
			if (!visited[(*itr).x][(*itr).y]) {
				std::shared_ptr<CheckTile> thisTile = std::make_shared<CheckTile>(sf::Vector2i((*itr).x, (*itr).y), currentTile);
				tilesToCheck.push(thisTile);
				visited[(*itr).x][(*itr).y] = true;
			}
		}

		// Pop the queue.
		tilesToCheck.pop();
	}
}