
// Copyright 2019 Corey Selover

#include "SaveLoad.h"

// Moon
#include "Game.h"
#include "DayNight.h"
#include "Npc.h"
#include "Map.h"
#include "Player.h"
#include "InventoryGui.h"
#include "Item.h"
#include "ItemLocker.h"
#include "ComputerGui.h"
#include "ResearchManager.h"
#include "Tree.h"

// STD
#include <sstream>
#include <iostream>
#include <fstream>

// Other libs
#include "libraries/pugixml.hpp"

bool SaveLoad::loadSaveFile(int slot) {

	if (slot < 0) {
		std::cout << "Invalid save slot: " << slot << std::endl;
		return false;
	}

	m_currentSaveSlot = slot;

    std::stringstream filePath;
    filePath << SAVES_PATH << "save_" << slot << ".xml";

    pugi::xml_document doc;
    // TODO - do this everywhere
    pugi::xml_parse_result result = doc.load_file(filePath.str().c_str());
    if (result) {

        // Game values
        pugi::xml_node game = doc.child("game");
        m_game->dayNight()->setYear(game.child("year").attribute("value").as_int());
        m_game->dayNight()->setMonth(game.child("month").attribute("value").as_int());
        m_game->dayNight()->setDay(game.child("day").attribute("value").as_int());

        // Player
        pugi::xml_node player = doc.child("player");
        std::dynamic_pointer_cast<Player>(m_game->entity("player"))->setFunds(player.child("funds").attribute("value").as_int());
        std::unordered_map<std::string, int> outfit;
        outfit["skin"] = player.child("skin").attribute("value").as_int();
        outfit["head"] = player.child("head").attribute("value").as_int();
        outfit["torso"] = player.child("torso").attribute("value").as_int();
        std::dynamic_pointer_cast<Player>(m_game->entity("player"))->setOutfit(outfit);

        // Inventory
        for (pugi::xml_node item = player.child("inventory").child("item"); item; item = item.next_sibling("item")) {
            if (std::string(item.attribute("type").as_string()).compare("seed") == 0) {
                std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->addToInventory(std::make_shared<Seed>(Crops(item.attribute("seed_type").as_int()), item.attribute("quantity").as_int(), "base", m_game), item.attribute("slot").as_int());
            }
            if (std::string(item.attribute("type").as_string()).compare("yield") == 0) {
                std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->addToInventory(std::make_shared<Yield>(Crops(item.attribute("crop_type").as_int()), item.attribute("quantity").as_int(), CropQuality(item.attribute("quality").as_int()), "base", m_game), item.attribute("slot").as_int());
            }
        }
		// Lockers
		for (pugi::xml_node locker = doc.child("locker"); locker; locker = locker.next_sibling("locker")) {
			for (pugi::xml_node item = locker.child("item"); item; item = item.next_sibling("item")) {
				if (std::string(item.attribute("type").as_string()).compare("seed") == 0) {
					std::dynamic_pointer_cast<ItemLocker>(m_game->entity(locker.attribute("name").as_string()))->addToInventory(std::make_shared<Seed>(Crops(item.attribute("seed_type").as_int()), item.attribute("quantity").as_int(), "base", m_game), item.attribute("slot").as_int());
				}
				if (std::string(item.attribute("type").as_string()).compare("yield") == 0) {
					std::dynamic_pointer_cast<ItemLocker>(m_game->entity(locker.attribute("name").as_string()))->addToInventory(std::make_shared<Yield>(Crops(item.attribute("crop_type").as_int()), item.attribute("quantity").as_int(), CropQuality(item.attribute("quality").as_int()), "base", m_game), item.attribute("slot").as_int());
				}
			}
		}

        // NPCs
        pugi::xml_node npcs = doc.child("npcs");
        std::dynamic_pointer_cast<Npc>(m_game->entity("robby"))->setFriendshipLevel(npcs.child("robby").attribute("like").as_int());
        std::dynamic_pointer_cast<Npc>(m_game->entity("stella"))->setFriendshipLevel(npcs.child("stella").attribute("like").as_int());
        std::dynamic_pointer_cast<Npc>(m_game->entity("al"))->setFriendshipLevel(npcs.child("al").attribute("like").as_int());
        std::dynamic_pointer_cast<Npc>(m_game->entity("antoni"))->setFriendshipLevel(npcs.child("antoni").attribute("like").as_int());

		std::unordered_map<std::string, bool> activities;
		activities["water"] = npcs.child("stella").child("assignments").attribute("water").as_bool();
		activities["weed"] = npcs.child("stella").child("assignments").attribute("weed").as_bool();
		activities["fertilize"] = npcs.child("stella").child("assignments").attribute("fertilize").as_bool();
		activities["spray"] = npcs.child("stella").child("assignments").attribute("spray").as_bool();
		std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->setStellaActivities(activities);

		std::unordered_map<int, bool> plots;
		int plotNumber = 0;
		for (pugi::xml_node plot = npcs.child("stella").child("plots").child("plot"); plot; plot = plot.next_sibling("plot")) {
			plots[plotNumber] = plot.attribute("value").as_bool();
			plotNumber++;
		}
		std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->setStellaPlots(plots);

        // Crops
        pugi::xml_node crops = doc.child("crops");
        std::shared_ptr<Map> base = m_game->map("base");
        for (pugi::xml_node crop = crops.child("base").child("crop"); crop; crop = crop.next_sibling("crop")) {
            int x = crop.attribute("x").as_int();
            int y = crop.attribute("y").as_int();
            int type = crop.attribute("type").as_int();
            int age = crop.attribute("age").as_int();
            base->tillByGrid(x, y);
            base->plantByGrid(x, y, static_cast<Crops>(type));
            base->setAgeOfCropAtTile(x, y, age);
        }
		std::shared_ptr<Map> woods = m_game->map("woods");
		for (pugi::xml_node crop = crops.child("woods").child("crop"); crop; crop = crop.next_sibling("crop")) {
			int x = crop.attribute("x").as_int();
			int y = crop.attribute("y").as_int();
			int type = crop.attribute("type").as_int();
			int age = crop.attribute("age").as_int();
			woods->tillByGrid(x, y);
			woods->plantByGrid(x, y, static_cast<Crops>(type));
			woods->setAgeOfCropAtTile(x, y, age);
		}

		// Weeds
		pugi::xml_node weeds = doc.child("weeds");
		for (pugi::xml_node weed = weeds.child("base").child("weed"); weed; weed = weed.next_sibling("weed")) {
			int x = weed.attribute("x").as_int();
			int y = weed.attribute("y").as_int();
			base->addWeedByGrid(x, y);
		}
		for (pugi::xml_node weed = weeds.child("woods").child("weed"); weed; weed = weed.next_sibling("weed")) {
			int x = weed.attribute("x").as_int();
			int y = weed.attribute("y").as_int();
			woods->addWeedByGrid(x, y);
		}

		// Trees
		pugi::xml_node trees = doc.child("trees");
		for (pugi::xml_node tree = trees.child("base").child("tree"); tree; tree = tree.next_sibling("tree")) {
			int x = tree.attribute("x").as_int();
			int y = tree.attribute("y").as_int();
			std::shared_ptr<Entity> entity = m_game->checkForEntityByGrid(x, y, "base");
			if(entity && entity->type().compare("tree") == 0) std::dynamic_pointer_cast<Tree>(entity)->cutDown();
		}
		for (pugi::xml_node tree = trees.child("woods").child("tree"); tree; tree = tree.next_sibling("tree")) {
			int x = tree.attribute("x").as_int();
			int y = tree.attribute("y").as_int();
			std::shared_ptr<Entity> entity = m_game->checkForEntityByGrid(x, y, "woods");
			if (entity && entity->type().compare("tree") == 0) std::dynamic_pointer_cast<Tree>(entity)->cutDown();
		}

		// Placeable items
		pugi::xml_node items = doc.child("placeables");
		for (pugi::xml_node item = items.child("base").child("placeable"); item; item = item.next_sibling("placeable")) {
			int x = item.attribute("x").as_int();
			int y = item.attribute("y").as_int();
			std::string type = item.attribute("type").as_string();

			if (type.compare("sprinkler") == 0) {
				std::string name = m_game->addEntityWithoutName(std::make_shared<Sprinkler>("base", m_game));
				m_game->entity(name)->setPositionByGrid(x, y);
			}
		}
		for (pugi::xml_node item = items.child("woods").child("placeable"); item; item = item.next_sibling("placeable")) {
			int x = item.attribute("x").as_int();
			int y = item.attribute("y").as_int();
			std::string type = item.attribute("type").as_string();

			if (type.compare("sprinkler") == 0) {
				std::string name = m_game->addEntityWithoutName(std::make_shared<Sprinkler>("woods", m_game));
				m_game->entity(name)->setPositionByGrid(x, y);
			}
		}

        // Emails
        pugi::xml_node emails = doc.child("emails");
        for (pugi::xml_node email = emails.child("email"); email; email = email.next_sibling("email")) {
            std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->archiveEmail(email.attribute("id").as_int());
        }

        // Contracts
        pugi::xml_node contracts = doc.child("contracts");
        for (pugi::xml_node contract = contracts.child("contract"); contract; contract = contract.next_sibling("contract")) {
            if (std::string(contract.attribute("status").as_string()).compare("offered") == 0)
                std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->offerContract(contract.attribute("id").as_int());
            else if (std::string(contract.attribute("status").as_string()).compare("accepted") == 0)
                std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->acceptContract(contract.attribute("id").as_int());
            else if (std::string(contract.attribute("status").as_string()).compare("completed") == 0)
                std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->completeContract(contract.attribute("id").as_int(), false);
            else if (std::string(contract.attribute("status").as_string()).compare("rejected") == 0)
                std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->rejectContract(contract.attribute("id").as_int());
        }

		// Research
		pugi::xml_node research = doc.child("research");
		m_game->researchManager()->research(research.attribute("current").as_string());
		std::vector<std::pair<std::string, int>> researched;
		for (pugi::xml_node item = research.child("item"); item; item = item.next_sibling("item")) {
			std::pair<std::string, int> pair;
			pair.first = item.attribute("name").as_string();
			pair.second = item.attribute("progress").as_int();
			researched.push_back(pair);
		}
		if (researched.size() > 0) m_game->researchManager()->unlockResearchItems(researched);
    }
    else {
        std::cout << "Load result: " << filePath.str() << " " << result.description() << std::endl;
        return false;
    }

    return true;
}

bool SaveLoad::saveFile(int slot) {
	if (slot < 0) {
		std::cout << "Invalid save slot: " << slot << std::endl;
		return false;
	}

    std::stringstream filePath;
    filePath << SAVES_PATH << "save_" << slot << ".xml";

    pugi::xml_document doc;

    // Game values
    doc.append_child("game");
    doc.child("game").append_child("year");
    doc.child("game").child("year").append_attribute("value");
    doc.child("game").child("year").attribute("value").set_value(m_game->dayNight()->year());
    doc.child("game").append_child("month");
    doc.child("game").child("month").append_attribute("value");
    doc.child("game").child("month").attribute("value").set_value(m_game->dayNight()->month());
    doc.child("game").append_child("day");
    doc.child("game").child("day").append_attribute("value");
    doc.child("game").child("day").attribute("value").set_value(m_game->dayNight()->day());

    // Player values
    doc.append_child("player");
    doc.child("player").append_child("funds");
    doc.child("player").child("funds").append_attribute("value");
    doc.child("player").child("funds").attribute("value").set_value(std::dynamic_pointer_cast<Player>(m_game->entity("player"))->currentFunds());
    doc.child("player").append_child("skin");
    doc.child("player").child("skin").append_attribute("value");
    doc.child("player").child("skin").attribute("value").set_value(std::dynamic_pointer_cast<Player>(m_game->entity("player"))->outfit()["skin"]);
    doc.child("player").append_child("head");
    doc.child("player").child("head").append_attribute("value");
    doc.child("player").child("head").attribute("value").set_value(std::dynamic_pointer_cast<Player>(m_game->entity("player"))->outfit()["head"]);
    doc.child("player").append_child("torso");
    doc.child("player").child("torso").append_attribute("value");
    doc.child("player").child("torso").attribute("value").set_value(std::dynamic_pointer_cast<Player>(m_game->entity("player"))->outfit()["torso"]);


    // Items
    doc.child("player").append_child("inventory");
    std::vector<std::shared_ptr<ItemSlot>> items = std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->allItemSlots();
    std::vector<std::shared_ptr<ItemSlot>>::iterator itemIt;
    for (itemIt = items.begin(); itemIt != items.end(); itemIt++) {
        if ((*itemIt)->item()) {
            pugi::xml_node item = doc.child("player").child("inventory").append_child("item");
            item.append_attribute("slot");
            item.attribute("slot").set_value((*itemIt)->slotNumber());
            item.append_attribute("type");
            item.attribute("type").set_value((*itemIt)->item()->type().c_str());
            item.append_attribute("quantity");
            item.attribute("quantity").set_value((*itemIt)->item()->quantity());

            // Item properties
            std::unordered_map<std::string, std::string> props = (*itemIt)->item()->properties();
            std::unordered_map<std::string, std::string>::iterator propsItr;
            for (propsItr = props.begin(); propsItr != props.end(); propsItr++) {
                item.append_attribute(propsItr->first.c_str());
                item.attribute(propsItr->first.c_str()).set_value(propsItr->second.c_str());
            }
        }
    }

	std::vector<std::shared_ptr<ItemLocker>> lockers = m_game->allLockers();
	std::vector<std::shared_ptr<ItemLocker>>::iterator lockIt;
	for (lockIt = lockers.begin(); lockIt != lockers.end(); lockIt++) {
		pugi::xml_node locker = doc.append_child("locker");
		locker.append_attribute("name");
		locker.attribute("name").set_value((*lockIt)->name().c_str());
		items = (*lockIt)->allItemSlots();
		std::vector<std::shared_ptr<ItemSlot>>::iterator lockItemIt;
		for (lockItemIt = items.begin(); lockItemIt != items.end(); lockItemIt++) {
			if ((*lockItemIt)->item()) {
				pugi::xml_node item = locker.append_child("item");
				item.append_attribute("slot");
				item.attribute("slot").set_value((*lockItemIt)->slotNumber());
				item.append_attribute("type");
				item.attribute("type").set_value((*lockItemIt)->item()->type().c_str());
				item.append_attribute("quantity");
				item.attribute("quantity").set_value((*lockItemIt)->item()->quantity());

				// Item properties
				std::unordered_map<std::string, std::string> props = (*lockItemIt)->item()->properties();
				std::unordered_map<std::string, std::string>::iterator propsItr;
				for (propsItr = props.begin(); propsItr != props.end(); propsItr++) {
					item.append_attribute(propsItr->first.c_str());
					item.attribute(propsItr->first.c_str()).set_value(propsItr->second.c_str());
				}
			}
		}
	}

    // NPC values
    doc.append_child("npcs");
    doc.child("npcs").append_child("robby");
    doc.child("npcs").child("robby").append_attribute("like");
    doc.child("npcs").child("robby").attribute("like").set_value(std::dynamic_pointer_cast<Npc>(m_game->entity("robby"))->friendshipLevel());
    
	std::unordered_map<std::string, bool> activities = std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->stellaActivities();
	std::unordered_map<int, bool> plots = std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->stellaPlots();
	doc.child("npcs").append_child("stella");
    doc.child("npcs").child("stella").append_attribute("like");
    doc.child("npcs").child("stella").attribute("like").set_value(std::dynamic_pointer_cast<Npc>(m_game->entity("stella"))->friendshipLevel());
	doc.child("npcs").child("stella").append_child("assignments");
	doc.child("npcs").child("stella").child("assignments").append_attribute("water");
	doc.child("npcs").child("stella").child("assignments").attribute("water").set_value(activities["water"]);
	doc.child("npcs").child("stella").child("assignments").append_attribute("weed");
	doc.child("npcs").child("stella").child("assignments").attribute("weed").set_value(activities["weed"]);
	doc.child("npcs").child("stella").child("assignments").append_attribute("fertilize");
	doc.child("npcs").child("stella").child("assignments").attribute("fertilize").set_value(activities["fertilize"]);
	doc.child("npcs").child("stella").child("assignments").append_attribute("spray");
	doc.child("npcs").child("stella").child("assignments").attribute("spray").set_value(activities["spray"]);
	doc.child("npcs").child("stella").append_child("plots");
	std::unordered_map<int, bool>::iterator plotsItr;
	for (plotsItr = plots.begin(); plotsItr != plots.end(); plotsItr++) {
		std::stringstream ss;
		ss << "plot" << plotsItr->first;
		pugi::xml_node plot = doc.child("npcs").child("stella").child("plots").append_child("plot");
		plot.append_attribute("value");
		plot.attribute("value").set_value(plotsItr->second);
	}

    doc.child("npcs").append_child("al");
    doc.child("npcs").child("al").append_attribute("like");
    doc.child("npcs").child("al").attribute("like").set_value(std::dynamic_pointer_cast<Npc>(m_game->entity("al"))->friendshipLevel());
    doc.child("npcs").append_child("antoni");
    doc.child("npcs").child("antoni").append_attribute("like");
    doc.child("npcs").child("antoni").attribute("like").set_value(std::dynamic_pointer_cast<Npc>(m_game->entity("antoni"))->friendshipLevel());

    // Crops
    doc.append_child("crops");
	doc.child("crops").append_child("base");
    std::vector<std::shared_ptr<Crop>> crops = m_game->allPlantedCrops("base");
    std::vector<std::shared_ptr<Crop>>::iterator cropIt;
    for (cropIt = crops.begin(); cropIt != crops.end(); cropIt++) {
        pugi::xml_node crop = doc.child("crops").child("base").append_child("crop");
        crop.append_attribute("x");
        crop.append_attribute("y");
        crop.append_attribute("type");
        crop.append_attribute("age");
        crop.attribute("x").set_value((*cropIt)->gridPosition().x);
        crop.attribute("y").set_value((*cropIt)->gridPosition().y);
        crop.attribute("type").set_value((*cropIt)->type());
        crop.attribute("age").set_value((*cropIt)->age());
    }
	doc.child("crops").append_child("woods");
	crops = m_game->allPlantedCrops("woods");
	for (cropIt = crops.begin(); cropIt != crops.end(); cropIt++) {
		pugi::xml_node crop = doc.child("crops").child("woods").append_child("crop");
		crop.append_attribute("x");
		crop.append_attribute("y");
		crop.append_attribute("type");
		crop.append_attribute("age");
		crop.attribute("x").set_value((*cropIt)->gridPosition().x);
		crop.attribute("y").set_value((*cropIt)->gridPosition().y);
		crop.attribute("type").set_value((*cropIt)->type());
		crop.attribute("age").set_value((*cropIt)->age());
	}

	// Weeds
	doc.append_child("weeds");
	doc.child("weeds").append_child("base");
	std::vector<std::pair<int, int>> weeds = m_game->allWeeds("base");
	std::vector<std::pair<int, int>>::iterator weedIt;
	for (weedIt = weeds.begin(); weedIt != weeds.end(); weedIt++) {
		pugi::xml_node weed = doc.child("weeds").child("base").append_child("weed");
		weed.append_attribute("x");
		weed.append_attribute("y");
		weed.attribute("x").set_value((*weedIt).first);
		weed.attribute("y").set_value((*weedIt).second);
	}
	doc.child("weeds").append_child("woods");
	weeds = m_game->allWeeds("woods");
	for (weedIt = weeds.begin(); weedIt != weeds.end(); weedIt++) {
		pugi::xml_node weed = doc.child("weeds").child("woods").append_child("weed");
		weed.append_attribute("x");
		weed.append_attribute("y");
		weed.attribute("x").set_value((*weedIt).first);
		weed.attribute("y").set_value((*weedIt).second);
	}

	// Trees
	doc.append_child("trees");
	doc.child("trees").append_child("base");
	std::vector<std::pair<int, int>> cutTrees = m_game->cutTrees("base");
	std::vector<std::pair<int, int>>::iterator treeIt;
	for (treeIt = cutTrees.begin(); treeIt != cutTrees.end(); treeIt++) {
		pugi::xml_node tree = doc.child("trees").child("base").append_child("tree");
		tree.append_attribute("x");
		tree.append_attribute("y");
		tree.attribute("x").set_value((*treeIt).first);
		tree.attribute("y").set_value((*treeIt).second);
	}
	doc.child("trees").append_child("woods");
	cutTrees = m_game->cutTrees("woods");
	for (treeIt = cutTrees.begin(); treeIt != cutTrees.end(); treeIt++) {
		pugi::xml_node tree = doc.child("trees").child("woods").append_child("tree");
		tree.append_attribute("x");
		tree.append_attribute("y");
		tree.attribute("x").set_value((*treeIt).first);
		tree.attribute("y").set_value((*treeIt).second);
	}

	// Placeable objects
	doc.append_child("placeables");
	doc.child("placeables").append_child("base");
	std::vector<std::shared_ptr<Entity>> placeables = m_game->allPlaceables("base");
	std::vector<std::shared_ptr<Entity>>::iterator placeIt;
	for (placeIt = placeables.begin(); placeIt != placeables.end(); placeIt++) {
		pugi::xml_node placeable = doc.child("placeables").child("base").append_child("placeable");
		placeable.append_attribute("x");
		placeable.append_attribute("y");
		placeable.append_attribute("type");
		placeable.attribute("x").set_value((*placeIt)->gridPosition().x);
		placeable.attribute("y").set_value((*placeIt)->gridPosition().y);
		placeable.attribute("type").set_value((*placeIt)->type().c_str());
	}

    // Emails
    doc.append_child("emails");
    std::vector<int> archivedEmails = std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->archivedEmailIds();
    std::vector<int>::iterator emailIt;
    for (emailIt = archivedEmails.begin(); emailIt != archivedEmails.end(); emailIt++) {
        pugi::xml_node email = doc.child("emails").append_child("email");
        email.append_attribute("id");
        email.attribute("id").set_value((*emailIt));
    }

    // Contracts
    doc.append_child("contracts");
    std::vector<int> offeredContracts = std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->offeredContractIds();
    for (emailIt = offeredContracts.begin(); emailIt != offeredContracts.end(); emailIt++) {
        pugi::xml_node contract = doc.child("contracts").append_child("contract");
        contract.append_attribute("id");
        contract.attribute("id").set_value((*emailIt));
        contract.append_attribute("status");
        contract.attribute("status").set_value("offered");
    }
    std::vector<int> acceptedContracts = std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->acceptedContractIds();
    for (emailIt = acceptedContracts.begin(); emailIt != acceptedContracts.end(); emailIt++) {
        pugi::xml_node contract = doc.child("contracts").append_child("contract");
        contract.append_attribute("id");
        contract.attribute("id").set_value((*emailIt));
        contract.append_attribute("status");
        contract.attribute("status").set_value("accepted");
    }
    std::vector<int> completedContracts = std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->completedContractIds();
    for (emailIt = completedContracts.begin(); emailIt != completedContracts.end(); emailIt++) {
        pugi::xml_node contract = doc.child("contracts").append_child("contract");
        contract.append_attribute("id");
        contract.attribute("id").set_value((*emailIt));
        contract.append_attribute("status");
        contract.attribute("status").set_value("completed");
    }
    std::vector<int> rejectedContracts = std::dynamic_pointer_cast<ComputerGui>(m_game->gui("computer"))->rejectedContractIds();
    for (emailIt = rejectedContracts.begin(); emailIt != rejectedContracts.end(); emailIt++) {
        pugi::xml_node contract = doc.child("contracts").append_child("contract");
        contract.append_attribute("id");
        contract.attribute("id").set_value((*emailIt));
        contract.append_attribute("status");
        contract.attribute("status").set_value("rejected");
    }

	// Research
	doc.append_child("research");
	doc.child("research").append_attribute("current");
	doc.child("research").attribute("current").set_value(m_game->researchManager()->currentResearchId().c_str());
	std::unordered_map<std::string, ResearchItem> resItems = m_game->researchManager()->researchItems();
	std::unordered_map<std::string, ResearchItem>::iterator resItr;
	for (resItr = resItems.begin(); resItr != resItems.end(); resItr++) {
		pugi::xml_node item = doc.child("research").append_child("item");
		item.append_attribute("name");
		item.attribute("name").set_value(resItr->first.c_str());
		item.append_attribute("progress");
		item.attribute("progress").set_value(resItr->second.m_progress);
	}

    doc.save_file(filePath.str().c_str());
    return true;
}

int SaveLoad::nextOpenSaveSlot() {
	int i = 0;
	while (true) {
		if (!fileExists(i)) {
			return i;
		}
		i++;
	}
}

bool SaveLoad::fileExists(int slotNumber) {
	std::stringstream filePath;
	filePath << SAVES_PATH << "save_" << slotNumber << ".xml";
	std::ifstream f;
	f.open(filePath.str());
	bool fileExists = f.is_open();
	f.close();
	return fileExists;
}

const int SaveLoad::currentSaveSlot() {
	return m_currentSaveSlot;
}
