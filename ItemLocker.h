
// Copyright 2019 Happyrock Studios

#ifndef ITEMLOCKER_H
#define ITEMLOCKER_H

// Moon
#include "Entity.h"
class Item;
class InventoryGui;
class ItemSlot;

class ItemLocker : public Entity {
public:
    ItemLocker(std::string name,
           std::string currentMap,
           std::shared_ptr<Game> game);

    void defineAnimations() {}

    void update(double dt);

    void open();

    void close();

    void toggle();

    std::shared_ptr<Item> handleClick(sf::Event event, std::shared_ptr<Item> heldItem);

    std::shared_ptr<Item> removeFromInventory(int slot = -1);

    std::shared_ptr<Item> addToInventory(std::shared_ptr<Item> item, int slot = -1);

	std::shared_ptr<Item> addToInventory(std::string itemType, int slot = -1);

    std::vector<std::shared_ptr<ItemSlot>> allItemSlots();

    void setDisplayText(std::string text);

    void setDisplayBelowText(std::string text);

    int count(std::string itemType);

	bool full();

private:
    std::shared_ptr<InventoryGui> m_inventory;

    bool m_open;

    int m_maxItems;

};

#endif // ITEMLOCKER_H
