
// Copyright 2019 Happyrock Studios

// Moon
#include "Entity.h"
#include "Game.h"

// STD
#include <iostream>

Entity::Entity(std::string name,
               std::string type,
               std::string texturePath,
               std::string currentMap,
               std::shared_ptr<Game> game,
               int spriteWidth,
               int spriteHeight) {

    m_name          = name;
    m_type          = type;
    m_texturePath   = texturePath;
    m_position      = sf::Vector2f();
    m_currentMap    = currentMap;
    m_game          = game;
    m_direction     = DOWN;
    m_obstacle      = false;
	m_fadingIn		= false;
	m_fadingOut		= false;
	m_shakeAmount	= -1;
	m_spriteScale = sf::Vector2f(1, 1);

    m_hitbox = sf::IntRect();
    m_hitboxOffset = sf::Vector2i();

    loadSprite(spriteWidth, spriteHeight);
    defineHitbox();

    m_animationClock.resume();
}

void Entity::loadSprite(int spriteWidth, int spriteHeight) {
    m_sprite = std::make_shared<sf::Sprite>();
    m_sprite->setTexture(*m_game->texture(m_texturePath));
    m_sprite->setPosition(m_position);
    m_sprite->setTextureRect(sf::IntRect(0, 0, spriteWidth, spriteHeight));
    m_sprite->setOrigin(0,0);
	m_sprites["background"].push_back(m_sprite);
}

void Entity::scaleSprites(float xScale, float yScale) {
    std::unordered_map<std::string, std::vector<std::shared_ptr<sf::Sprite>>>::iterator itr;
    std::vector<std::shared_ptr<sf::Sprite>>::iterator itr2;
    for (itr = m_sprites.begin(); itr != m_sprites.end(); itr++) {
        for(itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
            (*itr2)->setScale(xScale, yScale);
        }
    }

    m_hitbox.width *= xScale;
    m_hitbox.height *= yScale;

	m_spriteScale.x *= xScale;
	m_spriteScale.y *= yScale;
}

void Entity::defineHitbox() {
    m_hitboxSprite.setFillColor(sf::Color(100, 250, 50, 180));
    m_hitbox = sf::IntRect(m_position.x,
                           m_position.y,
                           m_sprite->getTextureRect().width,
                           m_sprite->getTextureRect().height);
}

void Entity::update(double dt) {
    std::unordered_map<std::string, std::vector<std::shared_ptr<sf::Sprite>>>::iterator itr;
    std::vector<std::shared_ptr<sf::Sprite>>::iterator itr2;
    for (itr = m_sprites.begin(); itr != m_sprites.end(); itr++) {
        for(itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
            (*itr2)->setPosition(m_position);
			if (m_fadingIn) {
				if (m_sprite->getColor().a < 255) (*itr2)->setColor(sf::Color(255, 255, 255, m_sprite->getColor().a + 1));
				else m_fadingIn = false;
			}
			if (m_fadingOut) {
				if (m_sprite->getColor().a > 0) (*itr2)->setColor(sf::Color(255, 255, 255, m_sprite->getColor().a - 1));
				else m_fadingOut = false;
			}
        }
    }

    m_hitbox.left = m_position.x + m_hitboxOffset.x;
    m_hitbox.top = m_position.y + m_hitboxOffset.y;
    m_hitboxSprite.setPosition(sf::Vector2f(m_hitbox.left, m_hitbox.top));
    m_hitboxSprite.setSize(sf::Vector2f(m_hitbox.width, m_hitbox.height));

    // TODO - this is a little fuzzy.
    m_gridPosition = m_game->pixelsToGrid(m_position.x, m_position.y);

    if (m_animationClock.getElapsedTime().asSeconds() >= ANIMATION_SPEED * m_animations[m_currentAnimation].m_animationSpeed) {
        animate();
        m_animationClock.reset(true);
    }
}

void Entity::draw(std::string layerName) {
    std::vector<std::shared_ptr<sf::Sprite>>::iterator itr;
    for(itr = m_sprites[layerName].begin(); itr != m_sprites[layerName].end(); itr++) {
        m_game->renderWindow()->draw(**itr);
    }
    //m_game->renderWindow()->draw(m_hitboxSprite);
}

void Entity::setAnimation(std::string animName) {
    if (m_animations.count(animName) == 0) {
        std::cout << "Animation " << animName << " does not exist for entity: " << m_name << std::endl;
        return;
    }

    if (m_currentAnimation.compare(animName) != 0) {
        m_animations[animName].m_currentFrame = 1;
        m_animationClock.reset(true);
        m_animationClock.add(sf::seconds(2.f));
    }

    m_currentAnimation = animName;
}

void Entity::animate() {
    if (m_currentAnimation.compare("") == 0) return;
    if (m_animations.find(m_currentAnimation) == m_animations.end()) return;

    sf::IntRect currentFrameSize = m_animations[m_currentAnimation].m_frameInfo;

    m_animations[m_currentAnimation].m_currentFrame++;

    if (m_animations[m_currentAnimation].m_currentFrame > m_animations[m_currentAnimation].m_numberOfFrames)
        m_animations[m_currentAnimation].m_currentFrame = 1;

    std::unordered_map<std::string, std::vector<std::shared_ptr<sf::Sprite>>>::iterator itr;
    std::vector<std::shared_ptr<sf::Sprite>>::iterator itr2;
    for (itr = m_sprites.begin(); itr != m_sprites.end(); itr++) {
        for(itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
            (*itr2)->setTextureRect(sf::IntRect(currentFrameSize.left + (currentFrameSize.width * (m_animations[m_currentAnimation].m_currentFrame - 1)),
                                         currentFrameSize.top,
                                         currentFrameSize.width,
                                         currentFrameSize.height));
        }
    }
}

//////////////////////////////////////////////////
//          Helper Functions                    //
//////////////////////////////////////////////////

bool Entity::clickedOn(float x, float y, bool respond) {
    if (x >= m_position.x && x <= m_position.x + m_hitbox.width
        && y >= m_position.y && y <= m_position.y + m_hitbox.height) {
        if (respond) {
            // Do something...?
        }
        return true;
    }

    return false;
}

// TODO
void Entity::shake(float amount) {
	m_shakeAmount = amount;
}

void Entity::fadeOut() {
	m_fadingOut = true;
}

void Entity::fadeIn() {
	m_fadingIn = true;
}

//////////////////////////////////////////////////
//          Getters and Setters                 //
//////////////////////////////////////////////////

void Entity::rename(std::string newName) {
	m_name = newName;
}

const std::string Entity::name() {
    return m_name;
}

const std::string Entity::type() {
    return m_type;
}

const std::string Entity::texturePath() {
    return m_texturePath;
}

void Entity::setCurrentMap(std::string newMap) {
    m_currentMap = newMap;
}

const std::string Entity::currentMap() {
    return m_currentMap;
}

void Entity::setPositionByGrid(int x, int y) {
    m_gridPosition = sf::Vector2i(x, y);
    m_position = m_game->gridToPixels(x, y);

    // Adjust for sprite origin
    m_position.x += m_sprite->getOrigin().x * m_sprite->getScale().x;
    m_position.y += m_sprite->getOrigin().y * m_sprite->getScale().y;
}

void Entity::setPositionByGrid(sf::Vector2i vec) {
    m_gridPosition = vec;
    m_position = m_game->gridToPixels(vec.x, vec.y);

    // Adjust for sprite origin
    m_position.x += m_sprite->getOrigin().x * m_sprite->getScale().x;
    m_position.y += m_sprite->getOrigin().y * m_sprite->getScale().y;
}

void Entity::setPositionByPixel(float x, float y) {
    m_position = sf::Vector2f(x, y);
    m_gridPosition = m_game->pixelsToGrid(m_position.x, m_position.y);
    //m_position.x += m_sprite->getOrigin().x;
    //m_position.y += m_sprite->getOrigin().y;
}

void Entity::setPositionByPixel(sf::Vector2f vec) {
    m_position = vec;
    m_gridPosition = m_game->pixelsToGrid(m_position.x, m_position.y);
    //m_position.x += m_sprite->getOrigin().x;
    //m_position.y += m_sprite->getOrigin().y;
}

void Entity::setSpritesPosition(sf::Vector2f vec) {
    std::unordered_map<std::string, std::vector<std::shared_ptr<sf::Sprite>>>::iterator itr;
    std::vector<std::shared_ptr<sf::Sprite>>::iterator itr2;
    for (itr = m_sprites.begin(); itr != m_sprites.end(); itr++) {
        for(itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
            (*itr2)->setPosition(vec);
        }
    }
}

sf::Vector2f Entity::spriteScale() {
	return m_spriteScale;
}

const sf::Vector2f Entity::pixelPosition() {
    return m_position;
}

const sf::Vector2i Entity::gridPosition() {
    return m_gridPosition;
}

std::shared_ptr<sf::Sprite> Entity::sprite() {
    return m_sprite;
}

const Direction Entity::direction() {
	return m_direction;
}

void Entity::setDirection(Direction direction) {
	m_direction = direction;
}

void Entity::setDirection(std::string directionString) {
	if (directionString.compare("left") == 0) m_direction = LEFT;
	else if (directionString.compare("right") == 0) m_direction = RIGHT;
	else if (directionString.compare("up") == 0) m_direction = UP;
	else if (directionString.compare("down") == 0) m_direction = DOWN;
	else if (directionString.compare("uleft") == 0) m_direction = ULEFT;
	else if (directionString.compare("uright") == 0) m_direction = URIGHT;
	else if (directionString.compare("dleft") == 0) m_direction = DLEFT;
	else if (directionString.compare("dright") == 0) m_direction = DRIGHT;
	else std::cout << "Invalid direction: " << directionString << std::endl;
}

void Entity::setVisibility(bool visibility) {
	std::unordered_map<std::string, std::vector<std::shared_ptr<sf::Sprite>>>::iterator itr;
	std::vector<std::shared_ptr<sf::Sprite>>::iterator itr2;
	for (itr = m_sprites.begin(); itr != m_sprites.end(); itr++) {
		for (itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			if (visibility) (*itr2)->setColor(sf::Color(255, 255, 255, 255));
			else (*itr2)->setColor(sf::Color(255, 255, 255, 0));
		}
	}
}

const bool Entity::visible() {
	return m_sprite->getColor().a == 255;
}
