
// Copyright 2019 Happyrock Studios

// Moon
#include "Game.h"
#include "TitleScreen.h"

// STD
#include <memory>
#include <time.h>
#include <iostream>

int main()
{
    srand (time(NULL));

    std::shared_ptr<Game> game = std::make_shared<Game>();
	game->setup();
	std::shared_ptr<TitleScreen> titleScreen = std::make_shared<TitleScreen>(game->game());
	unsigned int saveSlot = titleScreen->run();
	game->loadSaveSlot(saveSlot);
    game->run();

    return 0;
}
