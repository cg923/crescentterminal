
// Copyright 2019 Happyrock Studios

// Moon
#include "Tree.h"
#include "Game.h"
#include "Map.h"

// STD
#include <iostream>

Tree::Tree(std::string mapName, std::shared_ptr<Game> game, int gridX, int gridY, int spriteWidth, int spriteHeight)
    : Entity("tree", "tree", "tree", mapName, game, spriteWidth, spriteHeight) {

    m_stumpSprite = std::make_shared<sf::Sprite>();
    m_stumpSprite->setTexture(*m_game->texture(m_texturePath));
    m_stumpSprite->setPosition(m_position);
    m_stumpSprite->setTextureRect(sf::IntRect(spriteWidth, 0, spriteWidth, spriteHeight));

    m_trunkSprite = std::make_shared<sf::Sprite>();
    m_trunkSprite->setTexture(*m_game->texture(m_texturePath));
    m_trunkSprite->setPosition(m_position);
    m_trunkSprite->setTextureRect(sf::IntRect(spriteWidth*2, 0, spriteWidth, spriteHeight));

    m_sprites["background"].push_back(m_stumpSprite);
    m_sprites["background"].push_back(m_trunkSprite);
    m_sprites["foreground"].push_back(m_sprite);

    // Provide a little variety to the trees.
//    int direction = (rand() % 2) + 1;
//    if (direction == 2) {
//        m_stumpSprite->scale(-1, 1);
//        m_trunkSprite->scale(-1, 1);
//        m_sprite->scale(-1, 1);
//    }

    int red = (rand() % 255) + 155;
    int green = (rand() % 100) + 50;
    int blue = (rand() % 200) + 100;
    m_stumpSprite->setColor(sf::Color(red, green, blue, 255));
    m_trunkSprite->setColor(sf::Color(red, green, blue, 255));
    m_sprite->setColor(sf::Color(red, green, blue, 255));

    m_initGridPos = sf::Vector2i(gridX, gridY);
    m_gridPosition = sf::Vector2i(gridX, gridY);

    m_cut = false;
	m_init = false;
	m_health = 100;
}

void Tree::update(double dt) {
    Entity::update(dt);

	// Calling either of these functions in the constructor breaks, so we have to do this.
	if (!m_init) {
		setPositionByGrid(m_initGridPos);
		m_game->map(m_currentMap)->makeTileAnObstacle(m_initGridPos.x, m_initGridPos.y);
		m_init = true;
	}

    m_sprite->setPosition(m_position - sf::Vector2f(32,105));
    m_stumpSprite->setPosition(m_position - sf::Vector2f(32,105));
    m_trunkSprite->setPosition(m_position - sf::Vector2f(32,105));
}

void Tree::draw(std::string layerName) {
	if (!m_cut) {
		Entity::draw(layerName);
	}
	else {
		if (layerName.compare("background") == 0) m_game->renderWindow()->draw(*m_stumpSprite);
	}
}

void Tree::cut() {
	if (m_health <= 0) {
		m_cut = true;
		//m_game->map(m_currentMap)->makeTileNotAnObstacle(m_initGridPos.x, m_initGridPos.y);
	}
	else {
		m_health--;
	}
}

void Tree::cutDown() {
	m_cut = true;
	//m_game->map(m_currentMap)->makeTileNotAnObstacle(m_initGridPos.x, m_initGridPos.y);
}

bool Tree::isCut() {
	return m_cut;
}
