

// Copyright 2019 Happyrock Studios

// Moon
#include "Game.h"
#include "Npc.h"
#include "DayNight.h"
#include "Map.h"

// STD
#include <math.h>
#include <iostream>
#include <sstream>

Npc::Npc(std::string name,
	std::string texturePath,
	std::string currentMap,
	Direction direction,
	int friendshipLevel,
	std::shared_ptr<Game> game)
	: Character(name, "npc", texturePath, currentMap, direction, game) {

	m_maxSpeed = 0.5;
	m_speed = 0.5;
	m_up = false;
	m_down = false;
	m_left = false;
	m_right = false;
	m_walking = false;
	m_facePlayer = false;

	m_friendshipLevel = friendshipLevel;
	m_spokeMorning = false;
	m_spokeAfternoon = false;
	m_spokeEvening = false;

	m_currentActivity = std::make_shared<Activity>("upstairs", sf::Vector2i(14, 3), "sleep", DOWN);
	m_nextActivity = NULL;

	m_sprites["middle"].push_back(m_sprite);

	scaleSprites(1.5, 1.5);

	sf::IntRect texRect = m_sprite->getTextureRect();
	m_animations["stopped_working_down"] = Animation{ "stopped_working_down", sf::IntRect(0, texRect.height * 5, texRect.width, texRect.height), 4, true, 1, 1 };
	m_animations["stopped_working_right"] = m_animations["stopped_working_down"];
	m_animations["stopped_working_left"] = m_animations["stopped_working_down"];
	m_animations["stopped_working_up"] = m_animations["stopped_working_down"];
}

void Npc::update(double dt) {

    if (!m_facePlayer)
        m_facePlayerClock.reset(true);
    else {
        if (m_facePlayerClock.getElapsedTime().asSeconds() > 3)
            m_facePlayer = false;
        else {
            animate();
            return;
        }
    }

    Character::update(dt);

    if (m_schedule[m_game->dayOfWeek()].find(m_game->dayNight()->fullTime()) != m_schedule[m_game->dayOfWeek()].end())
        m_nextActivity = m_schedule[m_game->dayOfWeek()][m_game->dayNight()->fullTime()];

    if(m_nextActivity) {
		m_working = false;
        // Head to a different map.
        if(m_nextActivity->m_mapName.compare(m_currentMap) != 0) {
            if(m_nextActivity->m_mapName.compare("upstairs") == 0) {
                if (m_currentMap.compare("base") == 0) {
                    m_walkTarget = sf::Vector2i(22,17);
                    m_warpTo = "upstairs";
                }
            }
            else if (m_nextActivity->m_mapName.compare("base") == 0) {
                if (m_currentMap.compare("upstairs") == 0) {
                    m_walkTarget = sf::Vector2i(4,10);
                    m_warpTo = "base";
                }
            }
            else if (m_nextActivity->m_mapName.compare("woods") == 0) {
				// TODO
            }
        }
        // Walk to another position
        else if (m_nextActivity->m_gridPosition != m_gridPosition) {
            m_walkTarget = m_nextActivity->m_gridPosition;
        }
        // Nothing left to do.
        else {
            m_currentActivity = m_nextActivity;
            m_nextActivity = NULL;
            m_walkTarget = sf::Vector2i();
            m_walking = false;
            m_direction = m_currentActivity->m_direction;
			doCurrentActivity();
        }
    }
}

std::string Npc::whatDialogue() {
    std::string friendliness;
    if (m_friendshipLevel <= HOSTILE) {
        friendliness = "hostile";
    }
    else if (m_friendshipLevel > HOSTILE && m_friendshipLevel <= HATE) {
        friendliness = "hate";
    }
    else if (m_friendshipLevel > HATE && m_friendshipLevel <= DISLIKE) {
        friendliness = "dislike";
    }
    else if (m_friendshipLevel > DISLIKE && m_friendshipLevel <= AMBIVALENT) {
        friendliness = "ambivalent";
    }
    else if (m_friendshipLevel > AMBIVALENT && m_friendshipLevel <= LIKE) {
        friendliness = "like";
    }
    else if (m_friendshipLevel > LIKE && m_friendshipLevel < LOVE) {
        friendliness = "love";
    }
    else if (m_friendshipLevel > LOVE) {
        friendliness = "besties";
    }

    std::string timeOfDay = m_game->timeOfDayAsString();

    // Throw some randomness in for good measure.
    int random = rand() % 5 + 1;
    std::stringstream dialogue;
    dialogue << name() << "_" << friendliness << "_" << timeOfDay << "_" << random;

    if (timeOfDay.compare("morning") == 0) m_spokeMorning = true;
    else if (timeOfDay.compare("afternoon") == 0) m_spokeAfternoon = true;
    else if (timeOfDay.compare("evening") == 0) m_spokeEvening = true;

    return dialogue.str();
}

const int Npc::friendshipLevel() {
    return m_friendshipLevel;
}

void Npc::setFriendshipLevel(int level) {
    m_friendshipLevel = level;
}

void Npc::giveSchedule(std::unordered_map<std::string, std::unordered_map<std::string, std::shared_ptr<Activity>>> schedule) {
    m_schedule = schedule;
}

std::unordered_map<std::string, std::shared_ptr<Activity>> Npc::getSchedule(std::string dayOfWeek) {
	if (m_schedule.count(dayOfWeek) > 0) return m_schedule[dayOfWeek];
	else {
		std::cout << dayOfWeek << " is not a valid day of the week or is not stored in schedule" << std::endl;
	}
}

const bool Npc::spokeMorning() {
    return m_spokeMorning;
}

const bool Npc::spokeAfternoon() {
    return m_spokeAfternoon;
}

const bool Npc::spokeEvening() {
    return m_spokeEvening;
}

void Npc::advanceDay() {
    m_spokeMorning = false;
    m_spokeAfternoon = false;
    m_spokeEvening = false;
	// TODO - Hmm....
    m_currentActivity = std::make_shared<Activity>("upstairs", sf::Vector2i(14,3), "sleep", DOWN);
    m_nextActivity = NULL;
    m_walkTarget = sf::Vector2i();
    m_warpTo = "";
}

void Npc::facePlayer() {
    m_facePlayer = true;

    sf::Vector2i playerPos = m_game->entity("player")->gridPosition();

    if (playerPos.x < m_gridPosition.x) m_direction = LEFT;
    else if (playerPos.x > m_gridPosition.x) m_direction = RIGHT;
    else if (playerPos.y < m_gridPosition.y) m_direction = UP;
    else if (playerPos.y >= m_gridPosition.y) m_direction = DOWN;

    switch (m_direction) {
    case LEFT:
        setAnimation("stopped_left");
        break;
    case RIGHT:
        setAnimation("stopped_right");
        break;
    case UP:
        setAnimation("stopped_up");
        break;
    case DOWN:
        setAnimation("stopped_down");
        break;
    default:
        break;
    }
}

std::string Npc::getStatus() {
	if (m_friendshipLevel <= HOSTILE) {
		if (name().compare("robby") == 0) return "I could be better, honestly.";
		else if (name().compare("stella") == 0) return "Whatever.";
		else if (name().compare("al") == 0) return "Get fucked, kid.";
		else if (name().compare("antoni") == 0) return "...";
	}
	else if (m_friendshipLevel > HOSTILE && m_friendshipLevel <= HATE) {
		if (name().compare("robby") == 0) return "Uh, I'm okay I guess.";
		else if (name().compare("stella") == 0) return "Don't start with me, please.";
		else if (name().compare("al") == 0) return "Seriously?";
		else if (name().compare("antoni") == 0) return "...";
	}
	else if (m_friendshipLevel > HATE && m_friendshipLevel <= DISLIKE) {
		if (name().compare("robby") == 0) return "Things are okay. How are you?";
		else if (name().compare("stella") == 0) return "I'm fine, Doctor. Yourself?";
		else if (name().compare("al") == 0) return "Eh, whatever.";
		else if (name().compare("antoni") == 0) return "Ok.";
	}
	else if (m_friendshipLevel > DISLIKE && m_friendshipLevel <= AMBIVALENT) {
		if (name().compare("robby") == 0) return "Pretty good. Things seem to be humming along.";
		else if (name().compare("stella") == 0) return "I'm okay. And you?";
		else if (name().compare("al") == 0) return "Mmm.";
		else if (name().compare("antoni") == 0) return "Good.";
	}
	else if (m_friendshipLevel > AMBIVALENT && m_friendshipLevel <= LIKE) {
		if (name().compare("robby") == 0) return "Not bad, not bad. Hangin' in there?";
		else if (name().compare("stella") == 0) return "Fine, thank you. How are you?";
		else if (name().compare("al") == 0) return "Mmm.";
		else if (name().compare("antoni") == 0) return "I'm good.";
	}
	else if (m_friendshipLevel > LIKE && m_friendshipLevel < LOVE) {
		if (name().compare("robby") == 0) return "I'm good! Gettin' it done.";
		else if (name().compare("stella") == 0) return "I'm well, Doctor. And you?";
		else if (name().compare("al") == 0) return "Good, kid. You?";
		else if (name().compare("antoni") == 0) return "Good.";
	}
	else if (m_friendshipLevel > LOVE) {
		if (name().compare("robby") == 0) return "I'm great! Thanks! How are you?";
		else if (name().compare("stella") == 0) return "I'm really good. How are you?";
		else if (name().compare("al") == 0) return "Still alive.";
		else if (name().compare("antoni") == 0) return "I'm good. Why do adults always ask that?";
	}
}

//////////////////////////////////////////////////////////////
//                      Private methods                     //
//////////////////////////////////////////////////////////////

void Npc::doCurrentActivity() {
	if (m_currentActivity) {
		if (m_currentActivity->m_activityName.compare("water") == 0) {
			m_working = true;
			m_game->currentMap()->waterPlot(m_gridPosition);
		}
		else if (m_currentActivity->m_activityName.compare("weed") == 0) {
			m_working = true;
			m_game->currentMap()->removeWeedsFromPlot(m_gridPosition);
		}
		else if (m_currentActivity->m_activityName.compare("fertilize") == 0) {
			m_working = true;
		}
		else if (m_currentActivity->m_activityName.compare("spray") == 0) {
			m_working = true;
		}
		else {
			m_working = false;
		}
	}
}
