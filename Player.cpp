
// Copyright 2019 Happyrock Studios

// STD
#include <math.h>
#include <iostream>
#include <sstream>

// Moon
#include "Game.h"
#include "Player.h"
#include "Tool.h"
#include "Map.h"
#include "Crop.h"
#include "InventoryGui.h"
#include "ItemLocker.h"

Player::Player(std::string name, std::string currentMap, Direction direction, std::shared_ptr<Game> game)
    : Character(name, "player", "player1", currentMap, direction, game) {

    m_maxSpeed          = 1;
    m_speed             = 1;
	m_canMove			= true;
    m_up                = false;
    m_down              = false;
    m_left              = false;
    m_right             = false;
    m_currentFunds      = 5000;
	m_flashlightOn		= false;
	m_timeOut = 0.0;
	m_actionTimeout.reset(true);

    m_focusTile.sprite.setTexture(*m_game->texture("focus_tile"));
    m_focusTile.sprite.setOrigin(sf::Vector2f(16, 16));

    m_headSprite    = std::make_shared<sf::Sprite>();
    m_torsoSprite   = std::make_shared<sf::Sprite>();
    m_legsSprite    = std::make_shared<sf::Sprite>();
    m_shoesSprite   = std::make_shared<sf::Sprite>();

    m_headSprite->setPosition(m_position);
    m_torsoSprite->setPosition(m_position);
    m_legsSprite->setPosition(m_position);
    m_shoesSprite->setPosition(m_position);

    m_headSprite->setTextureRect(sf::IntRect(0, 0, CHARA_SPRITE_WIDTH, CHARA_SPRITE_HEIGHT));
    m_torsoSprite->setTextureRect(sf::IntRect(0, 0, CHARA_SPRITE_WIDTH, CHARA_SPRITE_HEIGHT));
    m_legsSprite->setTextureRect(sf::IntRect(0, 0, CHARA_SPRITE_WIDTH, CHARA_SPRITE_HEIGHT));
    m_shoesSprite->setTextureRect(sf::IntRect(0, 0, CHARA_SPRITE_WIDTH, CHARA_SPRITE_HEIGHT));

    m_headSprite->setOrigin(0,0);
    m_torsoSprite->setOrigin(0,0);
    m_legsSprite->setOrigin(0,0);
    m_shoesSprite->setOrigin(0,0);

    // The order in which we push to the vector determines draw order.
    m_sprites["middle"].push_back(m_sprite);
    m_sprites["middle"].push_back(m_torsoSprite);
    m_sprites["middle"].push_back(m_headSprite);
    m_sprites["middle"].push_back(m_legsSprite);
    m_sprites["middle"].push_back(m_shoesSprite);

    scaleSprites(1.5, 1.5);

    m_outfit["skin"] = 1;
    m_outfit["head"] = 2;
    m_outfit["torso"] = 1;
    m_outfit["pants"] = 1;
    m_outfit["shoes"] = 1;
    setOutfit(m_outfit);
}

void Player::update(double dt) {

    Character::update(dt);

	if (m_actionTimeout.getElapsedTime().asSeconds() < m_timeOut) return;

    if (m_left || m_right || m_up || m_down) {
        m_walking = true;
        walk();
    }
    else {
        m_walking = false;
    }

    // Check for map event contact.
    m_game->activateEventByGrid(m_gridPosition.x, m_gridPosition.y, "map");

    // Set focus tile location
    sf::Vector2i mousePos = m_game->mouseGridPosition();

    if (mousePos.x < m_gridPosition.x) m_focusTile.coordinates.x = m_gridPosition.x - 1;
    else if (mousePos.x == m_gridPosition.x) m_focusTile.coordinates.x = m_gridPosition.x;
    else m_focusTile.coordinates.x = m_gridPosition.x + 1;

    if (mousePos.y < m_gridPosition.y) m_focusTile.coordinates.y = m_gridPosition.y - 1;
    else if (mousePos.y == m_gridPosition.y) m_focusTile.coordinates.y = m_gridPosition.y;
    else m_focusTile.coordinates.y = m_gridPosition.y + 1;

    m_focusTile.sprite.setPosition(m_game->gridToPixels(m_focusTile.coordinates.x, m_focusTile.coordinates.y));

    if (m_currentTool) {
        m_currentTool->setPositionByPixel(m_position.x, m_position.y);
        m_currentTool->setDirection(m_direction);
		m_working = true;
    }
	else {
		m_working = false;
	}
}

void Player::draw(std::string layerName) {

    if(m_game->guiVisible() && (!m_currentTool || !m_currentTool->inUse()) && layerName.compare("background") == 0) m_game->renderWindow()->draw(m_focusTile.sprite);

    // draw currently held item
    if (m_direction == UP && m_currentTool) {
        m_currentTool->drawFromPlayer(layerName);
    }

    Character::draw(layerName);

    if (m_direction != UP && m_currentTool) {
        m_currentTool->drawFromPlayer(layerName);
    }
}

void Player::handleInput(sf::Event event) {
	if (m_actionTimeout.getElapsedTime().asSeconds() < m_timeOut) return;

    // Keyboard
    if (event.type == sf::Event::KeyPressed) {
		// All keyboard inputs should be paused during events.
		if (!m_canMove) return;
        switch(event.key.code) {
        case sf::Keyboard::W:
            m_up = true;
            break;
        case sf::Keyboard::S:
            m_down = true;
            break;
        case sf::Keyboard::A:
            m_left = true;
            break;
        case sf::Keyboard::D:
            m_right = true;
            break;
        case sf::Keyboard::T:
            break;
		case sf::Keyboard::C:
			m_flashlightOn = !m_flashlightOn;
			break;
        }
    }
    if (event.type == sf::Event::KeyReleased) {
        switch(event.key.code) {
        case sf::Keyboard::W:
            m_up = false;
            break;
        case sf::Keyboard::S:
            m_down = false;
            break;
        case sf::Keyboard::A:
            m_left = false;
            break;
        case sf::Keyboard::D:
            m_right = false;
            break;
        }
    }

    // Mouse
    if (event.type == sf::Event::MouseButtonPressed) {
        if (event.mouseButton.button == sf::Mouse::Left) {
			// Holding tool but not using.
			if (m_currentTool && !m_currentTool->inUse()) useHeldItem();
        }
    }
    if (event.type == sf::Event::MouseButtonReleased) {
        if (event.mouseButton.button == sf::Mouse::Left) {
            // Holding tool but using.
            if (m_currentTool && m_currentTool->inUse()) stopUsingHeldItem();
            // Not holding tool. Check for harvesting.
            else if (!m_currentTool) {
				// Harvest
				if (m_game->map(m_currentMap)->readyForHarvest(m_focusTile.coordinates.x, m_focusTile.coordinates.y)) {
					std::shared_ptr<Yield> crop = m_game->map(m_currentMap)->tileByGrid("background", m_focusTile.coordinates.x, m_focusTile.coordinates.y)->m_crop->harvest();
					std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->addToInventory(crop);
				}
				// Weed
				else if (m_game->map(m_currentMap)->removeWeedByGrid(m_focusTile.coordinates.x, m_focusTile.coordinates.y)) {
					pause(0.3);
				}
				else {
					std::shared_ptr<Entity> entity = m_game->checkForEntityByGrid(m_focusTile.coordinates.x, m_focusTile.coordinates.y, m_currentMap);
					if (entity && entity->name().compare("player") == 0) entity = NULL;
					// Activate item on ground
					if (entity) {
						entity->activate();
					}
					// Use held item
					else std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->useCurrentItem(m_focusTile.coordinates);
				}
            }
        }
        if (event.mouseButton.button == sf::Mouse::Right) {
            // TODO - should this go in a separate function?
            std::shared_ptr<Entity> entity = m_game->checkForEntityByGrid(m_focusTile.coordinates.x, m_focusTile.coordinates.y, m_currentMap);
			if (entity && entity->name().compare("player") == 0) entity = NULL;
            if (entity) {
                m_right = false;
                m_left = false;
                m_up = false;
                m_down = false;

                if (entity->type().compare("tool") == 0) {
                    if (m_currentTool && entity->name().compare(m_currentTool->name()) != 0) putDownTool();
                    pickUpTool(std::dynamic_pointer_cast<Tool>(entity));
                }
                else if (entity->type().compare("sprinkler") == 0) {
					std::dynamic_pointer_cast<InventoryGui>(m_game->gui("inventory"))->addToInventory(std::dynamic_pointer_cast<Item>(m_game->removeEntity(entity->name())));
                }
                else if (entity->type().compare("locker") == 0) {
                    std::dynamic_pointer_cast<ItemLocker>(entity)->toggle();
                }
                else if (entity->type().compare("npc") == 0) {
                    m_game->activateNpcEvent(m_focusTile.coordinates.x, m_focusTile.coordinates.y, m_currentMap);
                }
            }
            else if (m_game->activateEventByGrid(m_focusTile.coordinates.x, m_focusTile.coordinates.y, "click")) {
                m_right = false;
                m_left = false;
                m_up = false;
                m_down = false;
            }
			// Nothing at all clicked on.
			else {
				if (m_currentTool) putDownTool();
			}
        }
    }
}

void Player::pauseMovement() {
	m_canMove = false;
}

void Player::resumeMovement() {
	m_canMove = true;
}

void Player::useHeldItem() {
    if (!m_currentTool) return;
    m_tempSpeed = m_currentTool->maxSpeed();
    m_currentTool->use(m_focusTile.coordinates);
}

void Player::stopUsingHeldItem() {
	m_tempSpeed = -1;
    if (!m_currentTool) return;
    m_currentTool->stop();
	if (m_currentTool->name().compare("basketball") == 0) m_currentTool = NULL;
}

void Player::stopMovement() {
    m_up = false;
    m_down = false;
    m_left = false;
    m_right = false;
}

void Player::pickUpTool(std::shared_ptr<Tool> tool) {
    m_currentTool = tool;
    m_currentTool->hold();
}

std::shared_ptr<Tool> Player::putDownTool() {
    if (m_currentTool) {
        m_currentTool->stop();
        m_currentTool->letGo();
        m_currentTool->setPositionByGrid(m_focusTile.coordinates.x, m_focusTile.coordinates.y);
        std::shared_ptr<Tool> heldTool = m_currentTool;
        m_currentTool = NULL;
        return heldTool;
    }

    return NULL;
}

void Player::setOutfit(std::unordered_map<std::string, int> outfit) {
    // Skin tone
    if (outfit.count("skin") > 0) {
        std::stringstream texPath;
        texPath << "player" << outfit["skin"];
        m_sprite->setTexture(*m_game->texture(texPath.str()));
    }
    else {
        m_sprite->setTexture(*m_game->texture("player1"));
    }

    // Head
    if (outfit.count("head") > 0) {
        std::stringstream texPath;
        texPath << "head" << outfit["head"];
        m_headSprite->setTexture(*m_game->texture(texPath.str()));
    }
    else {
        m_headSprite->setTexture(*m_game->texture("head1"));
    }

    // Torso
    if (outfit.count("torso") > 0) {
        std::stringstream texPath;
        texPath << "torso" << outfit["torso"];
        m_torsoSprite->setTexture(*m_game->texture(texPath.str()));
    }
    else {
        m_torsoSprite->setTexture(*m_game->texture("torso1"));
    }

    // Pants
    if (outfit.count("legs") > 0) {
        std::stringstream texPath;
        texPath << "legs" << outfit["legs"];
        m_legsSprite->setTexture(*m_game->texture(texPath.str()));
    }
    else {
        m_legsSprite->setTexture(*m_game->texture("legs1"));
    }

    // Shoes
    if (outfit.count("shoes") > 0) {
        std::stringstream texPath;
        texPath << "shoes" << outfit["shoes"];
        m_shoesSprite->setTexture(*m_game->texture(texPath.str()));
    }
    else {
        m_shoesSprite->setTexture(*m_game->texture("shoes1"));
    }
}

void Player::pause(float timeOut) {
	m_actionTimeout.reset(true);
	m_timeOut = timeOut;
	m_walking = false;
	m_left = false;
	m_right = false;
	m_up = false;
	m_down = false;
}

//////////////////////////////////////////////////////////////////
//                      Getters & Setters                       //
//////////////////////////////////////////////////////////////////

const int Player::currentFunds() {
    return m_currentFunds;
}

void Player::setFunds(int funds) {
    m_currentFunds = funds;
}

void Player::addFunds(int fundsToAdd) {
    m_currentFunds += fundsToAdd;
}

const std::string Player::currentToolName() {
    if (m_currentTool) return m_currentTool->name();
    else return "No tool held";
}

std::shared_ptr<Tool> Player::currentTool() {
    return m_currentTool;
}

const bool Player::usingItem() {
    return  !(!m_currentTool || !m_currentTool->inUse());
}

const bool Player::flashlightOn() {
	return m_flashlightOn;
}

std::unordered_map<std::string, int> Player::outfit() {
    return m_outfit;
}



