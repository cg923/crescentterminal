
// Copyright 2019 Happyrock Studios

// Moon
#include "Gui.h"
#include "Game.h"

// STD
#include <iostream>
#include <sstream>

Clickable::Clickable(std::string name, sf::IntRect hitBox) {
    m_name = name;
    m_hitBox = hitBox;
}

const bool Clickable::wasClicked(sf::Vector2f clickPos) {
    return (clickPos.x >= m_hitBox.left
            && clickPos.x <= m_hitBox.left + m_hitBox.width
            && clickPos.y >= m_hitBox.top
            && clickPos.y <= m_hitBox.top + m_hitBox.height);
}

const bool Clickable::visible() {
    return m_visible;
}

void Clickable::setVisible(bool visible) {
    m_visible = visible;
}

const std::string Clickable::name() {
    return m_name;
}

void Clickable::setHitbox(sf::IntRect newRect) {
    m_hitBox = newRect;
}

/////////////////////////////////////////////////////////////
//                          GUI                            //
/////////////////////////////////////////////////////////////

Gui::Gui(sf::Vector2f position, std::shared_ptr<Game> game) {
    m_position = position;
    m_game = game;
    m_display = false;
}

void Gui::update(double dt) {

}

void Gui::draw() {
    m_game->renderWindow()->draw(m_sprite);
}

void Gui::display() {
    m_display = true;
}

void Gui::hide() {
    m_display = false;
}

const bool Gui::shouldDisplay() {
    return m_display;
}

sf::Text Gui::buildText(sf::Font& font, int characterSize, bool outline) {
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(characterSize);
    if (outline) {
        text.setFillColor(sf::Color::White);
        text.setOutlineColor(sf::Color::Black);
        text.setOutlineThickness(2.f);
    }
    else {
        text.setFillColor(sf::Color::Black);
    }

    return text;
}

std::string Gui::escapeString(std::string str, bool removeNewLines) {
	sf::String tmp;

	std::size_t i = 0;
	while(i < str.length()) {
		sf::Uint32 c = str[i];

		if(c == '\\') {
			i++;
			sf::Uint32 n = str[i];

			switch(n) {
				case '\\':
					tmp += n;
					break;
				case 'n':
					if (!removeNewLines) tmp += '\n';
					else tmp += ' ';
					break;
				case 't':
					tmp += '\t';
					break;
				default:
					std::cerr << "Error: invalid special char found : " << c << n << std::endl;
			}
		}
		else
			tmp += c;
		i++;
	}

	return tmp;
}
