
// Copyright 2019 Corey Selover

#include "MachiningGui.h"
#include "Game.h"

// STD
#include <iostream>
#include <sstream>

Machinable::Machinable(std::string name, bool unlocked, int cost, int hourCost, sf::IntRect texRect, std::vector<std::string> requirements)
	: Clickable(name, sf::IntRect()) {
	m_unlocked = unlocked;
	m_cost = cost;
	m_hourCost = hourCost;
	m_textureRect = texRect;
	m_requirements = requirements;
}

MachiningGui::MachiningGui(std::shared_ptr<Game> game)
	: Gui(sf::Vector2f(), game) {

	m_machinables["sprinkler"] = std::make_shared<Machinable>("sprinkler", false, 50000, 2, sf::IntRect(0, 64, 16, 16), std::vector<std::string>());

	m_confirmClickable = std::make_shared<Clickable>("confirm", sf::IntRect(0, 0, 100, 50));
	m_cancelClickable = std::make_shared<Clickable>("cancel", sf::IntRect(0, 0, 100, 50));

	defineSprite();

	m_costText = Gui::buildText(*m_game->font("dialogue"), 30, true);
	m_costText.setString("Select item to see cost");
	m_costText.setPosition(m_position + sf::Vector2f(0, 200) - sf::Vector2f(m_costText.getLocalBounds().width / 2, 0));

	m_itemToMachine = "No item selected";
	m_displayButtons = false;
}

void MachiningGui::defineSprite() {
	m_position = sf::Vector2f(m_game->renderWindow()->getSize().x / 2, 300);
	m_sprite.setTexture(*m_game->texture("machining"));
	m_sprite.setPosition(m_position);
	m_sprite.setOrigin(m_sprite.getTextureRect().width / 2, m_sprite.getTextureRect().height / 2);
	m_sprite.scale(2, 2);

	std::unordered_map<std::string, std::shared_ptr<Machinable>>::iterator itr;
	int itemNumber = 0;
	int rowNumber = 0;
	for (itr = m_machinables.begin(); itr != m_machinables.end(); itr++) {
		itr->second->m_sprite.setTexture(*m_game->texture("items"));
		itr->second->m_sprite.setPosition(m_position - m_sprite.getOrigin() * m_sprite.getScale().x + sf::Vector2f(40, 40) + sf::Vector2f(itemNumber*70, int(rowNumber / 10) * 40 ));
		itr->second->m_sprite.setTextureRect(itr->second->m_textureRect);
		itr->second->m_sprite.scale(3, 3);
		itr->second->setHitbox(sf::IntRect(itr->second->m_sprite.getPosition().x, itr->second->m_sprite.getPosition().y, itr->second->m_textureRect.width * itr->second->m_sprite.getScale().x, itr->second->m_textureRect.height * itr->second->m_sprite.getScale().y));
		itemNumber++;
		rowNumber++;
	}

	m_confirmSprite.setTexture(*m_game->texture("machining_buttons"));
	m_confirmSprite.setTextureRect(sf::IntRect(100, 50, 100, 50));
	m_confirmSprite.setPosition(m_position + sf::Vector2f(-150, 300));
	m_confirmClickable->setHitbox(sf::IntRect(m_confirmSprite.getPosition().x, m_confirmSprite.getPosition().y, 100, 50));

	m_cancelSprite.setTexture(*m_game->texture("machining_buttons"));
	m_cancelSprite.setTextureRect(sf::IntRect(100, 0, 100, 50));
	m_cancelSprite.setPosition(m_position + sf::Vector2f(50, 300));
	m_cancelClickable->setHitbox(sf::IntRect(m_cancelSprite.getPosition().x, m_cancelSprite.getPosition().y, 100, 50));
}

void MachiningGui::update(double dt) {
	Gui::update(dt);

	std::unordered_map<std::string, std::shared_ptr<Machinable>>::iterator itr;
	for (itr = m_machinables.begin(); itr != m_machinables.end(); itr++) {
		if (!itr->second->m_unlocked) itr->second->m_sprite.setColor(sf::Color(255, 255, 255, 85));
		else itr->second->m_sprite.setColor(sf::Color::White);
	}
}

void MachiningGui::draw() {
	Gui::draw();

	std::unordered_map<std::string, std::shared_ptr<Machinable>>::iterator itr;
	for (itr = m_machinables.begin(); itr != m_machinables.end(); itr++) {
		m_game->renderWindow()->draw(itr->second->m_sprite);
	}

	m_game->renderWindow()->draw(m_costText);

	if (m_displayButtons) {
		m_game->renderWindow()->draw(m_confirmSprite);
		m_game->renderWindow()->draw(m_cancelSprite);
	}
}

void MachiningGui::handleInput(sf::Event event) {
	if (event.type == sf::Event::MouseButtonReleased) {
	}
	switch (event.mouseButton.button) {
	case sf::Mouse::Left:
		handleClick(event);
		break;
	case sf::Mouse::Right:
		break;
	default:
		break;
	}
}

void MachiningGui::handleClick(sf::Event event) {
	std::unordered_map<std::string, std::shared_ptr<Machinable>>::iterator itr;
	sf::Vector2f mousePos = m_game->mousePixelPosition();
	for (itr = m_machinables.begin(); itr != m_machinables.end(); itr++) {
		if (itr->second->wasClicked(mousePos) && itr->second->m_unlocked) {
			machine(itr->second->name());
			return;
		}
	}

	if (m_displayButtons) {
		if (m_confirmClickable->wasClicked(mousePos)) {
			m_displayButtons = false;
			m_game->assignAl(m_machinables[m_itemToMachine]);
			m_itemToMachine = "No item selected";
			m_costText.setString("No item selected");
			m_costText.setPosition(m_position + sf::Vector2f(0, 200) - sf::Vector2f(m_costText.getLocalBounds().width / 2, 0));
			m_game->closeMachining();
		}
		else if (m_cancelClickable->wasClicked(mousePos)) {
			m_displayButtons = false;
			m_itemToMachine = "No item selected";
			m_costText.setString("No item selected");
			m_costText.setPosition(m_position + sf::Vector2f(0, 200) - sf::Vector2f(m_costText.getLocalBounds().width / 2, 0));
			m_game->closeMachining();
		}
	}

	sf::FloatRect compBounds = m_sprite.getGlobalBounds();
	if (mousePos.x < compBounds.left
		|| mousePos.x > compBounds.left + compBounds.width
		|| mousePos.y < compBounds.top
		|| mousePos.y > compBounds.top + compBounds.height) {
		m_game->closeMachining();
	}
}

void MachiningGui::machine(std::string itemName) {
	std::stringstream ss;
	ss << "Cost: " << m_machinables[itemName]->m_cost << " Required time: " << m_machinables[itemName]->m_hourCost << " hours";
	if (m_machinables[itemName]->m_requirements.size() > 0) {
		ss << "\nRequired items: ";
		std::vector<std::string>::iterator itr;
		for (itr = m_machinables[itemName]->m_requirements.begin(); itr != m_machinables[itemName]->m_requirements.end(); itr++) {
			ss << (*itr) << ", ";
		}
		ss.str(ss.str().substr(0, ss.str().length() - 2));
	}
	m_costText.setString(ss.str());
	m_costText.setPosition(m_position + sf::Vector2f(0, 200) - sf::Vector2f(m_costText.getLocalBounds().width / 2, 0));

	m_itemToMachine = itemName;
	m_displayButtons = true;
}

void MachiningGui::unlock(std::string itemName) {
	if (m_machinables.find(itemName) != m_machinables.end())
		m_machinables[itemName]->m_unlocked = true;
}
