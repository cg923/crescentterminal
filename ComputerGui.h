
// Copyright 2019 Happyrock Studios

#ifndef COMPUTER_GUI_H
#define COMPUTER_GUI_H

// Moon
#include "Gui.h"
class InventoryGui;
class Item;

// STD
#include <array>
#include <unordered_map>

//////////////////////////////////////////////////////////////
//                      Email                               //
//////////////////////////////////////////////////////////////

const int EMAIL_SPRITE_HEIGHT = 75;
const int EMAIL_SPRITE_WIDTH = 660;

class Email : public Clickable {
public:
    Email(int id, std::string from, std::string subject, int month, int day, int year, std::string content, std::shared_ptr<Game> game);

    virtual void setSpritePosition(sf::Vector2f position);

    virtual void draw();

    const int id();

    const std::string from();

    const std::string subject();

    const std::string fullDateAsString();

    const std::string content();

    const int day();

    const int month();

    const int year();

    void markAsRead();

    const bool read();

protected:
    int m_id;

    std::shared_ptr<Game> m_game;

    std::string m_from;
    std::string m_subject;
    std::string m_content;

    int m_day;
    int m_month;
    int m_year;

    bool m_read;

    sf::Sprite m_sprite;
    sf::Text m_fromText;
    sf::Text m_subjectText;
    sf::Text m_dateText;
    sf::Text m_contentPreviewText;
    sf::Text m_contentText;
};

//////////////////////////////////////////////////////////////////
//                          Contract                            //
//////////////////////////////////////////////////////////////////

const std::array<std::string, 2> VALID_VEGGIES = {"yield_asparagus", "yield_beets"};
const std::array<std::string, 2> VALID_FRUITS = {"yield_apple", "yield_orange"};

class Contract : public Email {
public:
    Contract(int id, std::string from, std::string goods, std::string details, int quantity, int reward, int day, int month, int year, std::shared_ptr<Game> game);

    const std::string goods();

    const std::string details();

    const int quantity();

    const int reward();

    void connectToHopper(std::string hopperName);

    void disconnectFromHopper();

    const std::string hopperName();

private:
    int m_quantity;

    int m_reward;

    sf::Text m_quantityText;

    std::string m_connectedHopperName;

};

//////////////////////////////////////////////////////////////////
//                  Computer Gui                                //
//////////////////////////////////////////////////////////////////

const int MAX_CONTRACTS = 4;

class ComputerGui : public Gui {
public:
    ComputerGui(std::shared_ptr<Game> game);

    void defineSprite();

    void defineButtons();

    void load();

    void update(double dt);

    void draw();

    void handleInput(sf::Event event);

    void handle(std::string buttonName);

    void advanceDay(std::string fullDate);

    /////////////////////////////////////////
    //              Emails                 //
    /////////////////////////////////////////

    void displayEmail(int emailId);

    void distributeEmails(std::string date);

    void inboxEmail(int emailId);

    void archiveInbox();

    void archiveEmail(int emailId);

    void sortInboxByDate();

    void sortArchivesByDate();

    std::vector<int> inboxedEmailIds();

    std::vector<int> archivedEmailIds();

    //////////////////////////////////////////
    //              Contracts               //
    //////////////////////////////////////////

    void displayContract(int contractId);

    void distributeContracts(std::string date);

    void offerContract(int contractId);

    bool acceptContract(int contractId);

    void completeContract(int contractId, bool reward);

    void rejectContract(int contractId);

    void sortOfferedByDate();

    void sortAcceptedByDate();

    void sortCompletedByDate();

    void sortRejectedByDate();

    void checkForCompletedContracts();

    std::vector<int> offeredContractIds();

    std::vector<int> acceptedContractIds();

    std::vector<int> completedContractIds();

    std::vector<int> rejectedContractIds();

    std::shared_ptr<Contract> contract(int contractId);

	///////////////////////////////////////////
	//				Purchasing				 //
	///////////////////////////////////////////

	void offerForSale(std::shared_ptr<Item> item);

	void offerForSale(std::vector<std::shared_ptr<Item>> items);

	void purchaseItems();

	void restockSellInventory();

	///////////////////////////////////////////
	//				Taskmaster				 //
	///////////////////////////////////////////

	std::unordered_map<std::string, bool> stellaActivities();

	std::unordered_map<int, bool> stellaPlots();

	void setStellaActivities(std::unordered_map<std::string, bool> activities);

	void setStellaPlots(std::unordered_map<int, bool> plots);

    ///////////////////////////////////////////
    //              Helper Functions         //
    ///////////////////////////////////////////

    // TODO - do this with template arguments?

    static bool sortEmailsByDate(std::shared_ptr<Email> emailA, std::shared_ptr<Email> emailB);

    static bool sortContractsByDate(std::shared_ptr<Contract> contractA, std::shared_ptr<Contract> contractB);


private:
    std::string checkButtonsForClick(sf::Vector2f mousePos);

    std::unordered_map<std::string, std::vector<std::shared_ptr<Clickable>>> m_buttons;

    sf::Sprite m_windowSprite;
    sf::Sprite m_emailTabsSprite;
    sf::Sprite m_emailExpandedSprite;
    sf::Sprite m_contractsTabsSprite;
    sf::Sprite m_acceptContractButtonSprite;
    sf::Sprite m_rejectContractButtonSprite;
	sf::Sprite m_taskmasterSprite;

    sf::Text m_windowTitleText;

    sf::Text m_emailTitleText;
    sf::Text m_emailFromText;
    sf::Text m_emailSubjectText;
    sf::Text m_emailDateText;
    sf::Text m_emailContentText;

    sf::CircleShape m_unreadEmailsIcon;
    sf::CircleShape m_unreadContractsIcon;
    sf::Text m_unreadEmailsText;
    sf::Text m_unreadContractsText;

	////////////////////////////////////////////////////////////////
	//						E-mail								  //
	////////////////////////////////////////////////////////////////
    std::unordered_map<int, std::shared_ptr<Email>> m_emails;
    std::unordered_map<int, std::shared_ptr<Email>> m_inbox;
    std::unordered_map<int, std::shared_ptr<Email>> m_archives;

    std::vector<std::shared_ptr<Email>> m_aboveVisibleInbox;
    std::vector<std::shared_ptr<Email>> m_visibleInbox;
    std::vector<std::shared_ptr<Email>> m_belowVisibleInbox;

    std::vector<std::shared_ptr<Email>> m_aboveVisibleArchives;
    std::vector<std::shared_ptr<Email>> m_visibleArchives;
    std::vector<std::shared_ptr<Email>> m_belowVisibleArchives;

	////////////////////////////////////////////////////////////////
	//						Contracts							  //
	////////////////////////////////////////////////////////////////

    std::unordered_map<int, std::shared_ptr<Contract>> m_contracts;
    std::unordered_map<int, std::shared_ptr<Contract>> m_offered;
    std::unordered_map<int, std::shared_ptr<Contract>> m_accepted;
    std::unordered_map<int, std::shared_ptr<Contract>> m_rejected;
    std::unordered_map<int, std::shared_ptr<Contract>> m_completed;

    std::vector<std::shared_ptr<Contract>> m_aboveVisibleOffered;
    std::vector<std::shared_ptr<Contract>> m_visibleOffered;
    std::vector<std::shared_ptr<Contract>> m_belowVisibleOffered;

    std::vector<std::shared_ptr<Contract>> m_aboveVisibleAccepted;
    std::vector<std::shared_ptr<Contract>> m_visibleAccepted;
    std::vector<std::shared_ptr<Contract>> m_belowVisibleAccepted;

    std::vector<std::shared_ptr<Contract>> m_aboveVisibleCompleted;
    std::vector<std::shared_ptr<Contract>> m_visibleCompleted;
    std::vector<std::shared_ptr<Contract>> m_belowVisibleCompleted;

    std::vector<std::shared_ptr<Contract>> m_aboveVisibleRejected;
    std::vector<std::shared_ptr<Contract>> m_visibleRejected;
    std::vector<std::shared_ptr<Contract>> m_belowVisibleRejected;

	////////////////////////////////////////////////////////////////
	//						Purchasing							  //
	////////////////////////////////////////////////////////////////

	std::shared_ptr<InventoryGui> m_sellInventory;
	std::shared_ptr<InventoryGui> m_buyInventory;

	sf::Text m_sellText;
	sf::Text m_buyText;
	sf::Text m_totalCostText;

	////////////////////////////////////////////////////////////////
	//						Taskmaster							  //
	////////////////////////////////////////////////////////////////

	bool m_displayPlotSelection;
	std::unordered_map<std::string, bool> m_stellaActivities;
	std::unordered_map<int, bool> m_plots;

	sf::Sprite m_stellaWaterSprite;
	sf::Sprite m_stellaWeedSprite;
	sf::Sprite m_stellaFertilizeSprite;
	sf::Sprite m_stellaSpraySprite;
	sf::Sprite m_plotSelectionSprite;
	std::unordered_map<int, sf::Sprite> m_plotSprites;

	const int PLOTS = 8;

	////////////////////////////////////////////////////////////////
	//					Everything Else							  //
	////////////////////////////////////////////////////////////////
	
    sf::Vector2f m_visibleEmailsPosition;

    bool m_displayEmail;
    bool m_displayExpandedEmail;
    bool m_displayInbox;
    bool m_displayArchives;
    bool m_displayPurchasing;
    bool m_displayContracts;
    bool m_displayExpandedContract;
    bool m_displayWindow;
    bool m_displayOffered;
    bool m_displayAccepted;
    bool m_displayCompleted;
    bool m_displayRejected;

    int m_clickedEmail;
    int m_clickedContract;
};

#endif // COMPUTER_GUI_H
