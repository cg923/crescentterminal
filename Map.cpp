
// Copyright 2019 Happyrock Studios

// STD
#include <iostream>
#include <sstream>
#include <fstream>

// Moon
#include "Map.h"
#include "Game.h"
#include "Tree.h"

Map::Map(std::string name, std::string filePath, std::shared_ptr<Game> game) {
    m_name = name;
    m_game = game;

    // Load from file.
    std::stringstream fullFilePath;
    fullFilePath << MAPS_PATH << filePath;
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(fullFilePath.str().c_str());
    std::cout << "Load result: " << fullFilePath.str() << " " << result.description() << std::endl;

    // Parse values.
    m_mapWidth      = std::stoi(doc.child("map").attribute("width").value());
    m_mapHeight     = std::stoi(doc.child("map").attribute("height").value());
    m_tileWidth     = std::stoi(doc.child("map").attribute("tilewidth").value());
    m_tileHeight    = std::stoi(doc.child("map").attribute("tileheight").value());
    m_tileSetWidth  = TILE_SET_FILE_WIDTH / m_tileWidth;
    m_tileSetHeight = TILE_SET_FILE_HEIGHT / m_tileHeight;

    // Parse and load tileset.
    std::stringstream tileSetPath;
    tileSetPath << MAPS_PATH << doc.child("map").child("tileset").attribute("source").value();
    tileSetPath.seekp(-3, std::ios_base::end);
    tileSetPath << "png";
    if (!m_tileSet.loadFromFile(tileSetPath.str())) {
        std::cout << "Failed to load tileset." << std::endl;
    }

    tileSetPath.str("");
    tileSetPath << IMAGES_PATH << "watered.png";
    if (!m_wateredTexture.loadFromFile(tileSetPath.str())) {
        std::cout << "Failed to load watered texture" << std::endl;
    }

	tileSetPath.str("");
	tileSetPath << IMAGES_PATH << "weed.png";
	if (!m_weedTexture.loadFromFile(tileSetPath.str())) {
		std::cout << "Failed to load weed texture" << std::endl;
	}

    // Populate tile layers
    for (pugi::xml_node layer = doc.child("map").child("layer"); layer; layer = layer.next_sibling("layer")) {
        std::string layerName = layer.attribute("name").as_string();
        m_layerVisibility[layerName] = true;

        // Instead of creating tiles, update pre-existing ones if we're on "farmable" layer.
        if (layerName.compare("farmable") == 0) {
            makeFarmable(layer);
            continue;
        }

		// Designate farming plots.
		if (layerName.substr(0, layerName.length() - 1).compare("plot") == 0) {
			designatePlot(layer, layerName);
			continue;
		}

        // Place objects if we're on the "objects" layer.
        if (layerName.compare("objects") == 0) {
            placeObjects(layer);
            continue;
        }

        Layer newLayer;

        // Create a vertex array.
        m_vertexArrays[layerName] = sf::VertexArray();
        m_vertexArrays[layerName].setPrimitiveType(sf::Quads);
        m_vertexArrays[layerName].resize(m_mapWidth * m_mapHeight * 4);

        // Create blank tiles.
        for(int x = 0; x < m_mapWidth + 1; x++) {
            newLayer.push_back(std::vector<std::shared_ptr<Tile>>());
            for(int y = 0; y < m_mapHeight + 1; y++) {
                std::shared_ptr<Tile> blankTile = std::make_shared<Tile>();
                blankTile->m_exists = false;
                newLayer[x].push_back(blankTile);
            }
        }

        // Load tile data.
        int tileCounter = 0;
        for (pugi::xml_node tile = layer.child("data").child("tile"); tile; tile = tile.next_sibling("tile")) {
            if (tile.attribute("gid")) {
                // Create tile
                std::shared_ptr<Tile> newTile = std::make_shared<Tile>();
                newTile->m_exists = true;
				newTile->m_farmable = false;
				newTile->m_tilled = false;
				newTile->m_watered = false;
				newTile->m_weeds = false;
				if (tile.attribute("gid").as_int() == 226) newTile->m_isWater = true;
				else newTile->m_isWater = false;
				newTile->m_wateredSprite.setTexture(m_wateredTexture);
				newTile->m_weedSprite.setTexture(m_weedTexture);

                // Calculate physical position
                int tileX = (tileCounter % m_mapWidth) * m_tileWidth;
                int tileY = int(tileCounter / m_mapWidth) * m_tileHeight;
                newTile->m_pixelPosition = sf::Vector2f(tileX, tileY);
                newTile->m_wateredSprite.setPosition(newTile->m_pixelPosition.x, newTile->m_pixelPosition.y);
                newTile->m_wateredSprite.setOrigin(sf::Vector2f(0,0));
				newTile->m_weedSprite.setPosition(newTile->m_pixelPosition.x, newTile->m_pixelPosition.y);
				newTile->m_weedSprite.setTextureRect(sf::IntRect(m_tileWidth * (rand() % 3), 0, m_tileWidth, m_tileHeight));
				newTile->m_weedSprite.setOrigin(sf::Vector2f(0, 0));

                // Calculate which tile from the tileset this should have.
                int tileSetX = ((std::atoi(tile.attribute("gid").value()) % m_tileSetWidth) - 1) * m_tileWidth;
                int tileSetY = int(std::atoi(tile.attribute("gid").value()) / m_tileSetWidth) * m_tileHeight;

                // Calculate grid position.
                int gridX = int(tileX / m_tileWidth);
                int gridY = int(tileY / m_tileHeight);
                newTile->m_gridPosition = sf::Vector2i(gridX, gridY);
                newTile->m_crop = std::make_shared<Crop>(CROP_NONE, sf::Vector2i(gridX, gridY), m_game, m_tileWidth, m_tileHeight);
                newLayer[gridX][gridY] = newTile;

                sf::Vertex* quad = &m_vertexArrays[layerName][(gridX + gridY * m_mapWidth) * 4];
                quad[0].position = sf::Vector2f(gridX * m_tileWidth, gridY * m_tileHeight);
                quad[1].position = sf::Vector2f((gridX + 1) * m_tileWidth, gridY * m_tileHeight);
                quad[2].position = sf::Vector2f((gridX + 1) * m_tileWidth, (gridY + 1) * m_tileHeight);
                quad[3].position = sf::Vector2f(gridX * m_tileWidth, (gridY + 1) * m_tileHeight);

                // define its 4 texture coordinates
                quad[0].texCoords = sf::Vector2f(tileSetX, tileSetY);
                quad[1].texCoords = sf::Vector2f(tileSetX + m_tileWidth, tileSetY);
                quad[2].texCoords = sf::Vector2f(tileSetX +  m_tileWidth, tileSetY + m_tileHeight);
                quad[3].texCoords = sf::Vector2f(tileSetX , tileSetY + m_tileHeight);
            }
            tileCounter++;
        }
        m_layers[layer.attribute("name").as_string()] = newLayer;
    }
}

void Map::makeFarmable(pugi::xml_node layer) {
    // Load tile data.
    int tileCounter = 0;
    for (pugi::xml_node tile = layer.child("data").child("tile"); tile; tile = tile.next_sibling("tile")) {
        if (tile.attribute("gid")) {

            // Calculate grid position.
            int gridX = tileCounter % m_mapWidth;
            int gridY = int(tileCounter / m_mapWidth);
            m_layers["background"][gridX][gridY]->m_farmable = true;
        }
        tileCounter++;
    }
}

void Map::designatePlot(pugi::xml_node layer, std::string layerName) {
	// Load tile data.
	int tileCounter = 0;
	int validTileCounter = 0;
	int plotNumber = stoi(layerName.substr(layerName.length() - 1));

	for (pugi::xml_node tile = layer.child("data").child("tile"); tile; tile = tile.next_sibling("tile")) {
		if (tile.attribute("gid")) {

			// Calculate grid position.
			int gridX = tileCounter % m_mapWidth;
			int gridY = int(tileCounter / m_mapWidth);
			m_farmPlots[plotNumber].push_back(sf::Vector2i(gridX, gridY));
			validTileCounter++;
		}
		tileCounter++;
	}

	if (validTileCounter > 0) m_farmPlotCenters[plotNumber] = m_farmPlots[plotNumber][int(validTileCounter / 2)];
}

void Map::placeObjects(pugi::xml_node layer) {
    // Load tile data.
    int tileCounter = 0;
    for (pugi::xml_node tile = layer.child("data").child("tile"); tile; tile = tile.next_sibling("tile")) {
        if (tile.attribute("gid")) {

            // Calculate grid position.
            int gridX = tileCounter % m_mapWidth;
            int gridY = int(tileCounter / m_mapWidth);

            // Tree
            if(tile.attribute("gid").as_int() == 109) {
                std::stringstream ss;
                ss << "tree" << tileCounter;
                m_game->addEntity(ss.str(), std::make_shared<Tree>(m_name, m_game, gridX, gridY));
            }
        }
        tileCounter++;
    }
}

void Map::update(double dt) {
	static int waterFrame = 0;
	static int frameCount = 1;
    if (m_layers.count("background") == 0 || m_layers["background"].size() == 0) return;
    else {
		frameCount++;
        for (int x = 0; x < m_mapWidth - 1; x++) {
            for (int y = 0; y < m_mapHeight - 1; y++) {
                updateTilledSprite(x, y);
                updateWateredSprite(x, y);
				if (frameCount % 50 == 0 && tileByGrid("background", x, y)->m_isWater) {
					sf::Vertex* quad = &m_vertexArrays["background"][(x + y * m_mapWidth) * 4];
					quad[0].texCoords = sf::Vector2f(5*m_tileWidth + waterFrame*m_tileWidth, 22*m_tileHeight);
					quad[1].texCoords = sf::Vector2f(6 * m_tileWidth + waterFrame * m_tileWidth, 22 * m_tileHeight);
					quad[2].texCoords = sf::Vector2f(6 * m_tileWidth + waterFrame * m_tileWidth, 23 * m_tileHeight);
					quad[3].texCoords = sf::Vector2f(5 * m_tileWidth + waterFrame * m_tileWidth, 23 * m_tileHeight);
					waterFrame++;
					if (waterFrame >= 3) waterFrame = 0;
				}
            }
        }
    }
}

void Map::drawLayer(std::string layerName) {
    if (m_layers.count(layerName) > 0 && m_layerVisibility[layerName] == true) {
        m_game->renderWindow()->draw(m_vertexArrays[layerName], &m_tileSet);
        for(int x = 0; x < m_mapWidth; x++) {
            for(int y = 0; y < m_mapHeight; y++) {
                std::shared_ptr<Tile> tile = m_layers[layerName][x][y];
                if (tile->m_exists) {
                    if (tile->m_watered) m_game->renderWindow()->draw(tile->m_wateredSprite);
					if (tile->m_weeds) m_game->renderWindow()->draw(tile->m_weedSprite);
                    if (tile->m_crop) tile->m_crop->draw();
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////
//                      Helper Functions                              //
////////////////////////////////////////////////////////////////////////

bool Map::checkForObstacleByPixel(float x, float y) {
    if (tileByPixels("obstacles", int(x), int(y))) return tileByPixels("obstacles", int(x), int(y))->m_exists;
    return false;
}

bool Map::checkForObstacleByGrid(int x, int y) {
    if (tileByGrid("obstacles", x, y)) return tileByGrid("obstacles", x, y)->m_exists;
	return false;
}

void Map::makeTileAnObstacle(int x, int y) {
    if (tileByGrid("obstacles", x, y)) tileByGrid("obstacles", x, y)->m_exists = true;
}

void Map::makeTileNotAnObstacle(int x, int y) {
	if (tileByGrid("obstacles", x, y)) tileByGrid("obstacles", x, y)->m_exists = false;
}

sf::Vector2i Map::pixelsToGrid(float x, float y) {
    return sf::Vector2i(int(x / m_tileWidth), int(y / m_tileHeight));
}

sf::Vector2f Map::gridToPixels(int x, int y) {
    return sf::Vector2f(x * m_tileWidth, y * m_tileHeight) + sf::Vector2f(m_tileWidth / 2, m_tileHeight / 2);
}

std::vector<sf::Vector2i> Map::validNeighborsOf(sf::Vector2i tile) {
    std::vector<sf::Vector2i> neighbors;
    if (tile.x > 0           && !tileByGrid("obstacles", tile.x - 1, tile.y)->m_exists) neighbors.push_back(sf::Vector2i(tile.x - 1, tile.y));
    if (tile.x < m_mapWidth  && !tileByGrid("obstacles", tile.x + 1, tile.y)->m_exists) neighbors.push_back(sf::Vector2i(tile.x + 1, tile.y));
    if (tile.y > 0           && !tileByGrid("obstacles", tile.x, tile.y - 1)->m_exists) neighbors.push_back(sf::Vector2i(tile.x, tile.y - 1));
    if (tile.y < m_mapHeight && !tileByGrid("obstacles", tile.x, tile.y + 1)->m_exists) neighbors.push_back(sf::Vector2i(tile.x, tile.y + 1));

    return neighbors;
}

void Map::tillByGrid(int x, int y) {
    std::shared_ptr<Tile> tile = m_layers["background"][x][y];
    if (tile->m_exists && tile->m_farmable) {
        tile->m_tilled = true;
		tile->m_weeds = false;
    }
}

void Map::updateTilledSprite(int x, int y) {
    std::shared_ptr<Tile> tile = m_layers["background"][x][y];
    if(!tile->m_tilled) return;

    bool leftTilled = m_layers["background"][x-1][y]->m_tilled;
    bool rightTilled = m_layers["background"][x+1][y]->m_tilled;

    if (leftTilled && rightTilled) {
        tile->m_textureRect = sf::IntRect(0, m_tileHeight * 4, m_tileWidth, m_tileHeight);
    }
    else if (leftTilled) {
        tile->m_textureRect = sf::IntRect(0, m_tileHeight * 6, m_tileWidth, m_tileHeight);
    }
    else if (rightTilled) {
        tile->m_textureRect = sf::IntRect(0, m_tileHeight * 5, m_tileWidth, m_tileHeight);
    }
    else tile->m_textureRect = sf::IntRect(0, m_tileHeight * 7, m_tileWidth, m_tileHeight);

    sf::Vertex* quad = &m_vertexArrays["background"][(x + y * m_mapWidth) * 4];
    quad[0].texCoords = sf::Vector2f(tile->m_textureRect.left, tile->m_textureRect.top);
    quad[1].texCoords = sf::Vector2f(tile->m_textureRect.left + m_tileWidth, tile->m_textureRect.top);
    quad[2].texCoords = sf::Vector2f(tile->m_textureRect.left +  m_tileWidth, tile->m_textureRect.top + m_tileHeight);
    quad[3].texCoords = sf::Vector2f(tile->m_textureRect.left , tile->m_textureRect.top + m_tileHeight);
}

void Map::updateWateredSprite(int x, int y) {
    std::shared_ptr<Tile> tile = m_layers["background"][x][y];
    if(!tile->m_watered) return;

    bool leftWatered = m_layers["background"][x-1][y]->m_watered;
    bool rightWatered = m_layers["background"][x+1][y]->m_watered;

    if (leftWatered && rightWatered) {
        tile->m_wateredSprite.setTextureRect(sf::IntRect(0, 0, m_tileWidth, m_tileHeight));
    }
    else if (leftWatered) {
        tile->m_wateredSprite.setTextureRect(sf::IntRect(0, m_tileHeight * 2, m_tileWidth, m_tileHeight));
    }
    else if (rightWatered) {
        tile->m_wateredSprite.setTextureRect(sf::IntRect(0, m_tileHeight, m_tileWidth, m_tileHeight));
    }
    else tile->m_wateredSprite.setTextureRect(sf::IntRect(0, m_tileHeight * 3, m_tileWidth, m_tileHeight));

}

bool Map::plantByGrid(int x, int y, Crops cropType) {
    std::shared_ptr<Tile> tile = m_layers["background"][x][y];
    if (tile->m_tilled && tile->m_crop->type() == CROP_NONE) {
        tile->m_crop->plant(cropType);
        return true;
    }

    return false;
}

void Map::waterByGrid(int x, int y) {
    std::shared_ptr<Tile> tile = m_layers["background"][x][y];
    if (tile->m_exists && tile->m_tilled) {
        tile->m_watered = true;
    }
}

void Map::waterPlot(sf::Vector2i position) {
	for (int i = 0; i < m_farmPlotCenters.size(); i++) {
		if (position == m_farmPlotCenters[i]) {
			std::vector<sf::Vector2i>::iterator itr;
			for (itr = m_farmPlots[i].begin(); itr != m_farmPlots[i].end(); itr++) {
				waterByGrid((*itr).x, (*itr).y);
			}
		}
	}
}

void Map::waterPlot(int plotNumber) {
	std::vector<sf::Vector2i>::iterator itr;
	for (itr = m_farmPlots[plotNumber].begin(); itr != m_farmPlots[plotNumber].end(); itr++) {
		waterByGrid((*itr).x, (*itr).y);
	}
}

void Map::addWeedByGrid(int x, int y) {
	std::shared_ptr<Tile> tile = m_layers["background"][x][y];
	if (tile->m_exists) {
		tile->m_weeds = true;
	}
}

bool Map::removeWeedByGrid(int x, int y) {
	std::shared_ptr<Tile> tile = m_layers["background"][x][y];
	bool wasAWeed = false;
	if (tile->m_exists) {
		if (tile->m_weeds) wasAWeed = true;
		tile->m_weeds = false;
	}

	return wasAWeed;
}

void Map::removeWeedsFromPlot(sf::Vector2i position) {
	for (int i = 0; i < m_farmPlotCenters.size(); i++) {
		if (position == m_farmPlotCenters[i]) {
			std::vector<sf::Vector2i>::iterator itr;
			for (itr = m_farmPlots[i].begin(); itr != m_farmPlots[i].end(); itr++) {
				removeWeedByGrid((*itr).x, (*itr).y);
			}
		}
	}
}

void Map::removeWeedsFromPlot(int plotNumber) {
	std::vector<sf::Vector2i>::iterator itr;
	for (itr = m_farmPlots[plotNumber].begin(); itr != m_farmPlots[plotNumber].end(); itr++) {
		removeWeedByGrid((*itr).x, (*itr).y);
	}
}

bool Map::readyForHarvest(int x, int y) {
    if (!m_layers["background"][x][y]->m_crop) return false;
    return m_layers["background"][x][y]->m_crop->readyForHarvest();
}

void Map::advanceDay() {
    if (m_layers.count("background") == 0 || m_layers["background"].size() == 0) return;
    
	for (int x = 0; x < m_mapWidth - 1; x++) {
        for (int y = 0; y < m_mapHeight - 1; y++) {
            std::shared_ptr<Tile> tile = tileByGrid("background", x, y);
			// Water
			if (tile->m_watered && !tile->m_weeds && tile->m_crop) tile->m_crop->ageUp(1);
			else if (tile->m_crop && tile->m_crop->type() != CROP_NONE && !tile->m_watered) tile->m_crop->wilt();
            tile->m_watered = false;
			// Weeds
			int addWeed = rand() % 20 + 1;
			if (addWeed == 1 && tile->m_farmable) tile->m_weeds = true;
        }
    }
}

void Map::setAgeOfCropAtTile(int x, int y, int age) {
    if (m_layers.count("background") == 0 || m_layers["background"].size() == 0) return;
    std::shared_ptr<Tile> tile = tileByGrid("background", x, y);
    tile->m_crop->setAge(age);
}

void Map::hideLayer(std::string layerName) {
    if (m_layerVisibility.count(layerName) > 0) {
        m_layerVisibility[layerName] = false;
    }
}

void Map::showLayer(std::string layerName) {
    if (m_layerVisibility.count(layerName) > 0) {
        m_layerVisibility[layerName] = true;
    }
}

////////////////////////////////////////////////////////////////////////
//                      Getters and Setters                           //
////////////////////////////////////////////////////////////////////////

const std::string Map::name() {
    return m_name;
}

sf::IntRect Map::dimensions() {
    return sf::IntRect(0, 0, m_mapWidth * m_tileWidth, m_mapHeight * m_tileHeight);
}

sf::Vector2i Map::mapSize() {
    return sf::Vector2i(m_mapWidth, m_mapHeight);
}

sf::Vector2i Map::tileDimensions() {
    return sf::Vector2i(m_tileWidth, m_tileHeight);
}

std::shared_ptr<Tile> Map::tileByGrid(std::string layerName, int x, int y) {
    return m_layers[layerName][x][y];
	if (x < m_layers[layerName].size() && y < m_layers[layerName][x].size())
		return m_layers[layerName][x][y];
	else return NULL;
}

std::shared_ptr<Tile> Map::tileByPixels(std::string layerName, int x, int y) {
	if (int(x / m_tileWidth) < m_layers[layerName].size() && int(y / m_tileHeight) < m_layers[layerName][int(x / m_tileWidth)].size())
		return m_layers[layerName][int(x / m_tileWidth)][int(y / m_tileHeight)];
	else return NULL;
}

std::vector<std::shared_ptr<Crop>> Map::allPlantedCrops() {
    std::vector<std::shared_ptr<Crop>> crops;
    for (int x = 0; x < m_mapWidth - 1; x++) {
        for (int y = 0; y < m_mapHeight - 1; y++) {
            std::shared_ptr<Tile> tile = tileByGrid("background", x, y);
            if (tile->m_crop && tile->m_crop->type() != CROP_NONE) crops.push_back(tile->m_crop);
        }
    }

    return crops;
}

std::vector<std::pair<int, int>> Map::allWeeds() {
	std::vector<std::pair<int, int>> weeds;
	for (int x = 0; x < m_mapWidth - 1; x++) {
		for (int y = 0; y < m_mapHeight - 1; y++) {
			std::shared_ptr<Tile> tile = tileByGrid("background", x, y);
			if (tile->m_weeds) weeds.push_back(std::pair<int, int>(x, y));
		}
	}

	return weeds;
}

std::vector<std::shared_ptr<Tile>> Map::adjacentFarmableTiles(sf::Vector2i position) {
	std::vector<std::shared_ptr<Tile>> tiles;
	if (!tileByGrid("background", position.x, position.y)->m_farmable) return tiles;
	else tiles.push_back(tileByGrid("background", position.x, position.y));



	return tiles;
}

sf::Vector2i Map::farmPlotCenter(int plot) {
	return m_farmPlotCenters[plot];
}

int Map::numberOfPlots() {
	return m_farmPlots.size();
}
