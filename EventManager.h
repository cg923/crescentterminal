
// Copyright 2019 Happyrock Studios

#ifndef MANAGER_H
#define MANAGER_H

// Moon
class Event;
class Game;
#include "Event.h";

// Other libs
#include "libraries/Chronometer.hpp"

// STD
#include <unordered_map>
#include <map>
#include <memory>
#include <string>

class EventManager {
public:
    EventManager(std::shared_ptr<Game> game);

	void update();

    void generateEventsFromFile(std::string filePath);

    bool activateMapEvent(int x, int y, std::string mapName);

    bool activateNpcEvent(int x, int y, std::string mapName);

    bool activateClickEvent(int x, int y, std::string mapName);

	bool activateTimeEvent(std::string fullDate);

	void execute(Command command);

	void executeImmediately(std::vector<Command> commands);

	void setWaitTime(float timeToWait);

    void print();

private:
    // Map name, coordinates, Event
    std::map<std::string, std::map<std::pair<int, int>, std::shared_ptr<Event> > > m_mapEvents;

    // Map name, coordinates, Event
    std::map<std::string, std::map<std::pair<int, int>, std::shared_ptr<Event> > > m_clickEvents;

    // NPC name, Event name, Event
    std::map<std::string, std::map<std::string, std::shared_ptr<Event> > > m_dialogueEvents;

    // Date and time, Event
    std::unordered_map<std::string, std::shared_ptr<Event> > m_timeEvents;

    std::shared_ptr<Game> m_game;

	float m_timeToWait;
	sftools::Chronometer m_waitClock;
	std::vector<Command> m_commandQueue;
};
#endif // MANAGER_H
