
// Copyright 2019 Corey Selover

// Moon
#include "ResearchManager.h"
#include "MachiningGui.h"

// STD
#include <iostream>

ResearchManager::ResearchManager(std::shared_ptr<Game> game) {
	m_game = game;
	m_currentResearchId = "none";

	std::vector<std::string> requirements;
	m_researchItems["basic_fertilizer"] = { "basic fertilizer", "I think we could start with an all-purpose, unspecialized fertilizer for use with any crop.", 5, 0, false, requirements };

	requirements.push_back("basic_fertilizer");
	m_researchItems["advanced_fertilizer"] = { "advanced fertilizer", "I could probably make a higher-grade, general purpose fertilizer.", 20, 0, false, requirements};
	requirements.push_back("advanced_fertilizer");
	m_researchItems["asparagus_fertilizer"] = { "asparagus fertilizer", "Let's develop a high quality fertilizer specifically for asparagus.", 5, 0, false, requirements };
	m_researchItems["beet_fertilizer"] = { "beets fertilizer", "Let's develop a  high quality fertilizer specifically for beets.", 5, 0, false, requirements };
	m_researchItems["bokchoy_fertilizer"] = { "bok choy fertilizer", "Let's develop a  high quality fertilizer specifically for bok choy.", 5, 0, false, requirements };
	m_researchItems["carrot_fertilizer"] = { "carrot fertilizer", "Let's develop a  high quality fertilizer specifically for carrots.", 5, 0, false, requirements };
	m_researchItems["garlic_fertilizer"] = { "garlic fertilizer", "Let's develop a  high quality fertilizer specifically for garlic.", 5, 0, false, requirements };
	m_researchItems["leek_fertilizer"] = { "leek fertilizer", "Let's develop a  high quality fertilizer specifically for leeks.", 5, 0, false, requirements };
	m_researchItems["onion_fertilizer"] = { "onion fertilizer", "Let's develop a  high quality fertilizer specifically for onions.", 5, 0, false, requirements };
	m_researchItems["potato_fertilizer"] = { "potato fertilizer", "Let's develop a  high quality fertilizer specifically for potatoes.", 5, 0, false, requirements };

	requirements.clear();
	m_researchItems["basic_pesticide"] = { "basic pesticide", "Hmm. Until we know more about what kind of pests are around, we can probably make an all-purpose pesticide.", 5, 0, false, requirements };
	requirements.push_back("basic_pesticide");
	m_researchItems["beetle_pesticide"] = { "beetle pesticide", "We could make a pesticide geared toward those beetle-like things.", 20, 0, false, requirements };
	m_researchItems["worm_pesticide"] = { "worm pesticide", "Let's make something to fight those worms.", 20, 0, false, requirements };
	m_researchItems["moth_pesticide"] = { "moth pesticide", "How about a pesticide to stop those moths?", 20, 0, false, requirements };
	m_researchItems["slug_pesticide"] = { "slug pesticide", "Let's wipe those slugs off planet... whatever this place is called.", 20, 0, false, requirements };

	requirements.clear();
	m_researchItems["sprinkler"] = { "sprinkler", "We could try to develop a small, simple sprinkler.", 5, 0, false, requirements };
	requirements.push_back("sprinkler");
	m_researchItems["irrigation_tank"] = { "irrigation tank", "I think we could make a tank that we could connect the sprinklers to, so you wouldn't have to turn them each on and off individually.", 10, 0, false, requirements };
	requirements.push_back("irrigation_tank");
	m_researchItems["auto_drip"] = { "auto drip system", "Let's automate this stuff.  Full robo-sprinklers.", 10, 0, false, requirements };
}

void ResearchManager::advanceDay() {
	if (m_researchItems.find(m_currentResearchId) != m_researchItems.end()) {
		m_researchItems[m_currentResearchId].m_progress++;
		if (m_researchItems[m_currentResearchId].m_progress >= m_researchItems[m_currentResearchId].m_duration) {
			m_researchItems[m_currentResearchId].m_finished = true;
			std::dynamic_pointer_cast<MachiningGui>(m_game->gui("machining"))->unlock(m_currentResearchId);
			m_currentResearchId = "none";
		}
	}
}

void ResearchManager::unlockResearchItems(std::vector<std::pair<std::string, int>> researchItems) {
	std::vector<std::pair<std::string, int>>::iterator itr;
	for (itr = researchItems.begin(); itr != researchItems.end(); itr++) {
		if (m_researchItems.find((*itr).first) == m_researchItems.end()) continue;
		m_researchItems[(*itr).first].m_progress = (*itr).second;
		if (m_researchItems[(*itr).first].m_progress >= m_researchItems[(*itr).first].m_duration) {
			m_researchItems[(*itr).first].m_finished = true;
			std::dynamic_pointer_cast<MachiningGui>(m_game->gui("machining"))->unlock((*itr).first);
		}		
	}
}

std::unordered_map<std::string, ResearchItem> ResearchManager::researchItems() {
	return m_researchItems;
}

void ResearchManager::research(std::string researchId) {
	m_currentResearchId = researchId;
}

void ResearchManager::stopResearch() {
	m_currentResearchId = "none";
}

const bool ResearchManager::researchFinished(std::string researchId) {
	if (m_researchItems.find(researchId) != m_researchItems.end()) {
		return m_researchItems[researchId].m_finished;
	}
	return false;
}

const std::string ResearchManager::currentResearchName() {
	if (m_researchItems.find(m_currentResearchId) != m_researchItems.end()) return m_researchItems[m_currentResearchId].m_name;
	else return "none";
}

const std::string ResearchManager::currentResearchId() {
	return m_currentResearchId;
}

const std::string ResearchManager::nextResearchDescription(std::string researchBranch) {
	if (researchBranch.compare("fertilizer") == 0) {
		if (!m_researchItems["basic_fertilizer"].m_finished) {
			return m_researchItems["basic_fertilizer"].m_description;
		}
		if (!m_researchItems["advanced_fertilizer"].m_finished) {
			return m_researchItems["advanced_fertilizer"].m_description;
		}
		return "I could develop a new crop-specific fertilizer. Which one sounds good?";
	}
	else if (researchBranch.compare("pesticide") == 0) {
		if (!m_researchItems["basic_pesticide"].m_finished) {
			return m_researchItems["basic_pesticide"].m_description;
		}
		return "I could develop a pest-specific pesticide. What do you need?";
	}
	else if (researchBranch.compare("irrigation") == 0) {
		if (!m_researchItems["sprinkler"].m_finished) {
			return m_researchItems["sprinkler"].m_description;
		}
		if (!m_researchItems["irrigation_tank"].m_finished) {
			return m_researchItems["irrigation_tank"].m_description;
		}
		return m_researchItems["auto_drip"].m_description;
	}
}

const std::string ResearchManager::nextResearchId(std::string researchBranch) {
	// TODO - specific fertilizers and pesticides
	if (researchBranch.compare("fertilizer") == 0) {
		if (!m_researchItems["basic_fertilizer"].m_finished) {
			return "basic_fertilizer";
		}
		if (!m_researchItems["advanced_fertilizer"].m_finished) {
			return "advanced_fertilizer";
		}
		return "TODO";
	}
	else if (researchBranch.compare("pesticide") == 0) {
		if (!m_researchItems["basic_pesticide"].m_finished) {
			return "basic_pesticide";
		}
		return "TODO";
	}
	else if (researchBranch.compare("irrigation") == 0) {
		if (!m_researchItems["sprinkler"].m_finished) {
			return "sprinkler";
		}
		if (!m_researchItems["irrigation_tank"].m_finished) {
			return "irrigation_tank";
		}
		return "auto_drip.";
	}
}

const std::string ResearchManager::timeLeft() {
	if (m_researchItems.find(m_currentResearchId) != m_researchItems.end()) {
		float done = float(m_researchItems[m_currentResearchId].m_progress) / float(m_researchItems[m_currentResearchId].m_duration);
		if (done <= 0.25) return "I've still got quite a bit to do on that.";
		else if (done > 0.25 && done <= 0.5) return "I've made some progress, but not quite done.";
		else if (done > 0.5 && done <= 0.75) return "I'm a little more than halfway done.";
		else if (done > 0.75) return "I should be finished in the next few days.";
	}
	else return "ERROR: NO CURRENT RESEARCH ITEM";
}