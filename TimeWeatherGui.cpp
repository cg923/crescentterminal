
// Copyright 2019 Happyrock Studios

#include "TimeWeatherGui.h"
#include "Game.h"
#include "Player.h"

// STD
#include <sstream>

TimeWeatherGui::TimeWeatherGui(std::shared_ptr<Game> game)
    : Gui(sf::Vector2f(), game) {

    defineSprite();
}

void TimeWeatherGui::defineSprite() {
    m_position = sf::Vector2f(20, 20);
    m_sprite.setTexture(*m_game->texture("timeweather"));
    m_sprite.setPosition(m_position);
    m_sprite.scale(3, 3);
    m_sprite.setOrigin(0, 0);

    m_weatherText = Gui::buildText(*m_game->font("dialogue"), 20, true);
    m_dateText = Gui::buildText(*m_game->font("dialogue"), 20, true);
    m_timeText = Gui::buildText(*m_game->font("dialogue"), 20, true);
    m_fundsText = Gui::buildText(*m_game->font("dialogue"), 20, true);

    m_weatherText.setPosition(m_position + sf::Vector2f(48, 16));
    m_dateText.setPosition(m_position + sf::Vector2f(48, 60));
    m_timeText.setPosition(m_position + sf::Vector2f(48, 106));
    m_fundsText.setPosition(m_position + sf::Vector2f(48, 150));
}

void TimeWeatherGui::update(double dt) {
    Gui::update(dt);

    m_weatherText.setString("Sunny");

    std::string dayOfWeek = m_game->dayOfWeek();
    std::stringstream ss;
    ss << dayOfWeek.substr(0, 3) << ", " << m_game->date();
    m_dateText.setString(ss.str());

    m_timeText.setString(m_game->time());

    m_fundsText.setString(std::to_string(std::dynamic_pointer_cast<Player>(m_game->entity("player"))->currentFunds()));
}

void TimeWeatherGui::draw() {
    Gui::draw();

    m_game->renderWindow()->draw(m_weatherText);
    m_game->renderWindow()->draw(m_dateText);
    m_game->renderWindow()->draw(m_timeText);
    m_game->renderWindow()->draw(m_fundsText);
}
