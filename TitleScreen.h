
// Copyright 2019 Corey Selover

#ifndef TITLE_SCREEN_H
#define TITLE_SCREEN_H

// STD
#include <memory>

// SFML
#include <SFML/Graphics.hpp>

// Other libs
#include "libraries/Chronometer.hpp"

class Game;
class Clickable;

class TitleScreen {
public:
	TitleScreen(std::shared_ptr<Game> game);

	int run();

	void handleClick(sf::Vector2f clickPos);

	void close();

private:
	std::shared_ptr<Game> m_game;

	sf::Sprite m_backgroundSprite;
	sf::Sprite m_logoSprite;
	sf::Sprite m_glowSprite;

	int m_saveSlot;

	sftools::Chronometer m_logoClock;
	sftools::Chronometer m_glowClock;

	std::shared_ptr<Clickable>	m_newGameButton;
	sf::Text					m_newGameButtonText;
	std::shared_ptr<Clickable>	m_loadGameButton;
	sf::Text					m_loadGameButtonText;
	std::shared_ptr<Clickable>	m_settingsButton;
	sf::Text					m_settingsButtonText;
	std::shared_ptr<Clickable>	m_exitButton;
	sf::Text					m_exitButtonText;
};

#endif