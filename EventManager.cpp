
// Copyright 2019 Happyrock Studios

// Moon
#include "EventManager.h"
#include "Entity.h"
#include "Player.h"
#include "Event.h"
#include "Game.h"
#include "Npc.h"
#include "Door.h"
#include "Map.h"
#include "DayNight.h"

// Other libs
#include "libraries/pugixml.hpp"

// STD
#include <iostream>
#include <string>

EventManager::EventManager(std::shared_ptr<Game> game) {
    m_game = game;
	m_timeToWait = -1;
}

void EventManager::update() {
	if (m_commandQueue.size() > 0) {
		if (m_waitClock.getElapsedTime().asSeconds() < m_timeToWait) return;
		if (m_game->displayingDialogue()) return;

		Command command = m_commandQueue.front();
		execute(command);

		m_commandQueue.erase(m_commandQueue.begin());
	}
}

void EventManager::generateEventsFromFile(std::string filePath) {
    // Load from file.
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(filePath.c_str());
    std::cout << "Load result: " << filePath << " " << result.description() << std::endl;

    // Store all Events
    for (pugi::xml_node event = doc.child("event"); event; event = event.next_sibling("event")) {
        std::string eventName = event.attribute("name").value();
        std::string eventType = event.attribute("type").value();
        bool repeat = event.attribute("repeat");
        std::vector<std::string> eventCommands;

        // Parse Commands
        for (pugi::xml_node command = event.child("command"); command; command = command.next_sibling("command")) {
            eventCommands.push_back(command.attribute("content").value());
        }

        std::shared_ptr<Event> newEvent = std::make_shared<Event>(eventName, eventType, eventCommands, repeat, true);

        // Map events
        if (eventType.compare("map") == 0) {
            m_mapEvents[event.attribute("map").value()][std::pair<int,int>(event.attribute("x").as_int(), event.attribute("y").as_int())] = newEvent;
        }

        // Click events
        if (eventType.compare("click") == 0) {
            m_clickEvents[event.attribute("map").value()][std::pair<int,int>(event.attribute("x").as_int(), event.attribute("y").as_int())] = newEvent;
        }

        // Dialogue events
        if (eventType.compare("dialogue") == 0) {
            m_dialogueEvents[event.attribute("npc").value()][eventName] = newEvent;
        }

        // Time events
        if (eventType.compare("time") == 0) {
			newEvent->setImmediate(false);
            m_timeEvents[event.attribute("time").value()] = newEvent;
        }
    }
}

void EventManager::execute(Command command) {
	// System
	if (command.who.compare("system") == 0) {
		if (command.what.compare("pause_input") == 0) {
			std::dynamic_pointer_cast<Player>(m_game->entity("player"))->pauseMovement();
			m_game->pauseWorld();
		}
		if (command.what.compare("resume_input") == 0) {
			std::dynamic_pointer_cast<Player>(m_game->entity("player"))->resumeMovement();
			m_game->resumeWorld();
		}
		if (command.what.compare("wait") == 0) {
			setWaitTime(stof(command.where));
		}
		if (command.what.compare("hide_gui") == 0) {
			m_game->hideGui();
		}
		if (command.what.compare("show_gui") == 0) {
			m_game->showGui();
		}
		if (command.what.compare("start_computer") == 0) {
			m_game->startComputer();
		}
		if (command.what.compare("hide_layer") == 0) {
			m_game->currentMap()->hideLayer(command.where);
		}
		if (command.what.compare("show_layer") == 0) {
			m_game->currentMap()->showLayer(command.where);
		}
		if (command.what.compare("fade_in_entity") == 0) {
			m_game->entity(command.where)->fadeIn();
		}
		if (command.what.compare("fade_out_entity") == 0) {
			m_game->entity(command.where)->fadeOut();
		}
		if (command.what.compare("set_darkness") == 0) {
			m_game->dayNight()->setDarknessAlpha(std::stoi(command.where));
		}
		if (command.what.compare("fade_out") == 0) {
			m_game->dayNight()->beginFadeOut();
		}
		if (command.what.compare("fade_in") == 0) {
			m_game->dayNight()->beginFadeIn();
		}
		if (command.what.compare("sleep") == 0) {
			m_game->setCurrentMap("upstairs");
			m_game->entity("player")->setPositionByGrid(2, 3);
			m_game->entity("player")->setDirection("down");
			m_game->advanceDay(true);
		}
	}
	// Player & NPCS
	else if (command.who.compare("player") == 0
		|| command.who.compare("stella") == 0
		|| command.who.compare("robby") == 0
		|| command.who.compare("al") == 0
		|| command.who.compare("antoni") == 0) {
		if (command.what.compare("set_target") == 0) {
			int x = std::stoi(command.where.substr(0, command.where.find(",")));
			int y = std::stoi(command.where.substr(command.where.find(",") + 1));
			std::dynamic_pointer_cast<Character>(m_game->entity(command.who))->setWalkTarget(x, y);
		}
		if (command.what.compare("dialogue") == 0) {
			m_game->showDialogue(command.who, command.where);
		}
		if (command.what.compare("choices") == 0) {
			m_game->showPrewrittenDialogue(command.where);
		}
		if (command.what.compare("warp") == 0) {
			if (command.who.compare("player") == 0) m_game->setCurrentMap(command.where);
			else m_game->entity(command.who)->setCurrentMap(command.where);
		}
		if (command.what.compare("set_position") == 0) {
			int x = std::stoi(command.where.substr(0, command.where.find(",")));
			int y = std::stoi(command.where.substr(command.where.find(",") + 1));
			m_game->entity(command.who)->setPositionByGrid(x, y);
		}
		if (command.what.compare("prompt_sleep") == 0) {
			m_game->promptSleep();
		}
		if (command.what.compare("cancel_sleep_prompt") == 0) {
			m_game->cancelSleepPrompt();
		}
		if (command.what.compare("invisible") == 0) {
			m_game->entity(command.who)->setVisibility(false);
		}
		if (command.what.compare("visible") == 0) {
			m_game->entity(command.who)->setVisibility(true);
		}
		if (command.what.compare("direction") == 0) {
			m_game->entity(command.who)->setDirection(command.where);
		}
	}
	// Doors
	else if (command.who.compare("door") == 0) {
		if (command.what.compare("open") == 0) {
			std::dynamic_pointer_cast<Door>(m_game->entity(command.where))->open();
		}
		if (command.what.compare("close") == 0) {
			std::dynamic_pointer_cast<Door>(m_game->entity(command.where))->close();
		}
		if (command.what.compare("lock") == 0) {
			std::dynamic_pointer_cast<Door>(m_game->entity(command.where))->lock();
		}
		if (command.what.compare("unlock") == 0) {
			std::dynamic_pointer_cast<Door>(m_game->entity(command.where))->unlock();
		}
	}
	// Item events
	else if (command.who.find("item_") != std::string::npos) {

	}
}

void EventManager::executeImmediately(std::vector<Command> commands) {
	std::vector<Command>::iterator itr;
	for (itr = commands.begin(); itr != commands.end(); itr++) {
		execute(*itr);
	}
}

bool EventManager::activateMapEvent(int x, int y, std::string mapName) {
	if (m_mapEvents.find(mapName) == m_mapEvents.end()) return false;
	if (m_mapEvents[mapName].find(std::pair<int,int>(x, y)) == m_mapEvents[mapName].end()) return false;

    std::shared_ptr<Event> event = m_mapEvents[mapName][std::pair<int,int>(x,y)];
    if (event) {
		if (!event->immediate()) m_commandQueue = event->execute();
		else executeImmediately(event->execute());
        event->reset();
		return true;
    }

    return false;
}

bool EventManager::activateClickEvent(int x, int y, std::string mapName) {
	if (m_clickEvents.find(mapName) == m_clickEvents.end()) return false;
	if (m_clickEvents[mapName].find(std::pair<int, int>(x, y)) == m_clickEvents[mapName].end()) return false;

    std::shared_ptr<Event> event = m_clickEvents[mapName][std::pair<int,int>(x,y)];
    if (event) {
		if (!event->immediate()) m_commandQueue = event->execute();
		else executeImmediately(event->execute());
		event->reset();
        return true;
    }

    return false;
}

bool EventManager::activateNpcEvent(int x, int y, std::string mapName) {
    std::shared_ptr<Npc> npc  = std::dynamic_pointer_cast<Npc>(m_game->checkForEntityByGrid(x, y, mapName));
    if(!npc || npc->type().compare("npc") != 0) {
        return false;
    }
    else {
		if (m_game->timeOfDayAsString().compare("morning") == 0 && npc->spokeMorning()) {
			m_game->showNpcDialogueOptions(npc->name());
			return true;
		}
		if (m_game->timeOfDayAsString().compare("afternoon") == 0 && npc->spokeAfternoon()) {
			m_game->showNpcDialogueOptions(npc->name());
			return true;
		}
		if (m_game->timeOfDayAsString().compare("evening") == 0 && npc->spokeEvening()) {
			m_game->showNpcDialogueOptions(npc->name());
			return true;
		}

        std::string dialogue = npc->whatDialogue();
        if (m_dialogueEvents[npc->name()].count(dialogue) == 0) return false;

        npc->facePlayer();

        std::vector<Command> commands = m_dialogueEvents[npc->name()][dialogue]->execute();
        std::vector<Command>::iterator itr;
        for (itr = commands.begin(); itr != commands.end(); itr++) {
            m_game->showDialogue((*itr).who, (*itr).what);
        }

        return true;
    }
}

bool EventManager::activateTimeEvent(std::string fullDate) {
	if (m_timeEvents.find(fullDate) != m_timeEvents.end()) {
		m_commandQueue = m_timeEvents[fullDate]->execute();
		return true;
	}
	else return false;
}

void EventManager::setWaitTime(float waitTime) {
	m_timeToWait = waitTime;
	m_waitClock.reset();
	m_waitClock.resume();
}

void EventManager::print() {
    std::cout << "Printing map events" << std::endl;
    std::map<std::string, std::map<std::pair<int, int>, std::shared_ptr<Event> > >::iterator itr;
    std::map<std::pair<int,int>, std::shared_ptr<Event>>::iterator itr2;
    for(itr = m_mapEvents.begin(); itr != m_mapEvents.end(); itr++) {
        std::cout << "map: " << itr->first << std::endl;
        for(itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
            std::cout << itr2->first.first << ", " << itr2->first.second << ": " << itr2->second->name() << std::endl;
        }
    }

    std::cout << "Printing dialogue events" << std::endl;
    std::map<std::string, std::map<std::string, std::shared_ptr<Event> > >::iterator itr3;
    std::map<std::string, std::shared_ptr<Event> >::iterator itr4;
    for(itr3 = m_dialogueEvents.begin(); itr3 != m_dialogueEvents.end(); itr3++) {
        std::cout << "npc: " << itr3->first << std::endl;
        for(itr4 = itr3->second.begin(); itr4 != itr3->second.end(); itr4++) {
            std::cout << itr4->first << ": " << itr4->second->name() << std::endl;
        }
    }

    std::cout << "Printing time events" << std::endl;
    std::unordered_map<std::string, std::shared_ptr<Event> >::iterator itr5;
    for (itr5 = m_timeEvents.begin(); itr5 != m_timeEvents.end(); itr5++) {
        std::cout << "time: " << itr5->first << " event name: " << itr5->second->name() << std::endl;
    }
}
