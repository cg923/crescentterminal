
// Copyright 2019 Happyrock Studios

// Moon
#include "ComputerGui.h"
#include "Game.h"
#include "ItemLocker.h"
#include "Player.h"
#include "InventoryGui.h"
#include "Crop.h"
#include "Item.h"
#include "Npc.h"

// STD
#include <iostream>
#include <sstream>

// Other libs
#include "libraries/pugixml.hpp"

///////////////////////////////////////////////////////////////////////////
//                              E-mail                                   //
///////////////////////////////////////////////////////////////////////////

Email::Email(int id, std::string from, std::string subject, int day, int month, int year, std::string content, std::shared_ptr<Game> game)
    : Clickable("email", sf::IntRect(0, 0, EMAIL_SPRITE_WIDTH, EMAIL_SPRITE_HEIGHT)) {
    m_id = id;
    m_game = game;
    m_from = from;
    m_subject = subject;
    m_content = Gui::escapeString(content);
    m_day = day;
    m_month = month;
    m_year = year;
    m_read = false;

    m_sprite.setTexture(*m_game->texture("email"));
    m_sprite.setPosition(sf::Vector2f());
    m_sprite.setTextureRect(sf::IntRect(0, 0, EMAIL_SPRITE_WIDTH, EMAIL_SPRITE_HEIGHT));
    m_sprite.setOrigin(0,0);

    m_fromText = Gui::buildText(*m_game->font("arial"), 20, false);
    m_subjectText = Gui::buildText(*m_game->font("arial"), 20, false);
    m_contentPreviewText = Gui::buildText(*m_game->font("arial"), 20, false);
    m_contentText = Gui::buildText(*m_game->font("arial"), 20, false);
    m_dateText = Gui::buildText(*m_game->font("arial"), 20, false);

    m_fromText.setString(m_from);
    m_subjectText.setString(m_subject);
    // TODO - account for new lines
    std::stringstream ss;
    ss << Gui::escapeString(content, true).substr(0, 40);
    if (m_content.length() > 40) ss << "...";
    m_contentPreviewText.setString(ss.str());
    m_contentText.setString(m_content);
    m_dateText.setString(fullDateAsString());
}

void Email::setSpritePosition(sf::Vector2f position) {
    setHitbox(sf::IntRect(position.x, position.y, EMAIL_SPRITE_WIDTH, EMAIL_SPRITE_HEIGHT));
    m_sprite.setPosition(position);
    m_fromText.setPosition(position + sf::Vector2f(30, 15));
    m_subjectText.setPosition(position + sf::Vector2f(250, 15));
    m_contentPreviewText.setPosition(position + sf::Vector2f(250, 40));
    m_dateText.setPosition(position + sf::Vector2f(540, 15));
}

void Email::draw() {
    m_game->renderWindow()->draw(m_sprite);
    m_game->renderWindow()->draw(m_fromText);
    m_game->renderWindow()->draw(m_subjectText);
    m_game->renderWindow()->draw(m_contentPreviewText);
    m_game->renderWindow()->draw(m_dateText);
}

const int Email::id() {
    return m_id;
}

const std::string Email::from() {
    return m_from;
}

const std::string Email::subject() {
    return m_subject;
}

const std::string Email::fullDateAsString() {
    std::stringstream ss;
    ss << m_month << "/" << m_day << "/" << m_year;
    return ss.str();
}

const std::string Email::content() {
    return m_content;
}

const int Email::day() {
    return m_day;
}

const int Email::month() {
    return m_month;
}

const int Email::year() {
    return m_year;
}

void Email::markAsRead() {
    m_read = true;
}

const bool Email::read() {
    return m_read;
}

//////////////////////////////////////////////////////////////////
//                      Contract                                //
//////////////////////////////////////////////////////////////////

Contract::Contract(int id, std::string from, std::string goods, std::string details, int quantity, int reward, int day, int month, int year, std::shared_ptr<Game> game)
    : Email(id, from, goods, day, month, year, details, game) {
    m_quantity = quantity;
    m_reward = reward;
}

const std::string Contract::goods() {
    return m_subject;
}

const std::string Contract::details() {
    return m_content;
}

const int Contract::quantity() {
    return m_quantity;
}

const int Contract::reward() {
    return m_reward;
}

void Contract::connectToHopper(std::string hopperName) {
    m_connectedHopperName = hopperName;
    std::stringstream ss;
    ss << from() << "\n" << goods() << " x" << quantity();
    std::dynamic_pointer_cast<ItemLocker>(m_game->entity(hopperName))->setDisplayBelowText(ss.str());
}

void Contract::disconnectFromHopper() {
    std::dynamic_pointer_cast<ItemLocker>(m_game->entity(m_connectedHopperName))->setDisplayBelowText("No associated contract");
    m_connectedHopperName = "";
}

const std::string Contract::hopperName() {
    return m_connectedHopperName;
}

//////////////////////////////////////////////////////
//                  ComputerGui                     //
//////////////////////////////////////////////////////

ComputerGui::ComputerGui(std::shared_ptr<Game> game)
    : Gui(sf::Vector2f(), game) {

    defineSprite();
    defineButtons();
    load();

    m_displayEmail = false;
    m_displayExpandedEmail = false;
    m_displayExpandedContract = false;
    m_displayInbox = true;
    m_displayArchives = false;
    m_displayPurchasing = false;
    m_displayContracts = false;
    m_displayWindow = false;
    m_displayOffered = true;
    m_displayAccepted = false;
    m_displayCompleted = false;
    m_displayRejected = false;
	m_stellaActivities["water"] = true;
	m_stellaActivities["weed"] = false;
	m_stellaActivities["fertilize"] = false;
	m_stellaActivities["spray"] = false;
	m_displayPlotSelection = false;

    m_visibleEmailsPosition = m_position + sf::Vector2f(360, 120) - m_sprite.getOrigin();

    m_clickedEmail = -1;
    m_clickedContract = -1;

	m_sellInventory = std::make_shared<InventoryGui>(m_game, m_visibleEmailsPosition + sf::Vector2f(250, 40), "purchasing", 39, 3, sf::IntRect(0, 0, 235, 54), sf::IntRect(0, 0, 235, 54));
	m_buyInventory = std::make_shared<InventoryGui>(m_game, m_visibleEmailsPosition + sf::Vector2f(250, 240), "purchasing", 39, 3, sf::IntRect(0, 0, 235, 54), sf::IntRect(0, 0, 235, 54));
	m_sellInventory->expand();
	m_buyInventory->expand();
	restockSellInventory();

	for (int i = 0; i < PLOTS; i++) {
		m_plots[i] = false;
	}

	m_plots[0] = true;
}

void ComputerGui::defineSprite() {
    m_position = sf::Vector2f(m_game->renderWindow()->getSize().x / 2, m_game->renderWindow()->getSize().y / 2);
    m_sprite.setTexture(*m_game->texture("computer"));
    m_sprite.setPosition(m_position);
    m_sprite.setOrigin(m_sprite.getTextureRect().width / 2, m_sprite.getTextureRect().height / 2);

    m_windowSprite.setTexture(*m_game->texture("computer_window"));
    m_windowSprite.setPosition(m_position);
    m_windowSprite.setOrigin(m_sprite.getTextureRect().width / 2, m_sprite.getTextureRect().height / 2);

    m_windowTitleText = buildText(*m_game->font("arial"), 20);
    m_windowTitleText.setString("FOR CENTERING");
    m_windowTitleText.setPosition(m_windowSprite.getGlobalBounds().left + m_windowSprite.getGlobalBounds().width / 2 - m_windowTitleText.getLocalBounds().width / 2,
                                  m_windowSprite.getGlobalBounds().top + 75);

    // Email
    m_emailTabsSprite.setTexture(*m_game->texture("email_tabs"));
    m_emailTabsSprite.setPosition(m_position - m_sprite.getOrigin() + sf::Vector2f(180, 105));
    m_emailTabsSprite.setTextureRect(sf::IntRect(0, 0, 163, 471));
    m_emailTabsSprite.setOrigin(0,0);

    m_emailExpandedSprite.setTexture(*m_game->texture("email_expanded"));
    m_emailExpandedSprite.setPosition(m_position);
    m_emailExpandedSprite.setOrigin(m_sprite.getTextureRect().width / 2, m_sprite.getTextureRect().height / 2);

    m_emailFromText = buildText(*m_game->font("arial"), 20, false);
    m_emailSubjectText = buildText(*m_game->font("arial"), 20, false);
    m_emailDateText = buildText(*m_game->font("arial"), 20, false);
    m_emailContentText = buildText(*m_game->font("arial"), 20, false);

    m_emailFromText.setPosition(m_position - m_sprite.getOrigin() + sf::Vector2f(300, 150));
    m_emailSubjectText.setPosition(m_position - m_sprite.getOrigin() + sf::Vector2f(300, 185));
    m_emailDateText.setPosition(m_position - m_sprite.getOrigin() + sf::Vector2f(750, 150));
    m_emailContentText.setPosition(m_position - m_sprite.getOrigin() + sf::Vector2f(300, 220));

    sf::FloatRect compBounds = m_sprite.getGlobalBounds();
    m_unreadEmailsIcon.setFillColor(sf::Color::Red);
    m_unreadEmailsIcon.setRadius(10.0f);
    m_unreadEmailsIcon.setPosition(compBounds.left + 53, compBounds.top + 61);
    m_unreadEmailsText = buildText(*m_game->font("arial"), 15, true);
    m_unreadEmailsText.setPosition(m_unreadEmailsIcon.getPosition() + sf::Vector2f(4, 0));

    // Contract
    m_contractsTabsSprite.setTexture(*m_game->texture("contract_tabs"));
    m_contractsTabsSprite.setPosition(m_position - m_sprite.getOrigin() + sf::Vector2f(180, 105));
    m_contractsTabsSprite.setTextureRect(sf::IntRect(0, 0, 163, 471));
    m_contractsTabsSprite.setOrigin(0,0);

    m_acceptContractButtonSprite.setTexture(*m_game->texture("contract_buttons"));
    m_acceptContractButtonSprite.setPosition(m_emailExpandedSprite.getPosition() - m_emailExpandedSprite.getOrigin() + sf::Vector2f(300, 500));
    m_acceptContractButtonSprite.setTextureRect(sf::IntRect(0, 50, 100, 50));
    m_acceptContractButtonSprite.setOrigin(0,0);

    m_rejectContractButtonSprite.setTexture(*m_game->texture("contract_buttons"));
    m_rejectContractButtonSprite.setPosition(m_emailExpandedSprite.getPosition() - m_emailExpandedSprite.getOrigin() + sf::Vector2f(450, 500));
    m_rejectContractButtonSprite.setTextureRect(sf::IntRect(0, 0, 100, 50));
    m_rejectContractButtonSprite.setOrigin(0,0);

    m_unreadContractsIcon.setFillColor(sf::Color::Red);
    m_unreadContractsIcon.setRadius(10.0f);
    m_unreadContractsIcon.setPosition(compBounds.left + 53, compBounds.top + 282);
    m_unreadContractsText = buildText(*m_game->font("arial"), 15, true);
    m_unreadContractsText.setPosition(m_unreadContractsIcon.getPosition() + sf::Vector2f(4, 0));

	// Purchasing
	m_sellText = buildText(*m_game->font("arial"), 20, false);
	m_buyText = buildText(*m_game->font("arial"), 20, false);
	m_totalCostText = buildText(*m_game->font("arial"), 20, false);

	m_sellText.setString("For Sale");
	m_buyText.setString("To Buy");
	m_totalCostText.setString("All Your Money");

	// Taskmaster
	sf::Vector2f windowPos = m_windowSprite.getPosition() - m_windowSprite.getOrigin();
	m_stellaWaterSprite.setTexture(*m_game->texture("switch"));
	m_stellaWaterSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	m_stellaWaterSprite.setOrigin(0, 0);
	m_stellaWaterSprite.setPosition(windowPos + sf::Vector2f(280, 182));

	m_stellaWeedSprite.setTexture(*m_game->texture("switch"));
	m_stellaWeedSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	m_stellaWeedSprite.setOrigin(0, 0);
	m_stellaWeedSprite.setPosition(windowPos + sf::Vector2f(435, 182));

	m_stellaFertilizeSprite.setTexture(*m_game->texture("switch"));
	m_stellaFertilizeSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	m_stellaFertilizeSprite.setOrigin(0, 0);
	m_stellaFertilizeSprite.setPosition(windowPos + sf::Vector2f(280, 235));

	m_stellaSpraySprite.setTexture(*m_game->texture("switch"));
	m_stellaSpraySprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	m_stellaSpraySprite.setOrigin(0, 0);
	m_stellaSpraySprite.setPosition(windowPos + sf::Vector2f(435, 235));

	m_plotSelectionSprite.setTexture(*m_game->texture("plot_selection"));
	m_plotSelectionSprite.setOrigin(0, 0);
	m_plotSelectionSprite.setPosition(windowPos);

	for (int i = 0; i < PLOTS; i++) {
		m_plotSprites[i] = sf::Sprite();
		m_plotSprites[i].setTexture(*m_game->texture("switch"));
		m_plotSprites[i].setTextureRect(sf::IntRect(0, 0, 32, 32));
		m_plotSprites[i].setOrigin(0, 0);
	}

	m_plotSprites[0].setPosition(windowPos + sf::Vector2f(350, 370));
	m_plotSprites[1].setPosition(windowPos + sf::Vector2f(660, 360));
	m_plotSprites[2].setPosition(windowPos + sf::Vector2f(600, 470));
	m_plotSprites[3].setPosition(windowPos + sf::Vector2f(715, 470));
	m_plotSprites[4].setPosition(windowPos + sf::Vector2f(840, 160));
	m_plotSprites[5].setPosition(windowPos + sf::Vector2f(840, 310));
	m_plotSprites[6].setPosition(windowPos + sf::Vector2f(840, 430));
	m_plotSprites[7].setPosition(windowPos + sf::Vector2f(970, 260));
}

void ComputerGui::defineButtons() {
    sf::FloatRect compBounds = m_sprite.getGlobalBounds();
    m_buttons["desktop"].push_back(std::make_shared<Clickable>("free-email", sf::IntRect(compBounds.left + 53, compBounds.top + 61, 90, 90)));
    m_buttons["desktop"].push_back(std::make_shared<Clickable>("purchasing", sf::IntRect(compBounds.left + 53, compBounds.top + 162, 90, 115)));
    m_buttons["desktop"].push_back(std::make_shared<Clickable>("contracts", sf::IntRect(compBounds.left + 53, compBounds.top + 282, 90, 90)));
    m_buttons["desktop"].push_back(std::make_shared<Clickable>("taskmaster", sf::IntRect(compBounds.left + 53, compBounds.top + 384, 90, 90)));
    m_buttons["desktop"].push_back(std::make_shared<Clickable>("log off", sf::IntRect(compBounds.left + 1038, compBounds.top + 601, 137, 70)));

    m_buttons["email"].push_back(std::make_shared<Clickable>("close window", sf::IntRect(compBounds.left + 1018, compBounds.top + 73, 28, 28)));
    m_buttons["email"].push_back(std::make_shared<Clickable>("close email", sf::IntRect(compBounds.left + 1090, compBounds.top + 90, 30, 30)));
    m_buttons["purchasing"].push_back(std::make_shared<Clickable>("close window", sf::IntRect(compBounds.left + 1018, compBounds.top + 73, 28, 28)));
    m_buttons["contracts"].push_back(std::make_shared<Clickable>("close window", sf::IntRect(compBounds.left + 1018, compBounds.top + 73, 28, 28)));
    m_buttons["contracts"].push_back(std::make_shared<Clickable>("close contract", sf::IntRect(compBounds.left + 1090, compBounds.top + 90, 30, 30)));
    m_buttons["plot selection"].push_back(std::make_shared<Clickable>("close window", sf::IntRect(compBounds.left + 1018, compBounds.top + 73, 28, 28)));

    m_buttons["email"].push_back(std::make_shared<Clickable>("inbox", sf::IntRect(compBounds.left + 180, compBounds.top + 106, 162, 64)));
    m_buttons["email"].push_back(std::make_shared<Clickable>("archives", sf::IntRect(compBounds.left + 180, compBounds.top + 171, 162, 64)));

    m_buttons["contracts"].push_back(std::make_shared<Clickable>("offered", sf::IntRect(compBounds.left + 180, compBounds.top + 106, 162, 64)));
    m_buttons["contracts"].push_back(std::make_shared<Clickable>("accepted", sf::IntRect(compBounds.left + 180, compBounds.top + 171, 162, 64)));
    m_buttons["contracts"].push_back(std::make_shared<Clickable>("completed", sf::IntRect(compBounds.left + 180, compBounds.top + 236, 162, 64)));
    m_buttons["contracts"].push_back(std::make_shared<Clickable>("rejected", sf::IntRect(compBounds.left + 180, compBounds.top + 301, 162, 64)));
    m_buttons["contracts"].push_back(std::make_shared<Clickable>("accept contract", sf::IntRect(compBounds.left + 300, compBounds.top + 500, 100, 50)));
    m_buttons["contracts"].push_back(std::make_shared<Clickable>("reject contract", sf::IntRect(compBounds.left + 450, compBounds.top + 500, 100, 50)));

	m_buttons["plot selection"].push_back(std::make_shared<Clickable>("water", sf::IntRect(m_stellaWaterSprite.getPosition().x, m_stellaWaterSprite.getPosition().y, 32, 32)));
	m_buttons["plot selection"].push_back(std::make_shared<Clickable>("weed", sf::IntRect(m_stellaWeedSprite.getPosition().x, m_stellaWeedSprite.getPosition().y, 32, 32)));
	m_buttons["plot selection"].push_back(std::make_shared<Clickable>("fertilize", sf::IntRect(m_stellaFertilizeSprite.getPosition().x, m_stellaFertilizeSprite.getPosition().y, 32, 32)));
	m_buttons["plot selection"].push_back(std::make_shared<Clickable>("spray", sf::IntRect(m_stellaSpraySprite.getPosition().x, m_stellaSpraySprite.getPosition().y, 32, 32)));

	for (int i = 0; i < PLOTS; i++) {
		std::stringstream ss;
		ss << "plot" << i;
		m_buttons["plot selection"].push_back(std::make_shared<Clickable>(ss.str(), sf::IntRect(m_plotSprites[i].getPosition().x, m_plotSprites[i].getPosition().y, 32, 32)));
	}
}

void ComputerGui::load() {
    std::stringstream filePath;
    filePath << RESOURCES_PATH << "emails.xml";

    pugi::xml_document doc;
    // TODO - do this everywhere
    pugi::xml_parse_result result = doc.load_file(filePath.str().c_str());
    if (result) {
        for (pugi::xml_node email = doc.child("email"); email; email = email.next_sibling("email")) {
            m_emails[email.attribute("id").as_int()] = std::make_shared<Email>(email.attribute("id").as_int(), email.attribute("from").as_string(), email.attribute("subject").as_string(), email.attribute("day").as_int(), email.attribute("month").as_int(), email.attribute("year").as_int(), email.attribute("content").as_string(), m_game);
        }
        for (pugi::xml_node contract = doc.child("contract"); contract; contract = contract.next_sibling("contract")) {
            m_contracts[contract.attribute("id").as_int()] = std::make_shared<Contract>(contract.attribute("id").as_int(), contract.attribute("from").as_string(), contract.attribute("good").as_string(), contract.attribute("details").as_string(), contract.attribute("quantity").as_int(), contract.attribute("reward").as_int(), contract.attribute("day").as_int(), contract.attribute("month").as_int(), contract.attribute("year").as_int(), m_game);
        }
    }
    else {
        std::cout << "Load result: " << filePath.str() << " " << result.description() << std::endl;
    }
}

void ComputerGui::update(double dt) {
    Gui::update(dt);

    if (m_displayEmail || m_displayPurchasing || m_displayContracts || m_displayPlotSelection) m_displayWindow = true;
    else m_displayWindow = false;

    if (m_displayInbox) {
        for (unsigned int i = 0; i < m_visibleInbox.size(); i++) {
            m_visibleInbox[i]->setSpritePosition(m_visibleEmailsPosition + sf::Vector2f(0, i*EMAIL_SPRITE_HEIGHT));
        }
    }
    else if (m_displayArchives) {
        for (unsigned int i = 0; i < m_visibleArchives.size(); i++) {
            m_visibleArchives[i]->setSpritePosition(m_visibleEmailsPosition + sf::Vector2f(0, i*EMAIL_SPRITE_HEIGHT));
        }
    }

    if (m_displayOffered) {
        for (unsigned int i = 0; i < m_visibleOffered.size(); i++) {
            m_visibleOffered[i]->setSpritePosition(m_visibleEmailsPosition + sf::Vector2f(0, i*EMAIL_SPRITE_HEIGHT));
        }
    }
    else if (m_displayAccepted) {
        for (unsigned int i = 0; i < m_visibleAccepted.size(); i++) {
            m_visibleAccepted[i]->setSpritePosition(m_visibleEmailsPosition + sf::Vector2f(0, i*EMAIL_SPRITE_HEIGHT));
        }
    }
    else if (m_displayCompleted) {
        for (unsigned int i = 0; i < m_visibleCompleted.size(); i++) {
            m_visibleCompleted[i]->setSpritePosition(m_visibleEmailsPosition + sf::Vector2f(0, i*EMAIL_SPRITE_HEIGHT));
        }
    }
    else if (m_displayRejected) {
        for (unsigned int i = 0; i < m_visibleRejected.size(); i++) {
            m_visibleRejected[i]->setSpritePosition(m_visibleEmailsPosition + sf::Vector2f(0, i*EMAIL_SPRITE_HEIGHT));
        }
    }

    if (m_offered.size() < 10) m_unreadContractsText.setString(std::to_string(m_offered.size()));
    else m_unreadContractsText.setString("9+");
    if (m_inbox.size() < 10) m_unreadEmailsText.setString(std::to_string(m_inbox.size()));
    else m_unreadEmailsText.setString("9+");

	if (m_displayPurchasing) {
		m_sellText.setPosition(m_sellInventory->position() - sf::Vector2f(m_sellText.getLocalBounds().width / 2, 30));
		m_buyText.setPosition(m_buyInventory->position() - sf::Vector2f(m_buyText.getLocalBounds().width / 2, 30));
		m_totalCostText.setPosition(m_visibleEmailsPosition + sf::Vector2f(-50, 400));

		std::stringstream ss;
		ss << "Total Cost: " << m_buyInventory->totalPriceOfItems();
		m_totalCostText.setString(ss.str());

		m_sellInventory->update(dt);
		m_buyInventory->update(dt);
	}

	if (m_stellaActivities["water"]) m_stellaWaterSprite.setTextureRect(sf::IntRect(32, 0, 32, 32));
	else m_stellaWaterSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	if (m_stellaActivities["weed"]) m_stellaWeedSprite.setTextureRect(sf::IntRect(32, 0, 32, 32));
	else m_stellaWeedSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	if (m_stellaActivities["fertilize"]) m_stellaFertilizeSprite.setTextureRect(sf::IntRect(32, 0, 32, 32));
	else m_stellaFertilizeSprite.setTextureRect(sf::IntRect(0, 0, 32, 32));
	if (m_stellaActivities["spray"]) m_stellaSpraySprite.setTextureRect(sf::IntRect(32, 0, 32, 32));
	else m_stellaSpraySprite.setTextureRect(sf::IntRect(0, 0, 32, 32));

	for (int i = 0; i < PLOTS; i++) {
		if (m_plots[i]) m_plotSprites[i].setTextureRect(sf::IntRect(32, 0, 32, 32));
		else m_plotSprites[i].setTextureRect(sf::IntRect(0, 0, 32, 32));
	}
}

void ComputerGui::draw() {
    Gui::draw();

    if (m_offered.size() > 0) {
        m_game->renderWindow()->draw(m_unreadContractsIcon);
        m_game->renderWindow()->draw(m_unreadContractsText);
    }
    if (m_inbox.size() > 0) {
        m_game->renderWindow()->draw(m_unreadEmailsIcon);
        m_game->renderWindow()->draw(m_unreadEmailsText);
    }

    if (m_displayWindow) {
        m_game->renderWindow()->draw(m_windowSprite);
        m_game->renderWindow()->draw(m_windowTitleText);

        if (m_displayEmail) {
            m_game->renderWindow()->draw(m_emailTabsSprite);

            if (m_displayInbox) {
                std::vector<std::shared_ptr<Email>>::iterator itr;
                for (itr = m_visibleInbox.begin(); itr != m_visibleInbox.end(); itr++) {
                    (*itr)->draw();
                }
            }
            else {
                std::vector<std::shared_ptr<Email>>::iterator itr;
                for (itr = m_visibleArchives.begin(); itr != m_visibleArchives.end(); itr++) {
                    (*itr)->draw();
                }
            }

            if (m_displayExpandedEmail) {
                m_game->renderWindow()->draw(m_emailExpandedSprite);
                m_game->renderWindow()->draw(m_emailFromText);
                m_game->renderWindow()->draw(m_emailSubjectText);
                m_game->renderWindow()->draw(m_emailDateText);
                m_game->renderWindow()->draw(m_emailContentText);
            }
        }

        if (m_displayContracts) {
            m_game->renderWindow()->draw(m_contractsTabsSprite);

            if (m_displayOffered) {
                std::vector<std::shared_ptr<Contract>>::iterator itr;
                for (itr = m_visibleOffered.begin(); itr != m_visibleOffered.end(); itr++) {
                    (*itr)->draw();
                }
            }
            else if (m_displayAccepted) {
                std::vector<std::shared_ptr<Contract>>::iterator itr;
                for (itr = m_visibleAccepted.begin(); itr != m_visibleAccepted.end(); itr++) {
                    (*itr)->draw();
                }
            }
            else if (m_displayCompleted) {
                std::vector<std::shared_ptr<Contract>>::iterator itr;
                for (itr = m_visibleCompleted.begin(); itr != m_visibleCompleted.end(); itr++) {
                    (*itr)->draw();
                }
            }
            else if (m_displayRejected) {
                std::vector<std::shared_ptr<Contract>>::iterator itr;
                for (itr = m_visibleRejected.begin(); itr != m_visibleRejected.end(); itr++) {
                    (*itr)->draw();
                }
            }

            if (m_displayExpandedContract) {
                m_game->renderWindow()->draw(m_emailExpandedSprite);
                m_game->renderWindow()->draw(m_emailFromText);
                m_game->renderWindow()->draw(m_emailSubjectText);
                m_game->renderWindow()->draw(m_emailDateText);
                m_game->renderWindow()->draw(m_emailContentText);

                if (m_displayOffered) {
                    m_game->renderWindow()->draw(m_acceptContractButtonSprite);
                    m_game->renderWindow()->draw(m_rejectContractButtonSprite);
                }
            }
        }

		if (m_displayPurchasing) {
			m_sellInventory->draw();
			m_buyInventory->draw();
			m_game->renderWindow()->draw(m_sellText);
			m_game->renderWindow()->draw(m_buyText);
			m_game->renderWindow()->draw(m_totalCostText);
		}
		if (m_displayPlotSelection) {
			m_game->renderWindow()->draw(m_plotSelectionSprite);
			for (int i = 0; i < PLOTS; i++) {
				m_game->renderWindow()->draw(m_plotSprites[i]);
			}
			m_game->renderWindow()->draw(m_stellaWaterSprite);
			m_game->renderWindow()->draw(m_stellaWeedSprite);
			m_game->renderWindow()->draw(m_stellaFertilizeSprite);
			m_game->renderWindow()->draw(m_stellaSpraySprite);
		}
    }
}

void ComputerGui::handleInput(sf::Event event) {
    if (event.type == sf::Event::MouseButtonReleased) {
        if (event.mouseButton.button == sf::Mouse::Left) {
            // If click outside the computer screen, close the computer.
            sf::Vector2f mousePos = m_game->mousePixelPosition();
            sf::FloatRect compBounds = m_sprite.getGlobalBounds();
            if (mousePos.x < compBounds.left
                || mousePos.x > compBounds.left + compBounds.width
                || mousePos.y < compBounds.top
            || mousePos.y > compBounds.top + compBounds.height) {
                m_game->closeComputer();
            }
            else handle(checkButtonsForClick(mousePos));

			if (m_displayPurchasing) {
				m_game->holdItem(m_sellInventory->handleClick(event, m_game->heldItem()));
				m_game->holdItem(m_buyInventory->handleClick(event, m_game->heldItem()));
			}
        }
    }
    if (event.type == sf::Event::MouseWheelScrolled) {
        if (event.mouseWheelScroll.delta > 0) {
            if (m_displayEmail && m_displayInbox && !m_displayExpandedEmail && m_aboveVisibleInbox.size() > 0) {
                m_belowVisibleInbox.insert(m_belowVisibleInbox.begin(), m_visibleInbox.back());
                m_visibleInbox.pop_back();
                m_visibleInbox.insert(m_visibleInbox.begin(), m_aboveVisibleInbox.back());
                m_aboveVisibleInbox.pop_back();
            }
            else if (m_displayEmail && m_displayArchives && !m_displayExpandedEmail && m_aboveVisibleArchives.size() > 0) {
                m_belowVisibleArchives.insert(m_belowVisibleArchives.begin(), m_visibleArchives.back());
                m_visibleArchives.pop_back();
                m_visibleArchives.insert(m_visibleArchives.begin(), m_aboveVisibleArchives.back());
                m_aboveVisibleArchives.pop_back();
            }
            else if (m_displayContracts && m_displayOffered && !m_displayExpandedContract && m_aboveVisibleOffered.size() > 0) {
                m_belowVisibleOffered.insert(m_belowVisibleOffered.begin(), m_visibleOffered.back());
                m_visibleOffered.pop_back();
                m_visibleOffered.insert(m_visibleOffered.begin(), m_aboveVisibleOffered.back());
                m_aboveVisibleOffered.pop_back();
            }
            else if (m_displayContracts && m_displayAccepted && !m_displayExpandedContract && m_aboveVisibleAccepted.size() > 0) {
                m_belowVisibleAccepted.insert(m_belowVisibleAccepted.begin(), m_visibleAccepted.back());
                m_visibleAccepted.pop_back();
                m_visibleAccepted.insert(m_visibleAccepted.begin(), m_aboveVisibleAccepted.back());
                m_aboveVisibleAccepted.pop_back();
            }
            else if (m_displayContracts && m_displayCompleted && !m_displayExpandedContract && m_aboveVisibleCompleted.size() > 0) {
                m_belowVisibleCompleted.insert(m_belowVisibleCompleted.begin(), m_visibleCompleted.back());
                m_visibleCompleted.pop_back();
                m_visibleCompleted.insert(m_visibleCompleted.begin(), m_aboveVisibleCompleted.back());
                m_aboveVisibleCompleted.pop_back();
            }
            else if (m_displayContracts && m_displayRejected && !m_displayExpandedContract && m_aboveVisibleRejected.size() > 0) {
                m_belowVisibleRejected.insert(m_belowVisibleRejected.begin(), m_visibleRejected.back());
                m_visibleRejected.pop_back();
                m_visibleRejected.insert(m_visibleRejected.begin(), m_aboveVisibleRejected.back());
                m_aboveVisibleRejected.pop_back();
            }
        }
        else {
            if (m_displayEmail && m_displayInbox && !m_displayExpandedEmail && m_belowVisibleInbox.size() > 0) {
                m_aboveVisibleInbox.push_back(m_visibleInbox.front());
                m_visibleInbox.erase(m_visibleInbox.begin());
                m_visibleInbox.push_back(m_belowVisibleInbox.front());
                m_belowVisibleInbox.erase(m_belowVisibleInbox.begin());
            }
            else if (m_displayEmail && m_displayArchives && !m_displayExpandedEmail && m_belowVisibleArchives.size() > 0) {
                m_aboveVisibleArchives.push_back(m_visibleArchives.front());
                m_visibleArchives.erase(m_visibleArchives.begin());
                m_visibleArchives.push_back(m_belowVisibleArchives.front());
                m_belowVisibleArchives.erase(m_belowVisibleArchives.begin());
            }
            else if (m_displayContracts && m_displayOffered && !m_displayExpandedContract && m_belowVisibleOffered.size() > 0) {
                m_aboveVisibleOffered.push_back(m_visibleOffered.front());
                m_visibleOffered.erase(m_visibleOffered.begin());
                m_visibleOffered.push_back(m_belowVisibleOffered.front());
                m_belowVisibleOffered.erase(m_belowVisibleOffered.begin());
            }
            else if (m_displayContracts && m_displayAccepted && !m_displayExpandedContract && m_belowVisibleAccepted.size() > 0) {
                m_aboveVisibleAccepted.push_back(m_visibleAccepted.front());
                m_visibleAccepted.erase(m_visibleAccepted.begin());
                m_visibleAccepted.push_back(m_belowVisibleAccepted.front());
                m_belowVisibleAccepted.erase(m_belowVisibleAccepted.begin());
            }
            else if (m_displayContracts && m_displayCompleted && !m_displayExpandedContract && m_belowVisibleCompleted.size() > 0) {
                m_aboveVisibleCompleted.push_back(m_visibleCompleted.front());
                m_visibleCompleted.erase(m_visibleCompleted.begin());
                m_visibleCompleted.push_back(m_belowVisibleCompleted.front());
                m_belowVisibleCompleted.erase(m_belowVisibleCompleted.begin());
            }
            else if (m_displayContracts && m_displayRejected && !m_displayExpandedContract && m_belowVisibleRejected.size() > 0) {
                m_aboveVisibleRejected.push_back(m_visibleRejected.front());
                m_visibleRejected.erase(m_visibleRejected.begin());
                m_visibleRejected.push_back(m_belowVisibleRejected.front());
                m_belowVisibleRejected.erase(m_belowVisibleRejected.begin());
            }
        }
    }
}

void ComputerGui::handle(std::string buttonName) {

    if (buttonName.compare("no buttons clicked") == 0) return;

    // E-Mail
    if(buttonName.compare("free-email") == 0) {
        // Do e-mail stuff.
        m_displayEmail = true;
        m_windowTitleText.setString("Free-email");
    }
    else if (buttonName.compare("inbox") == 0) {
        m_displayInbox = true;
        m_displayArchives = false;
        m_emailTabsSprite.setTextureRect(sf::IntRect(0, 0, 163, 471));
    }
    else if (buttonName.compare("archives") == 0) {
        m_displayInbox = false;
        m_displayArchives = true;
        m_emailTabsSprite.setTextureRect(sf::IntRect(163, 0, 163, 471));
    }
    else if (buttonName.compare("expand email") == 0) {
        m_displayExpandedEmail = true;
        m_emails[m_clickedEmail]->markAsRead();
        displayEmail(m_clickedEmail);
    }
    else if (buttonName.compare("close email") == 0 && m_displayExpandedEmail) {
        m_clickedEmail = -1;
        m_displayExpandedEmail = false;
    }
    // Purchasing
    else if (buttonName.compare("purchasing") == 0) {
        m_displayPurchasing = true;
        m_windowTitleText.setString("Purchasing");
    }
    // Contracts
    else if (buttonName.compare("contracts") == 0) {
        m_displayContracts = true;
        m_windowTitleText.setString("Contracts");
    }
    else if (buttonName.compare("offered") == 0) {
        m_displayOffered = true;
        m_displayAccepted = false;
        m_displayCompleted = false;
        m_displayRejected = false;
        m_contractsTabsSprite.setTextureRect(sf::IntRect(0, 0, 163, 471));
    }
    else if (buttonName.compare("accepted") == 0) {
        m_displayOffered = false;
        m_displayAccepted = true;
        m_displayCompleted = false;
        m_displayRejected = false;
        m_contractsTabsSprite.setTextureRect(sf::IntRect(163, 0, 163, 471));
    }
    else if (buttonName.compare("completed") == 0) {
        m_displayOffered = false;
        m_displayAccepted = false;
        m_displayCompleted = true;
        m_displayRejected = false;
        m_contractsTabsSprite.setTextureRect(sf::IntRect(326, 0, 163, 471));
    }
    else if (buttonName.compare("rejected") == 0) {
        m_displayOffered = false;
        m_displayAccepted = false;
        m_displayCompleted = false;
        m_displayRejected = true;
        m_contractsTabsSprite.setTextureRect(sf::IntRect(489, 0, 163, 471));
    }
    else if (buttonName.compare("expand contract") == 0) {
        m_displayExpandedContract = true;
        displayContract(m_clickedContract);
    }
    else if (buttonName.compare("accept contract") == 0) {
        if (acceptContract(m_clickedContract)) {
            m_displayExpandedContract = false;
            m_clickedContract = -1;
        }
        else {
            // TODO - display message telling user they are at max contracts
        }
    }
    else if (buttonName.compare("reject contract") == 0) {
        rejectContract(m_clickedContract);
        m_displayExpandedContract = false;
        m_clickedContract = -1;
    }
    else if (buttonName.compare("close contract") == 0 && m_displayExpandedContract) {
        m_clickedContract = -1;
        m_displayExpandedContract = false;
    }
    // Taskmaster
    else if (buttonName.compare("taskmaster") == 0) {
		m_displayPlotSelection = true;
        m_windowTitleText.setString("Taskmaster");
    }
	else if (buttonName.compare("water") == 0) {
		m_stellaActivities["water"] = !m_stellaActivities["water"];
	}
	else if (buttonName.compare("weed") == 0) {
		m_stellaActivities["weed"] = !m_stellaActivities["weed"];
	}
	else if (buttonName.compare("fertilize") == 0) {
		m_stellaActivities["fertilize"] = !m_stellaActivities["fertilize"];
	}
	else if (buttonName.compare("spray") == 0) {
		m_stellaActivities["spray"] = !m_stellaActivities["spray"];
	}
	else if (buttonName.compare("area") == 0) {
		m_displayPlotSelection = true;
	}
	else if (buttonName.substr(0, buttonName.length() - 1).compare("plot") == 0) {
		m_plots[stoi(buttonName.substr(buttonName.length() - 1))] = !m_plots[stoi(buttonName.substr(buttonName.length() - 1))];
	}
	else if (buttonName.compare("assign al") == 0) {
		std::cout << "al" << std::endl;
	}
	else if (buttonName.compare("assign robby") == 0) {
		std::cout << "robby" << std::endl;
	}
    else if (buttonName.compare("close window") == 0) {
        m_displayEmail = false;
        m_displayPurchasing = false;
        m_displayContracts = false;
		m_displayPlotSelection = false;
    }
    else if (buttonName.compare("log off") == 0) {
        m_game->closeComputer();
    }
}

std::string ComputerGui::checkButtonsForClick(sf::Vector2f mousePos) {
    std::string whichLayer = "";
	if (!m_displayEmail && !m_displayPurchasing && !m_displayContracts && !m_displayPlotSelection) whichLayer = "desktop";
	else if (m_displayEmail) whichLayer = "email";
	else if (m_displayPurchasing) whichLayer = "purchasing";
	else if (m_displayContracts) whichLayer = "contracts";
	else if (m_displayPlotSelection) whichLayer = "plot selection";

    std::vector<std::shared_ptr<Clickable>>::iterator itr;
    for (itr = m_buttons[whichLayer].begin(); itr != m_buttons[whichLayer].end(); itr++) {
        if ((*itr)->wasClicked(mousePos))
            return (*itr)->name();
    }

    // Check e-mails too!
    if (m_displayEmail && m_displayInbox && m_visibleInbox.size() > 0 && !m_displayExpandedEmail) {
        std::vector<std::shared_ptr<Email>>::iterator itr2;
        for (itr2 = m_visibleInbox.begin(); itr2 != m_visibleInbox.end(); itr2++) {
            if ((*itr2)->wasClicked(mousePos)) {
                m_clickedEmail = (*itr2)->id();
                return "expand email";
            }
        }
    }
    else if (m_displayEmail && m_displayArchives && !m_displayExpandedEmail) {
        std::vector<std::shared_ptr<Email>>::iterator itr2;
        for (itr2 = m_visibleArchives.begin(); itr2 != m_visibleArchives.end(); itr2++) {
            if ((*itr2)->wasClicked(mousePos)) {
                m_clickedEmail = (*itr2)->id();
                return "expand email";
            }
        }
    }

    // Contracts
    if (m_displayContracts && m_displayOffered && m_visibleOffered.size() > 0 && !m_displayExpandedContract) {
        std::vector<std::shared_ptr<Contract>>::iterator itr2;
        for (itr2 = m_visibleOffered.begin(); itr2 != m_visibleOffered.end(); itr2++) {
            if ((*itr2)->wasClicked(mousePos)) {
                m_clickedContract = (*itr2)->id();
                return "expand contract";
            }
        }
    }
    else if (m_displayContracts && m_displayAccepted && m_visibleAccepted.size() > 0 && !m_displayExpandedContract) {
        std::vector<std::shared_ptr<Contract>>::iterator itr2;
        for (itr2 = m_visibleAccepted.begin(); itr2 != m_visibleAccepted.end(); itr2++) {
            if ((*itr2)->wasClicked(mousePos)) {
                m_clickedContract = (*itr2)->id();
                return "expand contract";
            }
        }
    }
    else if (m_displayContracts && m_displayCompleted && m_visibleCompleted.size() > 0 && !m_displayExpandedContract) {
        std::vector<std::shared_ptr<Contract>>::iterator itr2;
        for (itr2 = m_visibleCompleted.begin(); itr2 != m_visibleCompleted.end(); itr2++) {
            if ((*itr2)->wasClicked(mousePos)) {
                m_clickedContract = (*itr2)->id();
                return "expand contract";
            }
        }
    }
    else if (m_displayContracts && m_displayRejected && m_visibleRejected.size() > 0 && !m_displayExpandedContract) {
        std::vector<std::shared_ptr<Contract>>::iterator itr2;
        for (itr2 = m_visibleRejected.begin(); itr2 != m_visibleRejected.end(); itr2++) {
            if ((*itr2)->wasClicked(mousePos)) {
                m_clickedContract = (*itr2)->id();
                return "expand contract";
            }
        }
    }

    return "no buttons clicked";
}

void ComputerGui::advanceDay(std::string fullDate) {
    archiveInbox();
    distributeEmails(fullDate);
    distributeContracts(fullDate);
    checkForCompletedContracts();
	purchaseItems();
	restockSellInventory();
}

//////////////////////////////////////////////////////////////////////////////
//                              E-mail                                      //
//////////////////////////////////////////////////////////////////////////////

void ComputerGui::displayEmail(int emailId) {
    m_emailFromText.setString(m_emails[emailId]->from());
    m_emailSubjectText.setString(m_emails[emailId]->subject());
    m_emailDateText.setString(m_emails[emailId]->fullDateAsString());
    m_emailContentText.setString(m_emails[emailId]->content());
}

void ComputerGui::distributeEmails(std::string date) {
    std::unordered_map<int, std::shared_ptr<Email>>::iterator itr;
    for (itr = m_emails.begin(); itr != m_emails.end(); itr++) {
        if (itr->second->fullDateAsString().compare(date) == 0) {
            inboxEmail(itr->first);
        }
    }
}

void ComputerGui::inboxEmail(int emailId) {
    if (m_emails.find(emailId) != m_emails.end()) {
        m_inbox[emailId] = m_emails[emailId];
        sortInboxByDate();
    }
    else {
        std::cout << "Email with id " << emailId << " does not exist." << std::endl;
    }
}

void ComputerGui::archiveInbox() {
    if (m_inbox.size() == 0) return;

    std::vector<int> inbox = inboxedEmailIds();
    std::vector<int>::iterator itr;
    for (itr = inbox.begin(); itr != inbox.end(); itr++) {
        if (m_emails[(*itr)]->read())
            archiveEmail(*itr);
    }
}

void ComputerGui::archiveEmail(int emailId) {
    if (m_emails.find(emailId) != m_emails.end()) {
        m_archives[emailId] = m_emails[emailId];
        m_inbox.erase(emailId);
        sortArchivesByDate();
        sortInboxByDate();
    }
    else {
        std::cout << "Email with id " << emailId << " does not exist." << std::endl;
    }
}

void ComputerGui::sortInboxByDate() {
    m_visibleInbox.clear();
    m_belowVisibleInbox.clear();
    m_aboveVisibleInbox.clear();

    std::unordered_map<int, std::shared_ptr<Email>>::iterator itr;
    for (itr = m_inbox.begin(); itr != m_inbox.end(); itr++) {
        m_visibleInbox.push_back(itr->second);
    }
    std::sort(m_visibleInbox.begin(), m_visibleInbox.end(), sortEmailsByDate);

    while (m_visibleInbox.size() > 5) {
        m_belowVisibleInbox.insert(m_belowVisibleInbox.begin(), m_visibleInbox.back());
        m_visibleInbox.pop_back();
    }
}

void ComputerGui::sortArchivesByDate() {
    m_visibleArchives.clear();
    m_belowVisibleArchives.clear();
    m_aboveVisibleArchives.clear();

    std::unordered_map<int, std::shared_ptr<Email>>::iterator itr;
    for (itr = m_archives.begin(); itr != m_archives.end(); itr++) {
        m_visibleArchives.push_back(itr->second);
    }
    std::sort(m_visibleArchives.begin(), m_visibleArchives.end(), sortEmailsByDate);
    std::reverse(m_visibleArchives.begin(), m_visibleArchives.end());

    while (m_visibleArchives.size() > 5) {
        m_belowVisibleArchives.insert(m_belowVisibleArchives.begin(), m_visibleArchives.back());
        m_visibleArchives.pop_back();
    }
}

std::vector<int> ComputerGui::inboxedEmailIds() {
    std::vector<int> ids;
    std::unordered_map<int, std::shared_ptr<Email>>::iterator itr;
    for (itr = m_inbox.begin(); itr != m_inbox.end(); itr++) {
        if (itr->second)
            ids.push_back(itr->first);
    }

    return ids;
}

std::vector<int> ComputerGui::archivedEmailIds() {
    std::vector<int> ids;
    std::unordered_map<int, std::shared_ptr<Email>>::iterator itr;
    for (itr = m_archives.begin(); itr != m_archives.end(); itr++) {
        if (itr->second)
            ids.push_back(itr->first);
    }

    return ids;
}

//////////////////////////////////////////////////////////////////////////////////
//                              Contracts                                       //
//////////////////////////////////////////////////////////////////////////////////

void ComputerGui::displayContract(int contractId) {
    m_emailFromText.setString(m_contracts[contractId]->from());
    m_emailSubjectText.setString(m_contracts[contractId]->goods());
    m_emailDateText.setString(m_contracts[contractId]->fullDateAsString());
    m_emailContentText.setString(m_contracts[contractId]->content());
}

void ComputerGui::distributeContracts(std::string date) {
    std::unordered_map<int, std::shared_ptr<Contract>>::iterator itr;
    for (itr = m_contracts.begin(); itr != m_contracts.end(); itr++) {
        if (itr->second->fullDateAsString().compare(date) == 0) {
            offerContract(itr->first);
        }
    }
}

void ComputerGui::offerContract(int contractId) {
    if (m_contracts.find(contractId) != m_contracts.end()) {
        m_offered[contractId] = m_contracts[contractId];
        sortOfferedByDate();
    }
    else {
        std::cout << "Contract with id " << contractId << " does not exist." << std::endl;
    }
}

bool ComputerGui::acceptContract(int contractId) {
    if (m_contracts.find(contractId) != m_contracts.end()) {
        if (m_accepted.size() < MAX_CONTRACTS) {
            m_accepted[contractId] = m_contracts[contractId];
            m_offered.erase(contractId);
            sortAcceptedByDate();
            sortOfferedByDate();

            // Update corresponding hopper
            bool hopper1 = false;
            bool hopper2 = false;
            bool hopper3 = false;
            bool hopper4 = false;
            std::unordered_map<int, std::shared_ptr<Contract>>::iterator itr;
            for (itr = m_accepted.begin(); itr != m_accepted.end(); itr++) {
                if(itr->second->hopperName().compare("hopper1") == 0) hopper1 = true;
                if(itr->second->hopperName().compare("hopper2") == 0) hopper2 = true;
                if(itr->second->hopperName().compare("hopper3") == 0) hopper3 = true;
                if(itr->second->hopperName().compare("hopper4") == 0) hopper4 = true;
            }
            if (!hopper1) m_accepted[contractId]->connectToHopper("hopper1");
            else if (!hopper2) m_accepted[contractId]->connectToHopper("hopper2");
            else if (!hopper3) m_accepted[contractId]->connectToHopper("hopper3");
            else if (!hopper4) m_accepted[contractId]->connectToHopper("hopper4");
            return true;
        }
        return false;
    }
    else {
        std::cout << "Contract with id " << contractId << " does not exist." << std::endl;
        return false;
    }

    return false;
}

void ComputerGui::completeContract(int contractId, bool reward) {
    if (m_contracts.find(contractId) != m_contracts.end()) {
        m_completed[contractId] = m_contracts[contractId];
        m_accepted.erase(contractId);
        sortAcceptedByDate();
        sortCompletedByDate();
        if (reward) {
            std::dynamic_pointer_cast<Player>(m_game->entity("player"))->addFunds(m_contracts[contractId]->reward());
            m_completed[contractId]->disconnectFromHopper();
        }
    }
    else {
        std::cout << "Contract with id " << contractId << " does not exist." << std::endl;
    }
}

void ComputerGui::rejectContract(int contractId) {
    if (m_contracts.find(contractId) != m_contracts.end()) {
        m_rejected[contractId] = m_contracts[contractId];
        m_offered.erase(contractId);
        sortRejectedByDate();
        sortOfferedByDate();
    }
    else {
        std::cout << "Contract with id " << contractId << " does not exist." << std::endl;
    }
}

void ComputerGui::sortOfferedByDate() {
    m_visibleOffered.clear();
    m_belowVisibleOffered.clear();
    m_aboveVisibleAccepted.clear();

    std::unordered_map<int, std::shared_ptr<Contract>>::iterator itr;
    for (itr = m_offered.begin(); itr != m_offered.end(); itr++) {
        m_visibleOffered.push_back(itr->second);
    }
    std::sort(m_visibleOffered.begin(), m_visibleOffered.end(), sortContractsByDate);

    while (m_visibleOffered.size() > 5) {
        m_belowVisibleOffered.insert(m_belowVisibleOffered.begin(), m_visibleOffered.back());
        m_visibleOffered.pop_back();
    }

}

void ComputerGui::sortAcceptedByDate() {
    m_visibleAccepted.clear();
    m_belowVisibleAccepted.clear();
    m_aboveVisibleAccepted.clear();

    std::unordered_map<int, std::shared_ptr<Contract>>::iterator itr;
    for (itr = m_accepted.begin(); itr != m_accepted.end(); itr++) {
        m_visibleAccepted.push_back(itr->second);
    }
    std::sort(m_visibleAccepted.begin(), m_visibleAccepted.end(), sortContractsByDate);

    while (m_visibleAccepted.size() > 5) {
        m_belowVisibleAccepted.insert(m_belowVisibleAccepted.begin(), m_visibleAccepted.back());
        m_visibleAccepted.pop_back();
    }
}

void ComputerGui::sortCompletedByDate() {
    m_visibleCompleted.clear();
    m_belowVisibleCompleted.clear();
    m_aboveVisibleCompleted.clear();

    std::unordered_map<int, std::shared_ptr<Contract>>::iterator itr;
    for (itr = m_completed.begin(); itr != m_completed.end(); itr++) {
        m_visibleCompleted.push_back(itr->second);
    }
    std::sort(m_visibleCompleted.begin(), m_visibleCompleted.end(), sortContractsByDate);

    while (m_visibleCompleted.size() > 5) {
        m_belowVisibleCompleted.insert(m_belowVisibleCompleted.begin(), m_visibleCompleted.back());
        m_visibleCompleted.pop_back();
    }
}

void ComputerGui::sortRejectedByDate() {
    m_visibleRejected.clear();
    m_belowVisibleRejected.clear();
    m_aboveVisibleRejected.clear();

    std::unordered_map<int, std::shared_ptr<Contract>>::iterator itr;
    for (itr = m_rejected.begin(); itr != m_rejected.end(); itr++) {
        m_visibleRejected.push_back(itr->second);
    }
    std::sort(m_visibleRejected.begin(), m_visibleRejected.end(), sortContractsByDate);

    while (m_visibleRejected.size() > 5) {
        m_belowVisibleRejected.insert(m_belowVisibleRejected.begin(), m_visibleRejected.back());
        m_visibleRejected.pop_back();
    }
}

void ComputerGui::checkForCompletedContracts() {
    std::unordered_map<int, std::shared_ptr<Contract>>::iterator itr;
    std::vector<int> completed;
    int whichHopper = 1;
    for (itr = m_accepted.begin(); itr != m_accepted.end(); itr++) {
        std::stringstream hopperName;
        hopperName << "hopper" << whichHopper;

        int fulfilledCount = 0;
        if (itr->second->goods().compare("Any Vegetable") == 0) {
            for (const auto& veggie : VALID_VEGGIES) {
                fulfilledCount += std::dynamic_pointer_cast<ItemLocker>(m_game->entity(hopperName.str()))->count(veggie);
            }
        }
        else if (itr->second->goods().compare("Any Fruit") == 0) {
            for (const auto& fruit : VALID_FRUITS) {
                fulfilledCount += std::dynamic_pointer_cast<ItemLocker>(m_game->entity(hopperName.str()))->count(fruit);
            }
        }
        else if (itr->second->goods().compare("Any Good") == 0) {
            for (const auto& veggie : VALID_VEGGIES) {
                fulfilledCount += std::dynamic_pointer_cast<ItemLocker>(m_game->entity(hopperName.str()))->count(veggie);
            }
            for (const auto& fruit : VALID_FRUITS) {
                fulfilledCount += std::dynamic_pointer_cast<ItemLocker>(m_game->entity(hopperName.str()))->count(fruit);
            }
        }
        else {
            fulfilledCount += std::dynamic_pointer_cast<ItemLocker>(m_game->entity(hopperName.str()))->count(itr->second->goods());
			std::cout << fulfilledCount << std::endl;
        }

        if (fulfilledCount >= itr->second->quantity()) {
            completed.push_back(itr->first);
        }
        whichHopper++;
    }

    for (int i = 0; i < completed.size(); i++) {
        completeContract(completed[i], true);
    }
}

std::vector<int> ComputerGui::offeredContractIds() {
    std::vector<int> ids;
    std::unordered_map<int, std::shared_ptr<Contract>>::iterator itr;
    for (itr = m_offered.begin(); itr != m_offered.end(); itr++) {
        //if (itr->second)
            ids.push_back(itr->first);
    }

    return ids;
}

std::vector<int> ComputerGui::acceptedContractIds() {
    std::vector<int> ids;
    std::unordered_map<int, std::shared_ptr<Contract>>::iterator itr;
    for (itr = m_accepted.begin(); itr != m_accepted.end(); itr++) {
        //if (itr->second)
            ids.push_back(itr->first);
    }

    return ids;
}

std::vector<int> ComputerGui::completedContractIds() {
    std::vector<int> ids;
    std::unordered_map<int, std::shared_ptr<Contract>>::iterator itr;
    for (itr = m_completed.begin(); itr != m_completed.end(); itr++) {
        //if (itr->second)
            ids.push_back(itr->first);
    }

    return ids;
}

std::vector<int> ComputerGui::rejectedContractIds() {
    std::vector<int> ids;
    std::unordered_map<int, std::shared_ptr<Contract>>::iterator itr;
    for (itr = m_rejected.begin(); itr != m_rejected.end(); itr++) {
        //if (itr->second)
            ids.push_back(itr->first);
    }

    return ids;
}

//////////////////////////////////////////////////////////////////////////////////////
//									Purchasing										//
//////////////////////////////////////////////////////////////////////////////////////

void ComputerGui::offerForSale(std::shared_ptr<Item> item) {
	m_sellInventory->addToInventory(item);
}

void ComputerGui::offerForSale(std::vector<std::shared_ptr<Item>> items) {
	m_sellInventory->addToInventory(items);
}

void ComputerGui::purchaseItems() {
	std::dynamic_pointer_cast<Player>(m_game->entity("player"))->addFunds(-1 * m_buyInventory->totalPriceOfItems());
	std::vector <std::shared_ptr<Item>> itemsToBuy = m_buyInventory->allItems();
	std::dynamic_pointer_cast<InventoryGui>(m_game->gui("purchasing"))->addToInventory(itemsToBuy);
	m_buyInventory->clear();
}

void ComputerGui::restockSellInventory() {
	m_sellInventory->clear();
	m_sellInventory->addToInventory(std::make_shared<Seed>(CROP_ASPARAGUS, 10, "base", m_game));
	m_sellInventory->addToInventory(std::make_shared<Seed>(CROP_BEETS, 10, "base", m_game));
	m_sellInventory->addToInventory(std::make_shared<Seed>(CROP_BOKCHOY, 10, "base", m_game));
	m_sellInventory->addToInventory(std::make_shared<Seed>(CROP_CARROTS, 10, "base", m_game));
	m_sellInventory->addToInventory(std::make_shared<Seed>(CROP_GARLIC, 10, "base", m_game));
}

//////////////////////////////////////////////////////////////////////////////////////
//                              Taskmaster		                                    //
//////////////////////////////////////////////////////////////////////////////////////

std::unordered_map<std::string, bool> ComputerGui::stellaActivities() {
	return m_stellaActivities;
}

std::unordered_map<int, bool> ComputerGui::stellaPlots() {
	return m_plots;
}

void ComputerGui::setStellaActivities(std::unordered_map<std::string, bool> activities) {
	m_stellaActivities.clear();
	m_stellaActivities = activities;
}

void ComputerGui::setStellaPlots(std::unordered_map<int, bool> plots) {
	m_plots.clear();
	m_plots = plots;
}

//////////////////////////////////////////////////////////////////////////////////////
//                              Helper Functions                                    //
//////////////////////////////////////////////////////////////////////////////////////

bool ComputerGui::sortEmailsByDate(std::shared_ptr<Email> emailA, std::shared_ptr<Email> emailB) {
    if (emailA->year() < emailB->year()) return true;
    else if (emailA->year() == emailB->year()) {
        if(emailA->month() < emailB->month()) return true;
        else if (emailA->month() == emailB->month()) {
            if(emailA->day() < emailB->day()) return true;
            else return false;
        }
        else return false;
    }
    else return false;
}

bool ComputerGui::sortContractsByDate(std::shared_ptr<Contract> contractA, std::shared_ptr<Contract> contractB) {
    if (contractA->year() < contractB->year()) return true;
    else if (contractA->year() == contractB->year()) {
        if(contractA->month() < contractB->month()) return true;
        else if (contractA->month() == contractB->month()) {
            if(contractA->day() < contractB->day()) return true;
            else return false;
        }
        else return false;
    }
    else return false;
}


