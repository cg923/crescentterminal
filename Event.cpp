
// Copyright 2019 Happyrock Studios

#include "Event.h"

// STD
#include <sstream>
#include <iostream>

Event::Event(std::string name,
             std::string type,
             std::vector<std::string> commands,
             bool repeat,
			 bool immediate) {
    m_name = name;
    m_type = type;
    m_activated = false;
    m_repeat = repeat;
	m_immediate = immediate;

    parse(commands);
}

void Event::parse(std::vector<std::string> commands) {
    std::vector<std::string>::iterator itr;
    for (itr = commands.begin(); itr != commands.end(); itr++) {
        Command command;
        command.who = (*itr).substr(0, (*itr).find(":"));
        command.what = (*itr).substr((*itr).find(":") + 1);
        if(command.what.find(":") != std::string::npos) {
            command.where = command.what.substr(command.what.find(":") + 1);
            command.what = command.what.substr(0, command.what.find(":"));
        } else {
            command.where = "";
        }
        m_commands.push_back(command);
    }
}

std::vector<Command> Event::execute() {
    if(!m_activated) {
        m_activated = true;
        return m_commands;
    }

    return std::vector<Command>();
}

void Event::reset() {
    if(m_repeat) {
        m_activated = false;
    }
}

const std::string Event::name() {
    return m_name;
}

const bool Event::immediate() {
	return m_immediate;
}

void Event::setImmediate(bool immediate) {
	m_immediate = immediate;
}
