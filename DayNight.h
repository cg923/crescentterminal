
// Copyright 2019 Happyrock Studios

#ifndef DAYNIGHT_H
#define DAYNIGHT_H

/**
 * This class keeps track of, pauses, resumes, and interprets the Day/Night cycle.
 * It shouldn't determine what to do with its own information, simply provide it
 * to the game as appropriate
 */

// Moon
class Game;

// STD
#include <memory>

// SFML
#include <SFML/Graphics.hpp>
#include "libraries/Chronometer.hpp"

class DayNight {
public:
    DayNight(std::shared_ptr<Game> game, float timeScale = 0.01f);

    void startFromBeginningOfTime();

	void drawDarkness();

    void pause();

    void resume();

    void toggle();

    void update();

    void updateStrings();

    void advanceDay();

    void advanceToHour(int hour);

	void beginFadeIn();

	void beginFadeOut();

	void fadeIn();

	void fadeOut();

	void resetDarkness();

    //////////////////////////////////////////////////
    //              Getters & Setters               //
    //////////////////////////////////////////////////

    const int seconds();

    const int minutes();

    const int hours();

    const std::string fullTime();

    void setTime(sf::Time currentTime, bool startNow = false);

    const int day();

    void setDay(int day);

    const std::string dayOfWeek();

    const int month();

    void setMonth(int month);

    const int year();

    void setYear(int year);

    const std::string fullDate();

    const std::string timeOfDay();

    void setDate(std::string currentDate);

    const int getDarknessAlpha();

	void setDarknessAlpha(int newAlpha);

	const bool fadingIn();

	const bool fadingOut();

	std::string formatTime(int hours, int minutes);

private:
    std::shared_ptr<Game> m_game;

    sftools::Chronometer   m_dayNightClock;

    std::string m_currentDateAsString;

    std::string m_currentTimeAsString;

	sf::RectangleShape  m_darkness;
	sf::Clock           m_darknessTimer;
	sf::RenderTexture	m_darknessRenderTex;
	int					m_darknessAlphaOverride;

	sf::Sprite			m_flashlightLightSprite;

    float m_timeScale;

    int m_day;
    int m_month;
    int m_year;

    int m_hours;
    int m_minutes;
    int m_secs;

	bool m_fadingOut;
	bool m_fadingIn;
};

#endif // DAYNIGHT_H
