
// Copyright 2019 Happyrock Studios

// Moon
#include "Door.h"
#include "Game.h"
#include "Map.h"

// STD
#include <iostream>

Door::Door(std::string currentMap, std::shared_ptr<Game> game, std::string owner)
    : Entity("door", "door", "door", currentMap, game, 32, 64) {
    m_open = false;
    unlock();
    m_owner = owner;

    m_openCloseTimer.reset(true);
    m_openCloseTimer.add(sf::seconds(2.f));

    m_sprites["middle"].push_back(m_sprite);
}

void Door::update(double dt) {
    Entity::update(dt);

    // TODO - this is dumb and we only do this because setting sprite origin has no effect which
    // is our fault.
    m_sprite->setPosition(m_position - sf::Vector2f(16,16));

    if (someoneTryingToOpen()) open();

    if (m_open) {
        if (m_openCloseTimer.getElapsedTime().asSeconds() <= 0.02) {
            m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().left, 0, 32, 64));
        }
        else if (m_openCloseTimer.getElapsedTime().asSeconds() > 0.02 && m_openCloseTimer.getElapsedTime().asSeconds() < 0.04) {
            m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().left, 64, 32, 64));
        }
        else if (m_openCloseTimer.getElapsedTime().asSeconds() >= 0.04 && m_openCloseTimer.getElapsedTime().asSeconds() < 2) {
            m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().left, 0, 0, 0));
        }
        else {
            close();
        }
    }
    else {
        if (m_openCloseTimer.getElapsedTime().asSeconds() <= 0.02) {
            m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().left, 0, 0, 0));
        }
        else if (m_openCloseTimer.getElapsedTime().asSeconds() > 0.02 && m_openCloseTimer.getElapsedTime().asSeconds() < 0.04) {
            m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().left, 64, 32, 64));
        }
        else {
            m_sprite->setTextureRect(sf::IntRect(m_sprite->getTextureRect().left, 0, 32, 64));
        }
    }
}

void Door::unlock() {
    m_locked = false;
    m_sprite->setTextureRect(sf::IntRect(32, m_sprite->getTextureRect().top, 32, 64));
    m_game->map(m_currentMap)->makeTileNotAnObstacle(m_gridPosition.x, m_gridPosition.y);
    m_game->map(m_currentMap)->makeTileNotAnObstacle(m_gridPosition.x, m_gridPosition.y + 1);
}

void Door::lock() {
    m_locked = true;
    m_sprite->setTextureRect(sf::IntRect(0, m_sprite->getTextureRect().top, 32, 64));
    m_game->map(m_currentMap)->makeTileAnObstacle(m_gridPosition.x, m_gridPosition.y);
    m_game->map(m_currentMap)->makeTileAnObstacle(m_gridPosition.x, m_gridPosition.y + 1);
}

void Door::open() {
    if (m_locked || m_open) return;
    m_open = true;
    m_openCloseTimer.reset(true);
}

void Door::close() {
    if (!m_open || someoneInDoorWay()) return;
    m_open = false;
    m_openCloseTimer.reset(true);
}

bool Door::someoneInDoorWay() {
    std::vector<std::shared_ptr<Entity>> nearbyEntities;
    std::shared_ptr<Entity> entity1 = m_game->checkForEntityByGrid(m_gridPosition.x, m_gridPosition.y - 1, m_currentMap, "door");
    std::shared_ptr<Entity> entity2 = m_game->checkForEntityByGrid(m_gridPosition.x, m_gridPosition.y, m_currentMap, "door");
    std::shared_ptr<Entity> entity3 = m_game->checkForEntityByGrid(m_gridPosition.x, m_gridPosition.y + 1, m_currentMap, "door");
    std::shared_ptr<Entity> entity4 = m_game->checkForEntityByGrid(m_gridPosition.x, m_gridPosition.y + 2, m_currentMap, "door");
    if (entity1) nearbyEntities.push_back(entity1);
    if (entity2) nearbyEntities.push_back(entity2);
    if (entity3) nearbyEntities.push_back(entity3);
    if (entity4) nearbyEntities.push_back(entity4);

    std::vector<std::shared_ptr<Entity>>::iterator itr;
    for (itr = nearbyEntities.begin(); itr != nearbyEntities.end(); itr++) {
        if ((*itr)->type().compare("player") == 0 || (*itr)->type().compare("npc") == 0) return true;
    }

    return false;
}

bool Door::someoneTryingToOpen() {
    std::vector<std::shared_ptr<Entity>> nearbyEntities;
    std::shared_ptr<Entity> entity1 = m_game->checkForEntityByGrid(m_gridPosition.x, m_gridPosition.y - 1, m_currentMap, "door");
    std::shared_ptr<Entity> entity4 = m_game->checkForEntityByGrid(m_gridPosition.x, m_gridPosition.y + 2, m_currentMap, "door");
    if (entity1) nearbyEntities.push_back(entity1);
    if (entity4) nearbyEntities.push_back(entity4);

    std::vector<std::shared_ptr<Entity>>::iterator itr;
    for (itr = nearbyEntities.begin(); itr != nearbyEntities.end(); itr++) {
        if ((*itr)->type().compare("player") == 0 || (*itr)->type().compare("npc") == 0) return true;
    }

    return false;
}

const std::string Door::owner() {
    return m_owner;
}

void Door::setOwner(std::string owner) {
    m_owner = owner;
}
