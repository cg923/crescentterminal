
// Copyright 2019 Happyrock Studios

#ifndef CHARACTER_H
#define CHARACTER_H

// Moon
#include "Entity.h"

// STD
#include <stack>

const int CHARA_SPRITE_WIDTH = 16;
const int CHARA_SPRITE_HEIGHT = 26;

struct CheckTile {
	CheckTile(sf::Vector2i position, std::shared_ptr<CheckTile> parent)
		: m_position(position), m_parent(parent), m_head(false) {}
	CheckTile(sf::Vector2i position)
		: m_position(position), m_parent(NULL), m_head(true) {}
	sf::Vector2i m_position;
	std::shared_ptr<CheckTile> m_parent;
	bool m_head;
};

class Character : public Entity {
public:
    Character(std::string name,
              std::string type,
              std::string texturePath,
              std::string currentMap,
              Direction direction,
              std::shared_ptr<Game> game);

    virtual void defineHitbox();

    virtual void defineAnimations();

    virtual void update(double dt);

    virtual void updateAnimation();

    virtual void walk();

	virtual void walk(sf::Vector2f nextDest);

    void setPositionByGrid(int x, int y);

    void setPositionByGrid(sf::Vector2i vec);

    void setPositionByPixel(float x, float y);

    void setPositionByPixel(sf::Vector2f vec);

    virtual void determineDirection();

    void checkForCollisions();

    virtual void advanceDay() {}

    void setCurrentMap(std::string newMap);

	const bool walking();

	void setWalkTarget(int x, int y);

protected:
	void determinePath();

	sf::Vector2i m_walkTarget;
	std::stack<sf::Vector2i> m_path;
	std::string m_warpTo;

    bool m_leftCollision;
    bool m_rightCollision;
    bool m_upCollision;
    bool m_downCollision;

    // TODO - these probably belong in Player
    bool m_up;
    bool m_down;
    bool m_left;
    bool m_right;
    bool m_walking;
    bool m_working;

    float m_maxSpeed;
    float m_speed;
    float m_tempSpeed;
};
#endif // CHARACTER_H
