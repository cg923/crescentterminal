
// Copyright 2019 Corey Selover

#include "TitleScreen.h"
#include "Game.h"

// Moon
#include "Gui.h"

// STD
#include <iostream>

TitleScreen::TitleScreen(std::shared_ptr<Game> game)
	: m_game(game), m_saveSlot(-1) {

	m_backgroundSprite.setTexture(*m_game->texture("title_screen"));
	m_glowSprite.setTexture(*m_game->texture("title_glow"));
	m_logoSprite.setTexture(*m_game->texture("title_logo"));

	sf::Vector2u windowSize = m_game->renderWindow()->getSize();
	sf::Vector2u textureSize = m_backgroundSprite.getTexture()->getSize();
	sf::Vector2f windowScale = sf::Vector2f(windowSize.x / textureSize.x, windowSize.y / textureSize.y);
	m_backgroundSprite.scale(windowScale.x, windowScale.y);
	m_glowSprite.scale(windowScale.x, windowScale.y);
	m_logoSprite.scale(windowScale.x, windowScale.y);

	m_logoSprite.setColor(sf::Color(255, 255, 255, 0));
	m_glowSprite.setColor(sf::Color(255, 255, 255, 13));

	// Buttons
	sf::Vector2f newGamePos = sf::Vector2f(m_game->renderWindow()->getSize().x / 2, m_game->renderWindow()->getSize().y / 2);
	sf::Vector2f loadGamePos = sf::Vector2f(20, m_game->renderWindow()->getSize().y - 120);
	sf::Vector2f settingsPos = sf::Vector2f(m_game->renderWindow()->getSize().x / 2, m_game->renderWindow()->getSize().y - 120);
	sf::Vector2f exitPos = sf::Vector2f(m_game->renderWindow()->getSize().x, m_game->renderWindow()->getSize().y - 120);

	m_newGameButtonText = Gui::buildText(*m_game->font("dialogue"), 40);
	m_loadGameButtonText = Gui::buildText(*m_game->font("dialogue"), 40);
	m_settingsButtonText = Gui::buildText(*m_game->font("dialogue"), 40);
	m_exitButtonText = Gui::buildText(*m_game->font("dialogue"), 40);

	m_newGameButtonText.setString("New Game");
	m_loadGameButtonText.setString("Load Game");
	m_settingsButtonText.setString("Settings");
	m_exitButtonText.setString("Exit Game");

	m_newGameButtonText.setPosition(newGamePos - sf::Vector2f(m_newGameButtonText.getLocalBounds().width / 2, -1 * 50 * windowScale.y));
	m_loadGameButtonText.setPosition(loadGamePos);
	m_settingsButtonText.setPosition(settingsPos - sf::Vector2f(m_settingsButtonText.getLocalBounds().width / 2, 0));
	m_exitButtonText.setPosition(exitPos - sf::Vector2f(20 + m_exitButtonText.getLocalBounds().width, 0));

	m_newGameButton = std::make_shared<Clickable>("new_game", sf::IntRect(m_newGameButtonText.getGlobalBounds()));
	m_loadGameButton = std::make_shared<Clickable>("new_game", sf::IntRect(m_loadGameButtonText.getGlobalBounds()));
	m_settingsButton = std::make_shared<Clickable>("new_game", sf::IntRect(m_settingsButtonText.getGlobalBounds()));
	m_exitButton = std::make_shared<Clickable>("new_game", sf::IntRect(m_exitButtonText.getGlobalBounds()));

	m_game->music("title")->play();
}

int TitleScreen::run() {
	m_logoClock.reset(true);
	m_glowClock.reset(true);

	int glowFrame = 0;
	bool glowForward = true;

	while (true) {

		if (m_saveSlot >= 0) {
			m_game->music("title")->stop();
			return m_saveSlot;
		}

		sf::Event event;
		while (m_game->renderWindow()->pollEvent(event)) {
			// Window events
			if (event.type == sf::Event::Closed) {
				close();
			}

			// Keyboard events
			if (event.type == sf::Event::KeyPressed) {
				switch (event.key.code) {
				case sf::Keyboard::E:
					return 0;
					break;
				default:
					break;
				}
			}

			// Mouse events
			if (event.type == sf::Event::MouseButtonPressed) {
				switch (event.mouseButton.button) {
				case sf::Mouse::Left:
					m_logoSprite.setColor(sf::Color(255, 255, 255, 255));
					handleClick(sf::Vector2f(sf::Mouse::getPosition()));
					break;
				default:
					break;
				}
			}
		}

		// Logo
		if (m_logoClock.getElapsedTime().asSeconds() >= 2.02 && m_logoSprite.getColor().a <= 253) {
			int oldAlpha = m_logoSprite.getColor().a;
			m_logoSprite.setColor(sf::Color(255, 255, 255, oldAlpha + 2));
			m_logoClock.reset(true);
			m_logoClock.add(sf::seconds(2));
		}

		// Glow
		if (m_glowClock.getElapsedTime().asSeconds() >= 0.05) {

			if (glowFrame == 0) glowForward = true;
			else if (glowFrame == 20) glowForward = false;

			m_glowSprite.setColor(sf::Color(255, 255, 255, 13 + glowFrame));

			if (glowForward) glowFrame++;
			else glowFrame--;

			m_glowClock.reset(true);
		}

		m_game->renderWindow()->clear();
		m_game->renderWindow()->draw(m_backgroundSprite);
		m_game->renderWindow()->draw(m_glowSprite);
		m_game->renderWindow()->draw(m_logoSprite);
		m_game->renderWindow()->draw(m_newGameButtonText);
		m_game->renderWindow()->draw(m_loadGameButtonText);
		m_game->renderWindow()->draw(m_settingsButtonText);
		m_game->renderWindow()->draw(m_exitButtonText);
		m_game->renderWindow()->display();
	}

	close();
}

void TitleScreen::handleClick(sf::Vector2f clickPos) {
	if (m_newGameButton->wasClicked(clickPos)) {
		m_saveSlot = m_game->nextOpenSaveSlot();
	}
	else if (m_exitButton->wasClicked(clickPos)) {
		close();
	}
}

void TitleScreen::close() {
	m_game->music("title")->stop();
	m_game->renderWindow()->close();
}
