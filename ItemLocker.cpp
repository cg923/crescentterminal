
// Copyright 2019 Happyrock Studios

// Moon
#include "ItemLocker.h"
#include "InventoryGui.h"
#include "Game.h"

// STD
#include <iostream>

ItemLocker::ItemLocker(std::string name,
           std::string currentMap,
           std::shared_ptr<Game> game)
    : Entity(name, "locker", "locker", currentMap, game, 32, 64) {

    m_inventory = std::dynamic_pointer_cast<InventoryGui>(game->gui(name));
	std::dynamic_pointer_cast<InventoryGui>(game->gui(name))->expand();
    setDisplayText("Locker Contents");
    setDisplayBelowText("");

    m_open = false;

    m_sprites["middle"].push_back(m_sprite);
}

void ItemLocker::update(double dt) {
    Entity::update(dt);

    m_sprite->setOrigin(0, m_sprite->getTextureRect().height / 4);

    m_sprite->setPosition(m_position - sf::Vector2f(m_sprite->getTextureRect().width / 2, m_sprite->getTextureRect().height * 2/4));
}

void ItemLocker::open() {
    m_open = true;
    m_sprite->setTextureRect(sf::IntRect(0, m_sprite->getTextureRect().height, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
    m_game->displayLocker(name());
}

void ItemLocker::close() {
    m_open = false;
    m_sprite->setTextureRect(sf::IntRect(0, 0, m_sprite->getTextureRect().width, m_sprite->getTextureRect().height));
    m_game->displayLocker("");
}

void ItemLocker::toggle() {
    if (!m_open) open();
    else close();
}

std::shared_ptr<Item> ItemLocker::handleClick(sf::Event event, std::shared_ptr<Item> heldItem) {
    return m_inventory->handleClick(event, heldItem);
}

std::shared_ptr<Item> ItemLocker::removeFromInventory(int slot) {
    return m_inventory->removeFromInventory(slot);
}

std::shared_ptr<Item> ItemLocker::addToInventory(std::shared_ptr<Item> item, int slot) {
    return m_inventory->addToInventory(item, slot);
}

std::shared_ptr<Item> ItemLocker::addToInventory(std::string item, int slot) {
	return m_inventory->addToInventory(item, slot);
}

std::vector<std::shared_ptr<ItemSlot>> ItemLocker::allItemSlots() {
    return m_inventory->allItemSlots();
}

void ItemLocker::setDisplayText(std::string text) {
    m_inventory->setDisplayText(text);
}

void ItemLocker::setDisplayBelowText(std::string text) {
    m_inventory->setDisplayBelowText(text);
}

int ItemLocker::count(std::string itemType) {
    return m_inventory->count(itemType);
}

bool ItemLocker::full() {
	return m_inventory->full();
}
